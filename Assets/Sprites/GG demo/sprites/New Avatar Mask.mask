%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInternal: {fileID: 0}
  m_Name: New Avatar Mask
  m_Mask: 01000000000000000000000000000000000000000000000000000000010000000100000000000000000000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: GeoSphere010
    m_Weight: 0
  - m_Path: Hero
    m_Weight: 0
  - m_Path: HeroCullOff
    m_Weight: 0
  - m_Path: HeroDetals
    m_Weight: 0
  - m_Path: nurbsCircle1
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS/L_UpLeg
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS/L_UpLeg/L_Leg
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS/L_UpLeg/L_Leg/L_foot
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS/L_UpLeg/L_Leg/L_foot/L_toeEnd
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS/L_UpLeg/L_Leg/L_foot/L_toeEnd/joint12
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS/R_UpLeg
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS/R_UpLeg/R_Leg
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS/R_UpLeg/R_Leg/R_foot
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS/R_UpLeg/R_Leg/R_foot/R_toeEnd
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS/R_UpLeg/R_Leg/R_foot/R_toeEnd/joint12 1
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS/Spine
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS/Spine/spine2
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS/Spine/spine2/chest
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS/Spine/spine2/chest/HeroCloak
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS/Spine/spine2/chest/L_shoulders
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS/Spine/spine2/chest/L_shoulders/L_Arm
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS/Spine/spine2/chest/L_shoulders/L_Arm/L_foreArm
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS/Spine/spine2/chest/L_shoulders/L_Arm/L_foreArm/L_hand
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS/Spine/spine2/chest/L_shoulders/L_Arm/L_foreArm/L_hand/joint13
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS/Spine/spine2/chest/L_shoulders/L_Arm/L_foreArm/L_hand/joint13/joint22
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS/Spine/spine2/chest/L_shoulders/L_Arm/L_foreArm/L_hand/joint13/joint22/joint23
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS/Spine/spine2/chest/L_shoulders/L_Arm/L_foreArm/L_hand/joint13/joint22/joint23/joint24
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS/Spine/spine2/chest/L_shoulders/L_Arm/L_foreArm/L_hand/joint14
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS/Spine/spine2/chest/L_shoulders/L_Arm/L_foreArm/L_hand/joint14/joint15
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS/Spine/spine2/chest/L_shoulders/L_Arm/L_foreArm/L_hand/joint14/joint15/joint16
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS/Spine/spine2/chest/L_shoulders/L_Arm/L_foreArm/L_hand/joint14/joint15/joint16/joint17
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS/Spine/spine2/chest/L_shoulders/L_Arm/L_foreArm/L_hand/joint18
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS/Spine/spine2/chest/L_shoulders/L_Arm/L_foreArm/L_hand/joint18/joint19
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS/Spine/spine2/chest/L_shoulders/L_Arm/L_foreArm/L_hand/joint18/joint19/joint20
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS/Spine/spine2/chest/L_shoulders/L_Arm/L_foreArm/L_hand/joint18/joint19/joint20/joint21
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS/Spine/spine2/chest/L_shoulders/L_Arm/L_foreArm/L_hand/joint25
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS/Spine/spine2/chest/L_shoulders/L_Arm/L_foreArm/L_hand/joint25/joint26
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS/Spine/spine2/chest/L_shoulders/L_Arm/L_foreArm/L_hand/joint25/joint26/joint27
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS/Spine/spine2/chest/L_shoulders/L_Arm/L_foreArm/L_hand/joint25/joint26/joint27/joint28
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS/Spine/spine2/chest/L_shoulders/L_Arm/L_foreArm/L_hand/joint29
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS/Spine/spine2/chest/L_shoulders/L_Arm/L_foreArm/L_hand/joint29/joint30
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS/Spine/spine2/chest/L_shoulders/L_Arm/L_foreArm/L_hand/joint29/joint30/joint31
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS/Spine/spine2/chest/L_shoulders/L_Arm/L_foreArm/L_hand/joint29/joint30/joint31/joint32
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS/Spine/spine2/chest/neck
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS/Spine/spine2/chest/neck/head
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS/Spine/spine2/chest/neck/head/hat
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS/Spine/spine2/chest/neck/head/hat/hat2
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS/Spine/spine2/chest/neck/head/hat/hat2/hat3
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS/Spine/spine2/chest/neck/head/hat/hat2/hat3/hat4
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS/Spine/spine2/chest/neck/head/hat/hat2/hat3/hat4/hatend
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS/Spine/spine2/chest/R_shoulders
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS/Spine/spine2/chest/R_shoulders/R_Arm
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS/Spine/spine2/chest/R_shoulders/R_Arm/R_foreArm
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS/Spine/spine2/chest/R_shoulders/R_Arm/R_foreArm/R_hand
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS/Spine/spine2/chest/R_shoulders/R_Arm/R_foreArm/R_hand/joint13
      1
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS/Spine/spine2/chest/R_shoulders/R_Arm/R_foreArm/R_hand/joint13
      1/joint22 1
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS/Spine/spine2/chest/R_shoulders/R_Arm/R_foreArm/R_hand/joint13
      1/joint22 1/joint23 1
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS/Spine/spine2/chest/R_shoulders/R_Arm/R_foreArm/R_hand/joint13
      1/joint22 1/joint23 1/joint24 1
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS/Spine/spine2/chest/R_shoulders/R_Arm/R_foreArm/R_hand/joint14
      1
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS/Spine/spine2/chest/R_shoulders/R_Arm/R_foreArm/R_hand/joint14
      1/joint15 1
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS/Spine/spine2/chest/R_shoulders/R_Arm/R_foreArm/R_hand/joint14
      1/joint15 1/joint16 1
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS/Spine/spine2/chest/R_shoulders/R_Arm/R_foreArm/R_hand/joint14
      1/joint15 1/joint16 1/joint17 1
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS/Spine/spine2/chest/R_shoulders/R_Arm/R_foreArm/R_hand/joint18
      1
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS/Spine/spine2/chest/R_shoulders/R_Arm/R_foreArm/R_hand/joint18
      1/joint19 1
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS/Spine/spine2/chest/R_shoulders/R_Arm/R_foreArm/R_hand/joint18
      1/joint19 1/joint20 1
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS/Spine/spine2/chest/R_shoulders/R_Arm/R_foreArm/R_hand/joint18
      1/joint19 1/joint20 1/joint21 1
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS/Spine/spine2/chest/R_shoulders/R_Arm/R_foreArm/R_hand/joint25
      1
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS/Spine/spine2/chest/R_shoulders/R_Arm/R_foreArm/R_hand/joint25
      1/joint26 1
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS/Spine/spine2/chest/R_shoulders/R_Arm/R_foreArm/R_hand/joint25
      1/joint26 1/joint27 1
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS/Spine/spine2/chest/R_shoulders/R_Arm/R_foreArm/R_hand/joint25
      1/joint26 1/joint27 1/joint28 1
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS/Spine/spine2/chest/R_shoulders/R_Arm/R_foreArm/R_hand/joint29
      1
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS/Spine/spine2/chest/R_shoulders/R_Arm/R_foreArm/R_hand/joint29
      1/joint30 1
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS/Spine/spine2/chest/R_shoulders/R_Arm/R_foreArm/R_hand/joint29
      1/joint30 1/joint31 1
    m_Weight: 0
  - m_Path: nurbsCircle1/HIPS/Spine/spine2/chest/R_shoulders/R_Arm/R_foreArm/R_hand/joint29
      1/joint30 1/joint31 1/joint32 1
    m_Weight: 0
