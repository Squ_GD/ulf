﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
#region SNAPSHOTS
    [Header("Snapshots")]
    [SerializeField, FMODUnity.EventRef] //FMOD path for a snapshot to be applied when inventory is opened
    private string inventorySnapshotPath;
    private FMOD.Studio.EventInstance inventorySnapshot;
    private bool inventoryOpen = false;
    [SerializeField, FMODUnity.EventRef] //FMOD path for a snapshot to be applied during battle
    private string battleSnapshotPath;
    private FMOD.Studio.PLAYBACK_STATE battleSnapshotState;
    private FMOD.Studio.EventInstance battleSnapshot;
#endregion

#region VARIABLES FOR INVENTORY SOUND HANDLING
    [Header("Inventory Sounds")]
    [SerializeField, FMODUnity.EventRef] //FMOD event path for inventory opening SFX
    private string openInventorySound;
    [SerializeField, FMODUnity.EventRef] //FMOD event path for inventory cursor change position SFX
    private string cursorInventorySound;
    [SerializeField, Tooltip("FMOD base path for inventory item move SFX")]
    private string itemInventorySoundBase;
    private Inventory inventory;
#endregion

#region (DE)INITIALIZATION
    private void Awake()
    {
        inventorySnapshot = FMODUnity.RuntimeManager.CreateInstance(inventorySnapshotPath);
        battleSnapshot = FMODUnity.RuntimeManager.CreateInstance(battleSnapshotPath);
    }
    private void Start()
    {
        //Subscribe to ingame events
        EventManager.AddListener("EnemyLook", BattleMode);
        EventManager.AddListener("EnemyHP Visible", BattleExited);

        //Sibscribe to inventory events
        inventory = Inventory.inventory;
        inventory.OnInventoryOpen += InventoryOpened;
        inventory.OnCursoreMove += InventoryCursorMoved;
        inventory.OnAddItem += InventoryAddItem;
        inventory.OnCraftItem += InventoryCraftItem;
        inventory.OnEquipItem += InventoryEquipItem;
    }

    private void OnDestroy()
    {
        //Unsubscribe from ingame events
        EventManager.RemoveListener("EnemyLook", BattleMode);
        EventManager.RemoveListener("EnemyHP Visible", BattleExited);

        //Unsibscribe from inventory events
        inventory.OnInventoryOpen -= InventoryOpened;
        inventory.OnCursoreMove -= InventoryCursorMoved;
        inventory.OnAddItem -= InventoryAddItem;
        inventory.OnCraftItem -= InventoryCraftItem;
        inventory.OnEquipItem -= InventoryEquipItem;
    }
#endregion

#region PLAY/SNAPSHOT METHODS TO BE TRIGGERED IN GAME
    private void BattleMode(BecomeEvent BE)
    {
        battleSnapshot.getPlaybackState(out battleSnapshotState);

        if (BE.come && battleSnapshotState == FMOD.Studio.PLAYBACK_STATE.STOPPED)
        {
            battleSnapshot.start();
        }
    }
    private void BattleExited(BecomeEvent BE)
    {
        if (!BE.come)
        {
            battleSnapshot.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        }
    }
#endregion

#region PLAY/SNAPSHOTS METHODS TO BE TRIGGERED BY INVENTORY EVENTS
    private void InventoryOpened()
    {
        if (!inventoryOpen)
        {
            inventorySnapshot.start();
        } else
        {
            inventorySnapshot.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        }
        inventoryOpen = !inventoryOpen;
        
        FMODUnity.RuntimeManager.PlayOneShot(openInventorySound, transform.position);
    }
    private void InventoryCursorMoved()
    {
        FMODUnity.RuntimeManager.PlayOneShot(cursorInventorySound, transform.position);
    }
    private void InventoryAddItem(string itemName)
    {
        FMODUnity.RuntimeManager.PlayOneShot(itemInventorySoundBase + itemName, transform.position);
    }
    private void InventoryCraftItem(string itemName)
    {
        FMODUnity.RuntimeManager.PlayOneShot(itemInventorySoundBase + itemName + "_craft", transform.position);
    }
    private void InventoryEquipItem(string itemName)
    {
        FMODUnity.RuntimeManager.PlayOneShot(itemInventorySoundBase + itemName + "_equip", transform.position);
    }
#endregion
}
