﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Steamworks;

public class PlayerNetSocket : MonoBehaviour
{
    private Callback<P2PSessionRequest_t> _p2PSessionRequestCallback;

    public static PlayerNetSocket playerNetSocket;
    private List<LobbyPlayer> playerList;

    private bool ready = false, firstStart = false;

    private void Awake()
    {
        playerNetSocket = this;

        
    }

    private void OnEnable()
    {
        //CSteamID currID = (CSteamID)LobbyScript.lobby.current_lobbyID;
       // int numPlayers = SteamMatchmaking.GetNumLobbyMembers(currID);

        
    }


    // Start is called before the first frame update
    void Start()
    {
        playerList = new List<LobbyPlayer>();
        DontDestroyOnLoad(this);

        _p2PSessionRequestCallback = Callback<P2PSessionRequest_t>.Create(OnP2PSessionRequest);
    }

    void OnP2PSessionRequest(P2PSessionRequest_t request)
    {
        CSteamID clientId = request.m_steamIDRemote;

        SteamNetworking.AcceptP2PSessionWithUser(clientId);

    }

    public void EnemyReady(bool ready_, int playerID_)
    {
        LobbyPlayer player = playerList.Find(p => p.innerID == playerID_);
        player.ready = ready_;
        //Debug.LogError(player.name + " is ready");
        if (CheckAllIsReady() && !firstStart)
        {
            Converter.converter.SendReadyToAll(ready);
            Converter.converter.First_Stand_Ulf();
            //Control_Ulf.ulf_static.Set_Random_Deg_Multiplayer();
            firstStart = true;
        }

    }

    public void IReady(bool ready_)
    {
        ready = ready_;
        //Debug.LogError("I ready");
        Converter.converter.SendReadyToAll(ready);

        if (CheckAllIsReady() && !firstStart)
        {
            //Control_Ulf.ulf_static.Set_Random_Deg_Multiplayer();
            Converter.converter.First_Stand_Ulf();

            firstStart = true;
        }
    }

    private bool CheckAllIsReady()
    {
        if (!ready)
            return false;

        foreach (LobbyPlayer player in playerList)
        {
            if (!player.ready)
                return false;
        }

        //Debug.LogError("All is ready");

        return true;
    }

    public void AddPlayer(CSteamID newPlayer_)
    {
        LobbyPlayer newLobbyPlayer = new LobbyPlayer(newPlayer_);
        string namePlayer = SteamFriends.GetFriendPersonaName(newPlayer_);
        newLobbyPlayer.name = namePlayer;
       // Debug.LogError("New player: " + namePlayer);
        playerList.Add(newLobbyPlayer);
       // SteamNetworking.SendP2PPacket()
    }

    public void SendAllPlayers(string message_)
    {
        for(int i = 0; i < playerList.Count; i++)
        {
            byte[] bytes = new byte[message_.Length * sizeof(char)];
            System.Buffer.BlockCopy(message_.ToCharArray(), 0, bytes, 0, bytes.Length);

            SteamNetworking.SendP2PPacket(playerList[i].id, bytes, (uint)bytes.Length, EP2PSend.k_EP2PSendReliable);
        }
    }

    private void Update()
    {
        uint size;

        // repeat while there's a P2P message available
        // will write its size to size variable
        while (SteamNetworking.IsP2PPacketAvailable(out size))
        {
            // allocate buffer and needed variables
            var buffer = new byte[size];
            uint bytesRead;
            CSteamID remoteId;

            // read the message into the buffer
            if (SteamNetworking.ReadP2PPacket(buffer, size, out bytesRead, out remoteId))
            {
                // convert to string
                char[] chars = new char[bytesRead / sizeof(char)];
                System.Buffer.BlockCopy(buffer, 0, chars, 0, (int)bytesRead);

                string message = new string(chars, 0, chars.Length);
                //Debug.Log("Received a message: " + message);
                int  innerIDPlayer = playerList.Find(p => p.id == remoteId).innerID;
                //Debug.Log("New message");
                Converter.converter.ReceivedMessage(message, innerIDPlayer);
            }
        }
    }
}

public class LobbyPlayer
{
    public bool ready;
    private static int nextInnerID = 0;
    public CSteamID id;
    public int innerID;
    public string name;
    //public Battle_Ulf battle;
    //public Control_Ulf control;
    public LobbyPlayer(CSteamID id_)
    {
        ready = false;
        innerID = ++nextInnerID;
        id = id_;
        name = "";
        Debug.Log("New user id: " + innerID);
    }
}
