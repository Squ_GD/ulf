﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EquipUI : MonoBehaviour {
    Animator _animator;

    private void OnEnable()
    {
        _animator = GetComponent<Animator>();
        EventManager.AddListener("Inventory", inventoryOpen);
    }
    // Use this for initialization
    void Start () {
		
	}
	
    private void inventoryOpen(BecomeEvent BE)
    {
        if (BE.id == 1 && BE.come)
            _animator.SetTrigger("open");
    }
	// Update is called once per frame
	void Update () {
		
	}
}
