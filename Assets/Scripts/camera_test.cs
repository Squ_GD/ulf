﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class camera_test : CameraUlf
{
    private void Start()
    {
        StartCoroutine(scroll());
    }
    IEnumerator scroll()
    {
        while(true)
        {
            
            yield return null;
        }
    }
    private void Update()
    {
        if (!target)
            return;

        float delta = Time.deltaTime;

        Vector3 downPlusVect = -target.up;// * target.GetComponent<CapsuleCollider2D>().size.y/2f;
        Vector3 ulfPos = new Vector3(target.position.x, target.position.y, -10f);// 0-  downPlusVect;
        transform.position += (ulfPos - transform.position) * delta ;

        float angle = Vector3.Angle(transform.right, -target.right) * delta;
        transform.rotation.SetLookRotation(target.up, transform.right);
        transform.LookAt(transform.position + transform.forward, (target.up + transform.up) * delta);
    }
    private void LateUpdate()
    {
        return;
    }
}
