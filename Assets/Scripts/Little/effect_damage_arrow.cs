﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class effect_damage_arrow : MonoBehaviour {
    public float stretchTime, disapearStart;
	// Use this for initialization
	void Start () {
       // StartCoroutine(startStretch());
	}
	

    IEnumerator startStretch()
    {
        int frames = 5;
        bool disapearStarted = false;
        float oneFrame = stretchTime / frames;
        for (int s = 0; s < frames; s++)
        {
            yield return new WaitForSeconds(oneFrame);

            transform.localScale = new Vector3(transform.localScale.x + .1f, transform.localScale.y, 1);

            if (oneFrame * frames >= disapearStart && !disapearStarted)
            {
                StartCoroutine(startDisapear());
                disapearStarted = true;
            }
        }
    }

    IEnumerator startDisapear()
    {
        float alpha = GetComponent<SpriteRenderer>().color.a;
        float alphaDelta = alpha / 5f;
        for (int d = 0; d < 5; d++)
        {
            yield return new WaitForSeconds(.01f);
            alpha -= alphaDelta;
            GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, alpha);
        }
    }
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.J))
        {
            StartCoroutine(startStretch());
        }
	}
}
