﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fog : MonoBehaviour {
    public float rotateSpeed;
    private int direct;
    private float summaryInDirect, maxInDirect;
    // Use this for initialization
    void Start () {
        direct = -1;
        summaryInDirect = maxInDirect / 2f;
        maxInDirect = 5f;
    }
	
	// Update is called once per frame
	void Update () {
        transform.Rotate(Vector3.forward * direct, Time.deltaTime * rotateSpeed * .5f);
        summaryInDirect += Time.deltaTime * rotateSpeed;
        if(summaryInDirect >= maxInDirect)
        {
            direct *= -1;
            summaryInDirect = 0;
        }
    }
}
