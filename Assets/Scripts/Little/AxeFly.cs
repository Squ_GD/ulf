﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AxeFly : MonoBehaviour {
    public float ImpulseThrow, ImpulseRotate, speedCharacter;
    public SpriteRenderer sprite;
    private Rigidbody2D _rigidbody;
    private Collider2D colliderHandle, colliderBlade;
    private bool contactPlanet;
    private Weapon _weapon;
    private Vector3 planetCenter;
    private Transform SprMask;
    private bool firstContactEnemy;

    private void Awake()
    {

        colliderHandle = transform.GetChild(0).GetComponents<CapsuleCollider2D>()[0];
        colliderBlade = transform.GetChild(1).GetComponents<CapsuleCollider2D>()[0];
    }

    // Use this for initialization
    void Start () {
        SprMask = transform.GetChild(2);
        SprMask.gameObject.SetActive(false);

        _rigidbody = GetComponent<Rigidbody2D>();
        //Debug.Log("Speed of throw: " + speedCharacter);
        _rigidbody.AddForce(transform.right * (ImpulseThrow + speedCharacter/15f), ForceMode2D.Impulse);
        _rigidbody.AddForceAtPosition(-transform.right * ImpulseRotate, transform.localPosition - transform.up * transform.localScale.x, ForceMode2D.Impulse);
        
        _weapon.type_weapon = Weapon.Type.fly_axe;
    }

    public void SetIgnoredCollisions(Collider2D ignoredCollider)
    {
        Physics2D.IgnoreCollision(ignoredCollider, colliderHandle, true);
        Physics2D.IgnoreCollision(ignoredCollider, colliderBlade, true);
    }

    public void SetWeapon(Weapon weapon)
    {
        _weapon = weapon;
    }

    public void SetPlanet(Vector3 PC)
    {
        planetCenter = PC;
    }

    public void SetSpeed(float speed)
    {
        speedCharacter = speed;
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "enemy" || collision.gameObject.tag == "Player")
        {
            Debug.Log("Trigger: " + collision.gameObject.tag);
            resetForces();
        }
        /*if (collision.gameObject.tag == "planet")
        {
            resetForces();
            contactPlanet = true;
            Debug.Log("axe collision planet");
        }*/
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        

        if (collision.gameObject.tag == "enemy")
        {
            resetForces();
            //colliderBlade.enabled = false;
            firstContactEnemy = true;
        }

    }
    private void resetForces()
    {
        //_rigidbody.angularDrag = 100;
        _rigidbody.angularVelocity = 0;
        _rigidbody.velocity = new Vector2();
    }
    private void stoped(Transform transCollision)
    {
        contactPlanet = true;
        _rigidbody.simulated = false;
        colliderBlade.enabled = false;

        float angl = Vector3.Angle(SprMask.right, transCollision.position - SprMask.position);

        Debug.Log("Axe stuck angle: " + angl); 

        if (!firstContactEnemy && angl <= 45)
        {
            SprMask.gameObject.SetActive(true);
            if (SprMask.InverseTransformPoint(transCollision.position).y > 0)
                SprMask.Rotate(Vector3.forward, angl);
            else
                SprMask.Rotate(-Vector3.forward, angl);

            SprMask.transform.position += SprMask.right * .3f * transform.localScale.x;
        }
        StartCoroutine(destroyAxe());
    }

    public void collisionBlade(Collider2D collision, Transform TB)
    {
        //Debug.Log("Trigger");
        if(collision.tag == "lootable")
            return;

        if (collision.tag == "planet")
        {
            resetForces();
            stoped(collision.transform);
        }

        if (collision.tag == "enemy" && !firstContactEnemy)
        {
            Battle_Basis BB = collision.GetComponent<Battle_Basis>();
            DamageEnemy(BB);
            transform.parent = BB.Get_StickFor_Missle();
            TB.gameObject.SetActive(false);
            resetForces();
            stoped(collision.transform);
        }
    }

    IEnumerator destroyAxe()
    {
        yield return new WaitForSeconds(1f);
        for (float a = 1; a >= 0; a-=.1f)
        {
            //Debug.Log("Color alpha: " + a);
            sprite.color = new Color(1, 1, 1, a);
            yield return new WaitForSeconds(.05f);
        }
        Destroy(gameObject);
    }

    private void DamageEnemy(Battle_Basis battle)
    {
        battle.Attacking(_weapon, transform.GetChild(1).transform.position);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (!contactPlanet)
        {
            Vector3 grav = planetCenter - transform.position;
            grav.Normalize();
            _rigidbody.AddForce(grav * _rigidbody.gravityScale);
        }
    }
}
