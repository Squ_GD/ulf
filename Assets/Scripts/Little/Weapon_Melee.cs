﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Weapon_Melee : Weapon_Basis {
    protected CapsuleCollider2D myCollider;


    // Use this for initialization
    void Start () {
        myCollider = GetComponent<CapsuleCollider2D>();
        if (ulfWeapon > 0)
            strengthChange(0);
    }
    public void ActivateCollider(bool isActive)
    {
        if(myCollider)
        myCollider.enabled = isActive;
        activated = true;
    }

    /*protected override void DisableWeapon(BecomeEvent BE)
    {
        if (BE.come && BE.id == myID)
        {
            Debug.Log("disable weapon: " + gameObject.name);
            if (!gameObject.GetComponent<Movement_basis>())
                myCollider.enabled = false;
        }
    }*/

    public override void Attack()
    {
        ActivateCollider(!myCollider.enabled);
    }

    public override void Attack(bool attack_)
    {
        ActivateCollider(attack_);
    }
    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == weapon.tagWeapon)
            return;
        if (!activated)
        {
            Debug.Log("Deactivated");
            return;
        }
        if (collider.tag == "enemy" || collider.tag == "Player")
        {
            if (!collider.GetComponent<Battle_Basis>())
                return;
            float scaleX = transform.localScale.x;
            Vector3 offsetX = transform.right * myCollider.offset.x * scaleX;
            Vector3 offsetY = transform.up * myCollider.offset.y * scaleX;
            Vector3 contactPoint = transform.position + (offsetX + offsetY) / 2f;

            if ( UnityEngine.Random.Range(.01f, 1f) < missChance)
            {

                collider.GetComponent<Battle_Basis>().Attacking(0,0);
            }
            else// if(collider.tag != weapon.tagWeapon)
            {
                collider.GetComponent<Battle_Basis>().Attacking(weapon, contactPoint);
                if (rune.RuneType != Rune.TypeRune.empty)
                {
                    //Debug.Log("Set rune: " + rune.RuneType);
                    collider.GetComponent<Battle_Basis>().SetRune(rune);
                }
                //Physics2D.IgnoreCollision(collider, myCollider);

                if (ulfWeapon > 0)
                {
                    strengthChange(-1);
                }
            }
        }
    }
}