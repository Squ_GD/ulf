﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon_Summon : Weapon_Basis {
    public GameObject summonEnemy;
    private int dirNew_;
    private Planet summPlanet;
    private Transform masterTrans;
	// Use this for initialization
	void Start () {
		
	}
    public override void Attack(bool attack_)
    {

    }

    public override void Attack()
    {
        if(!summonEnemy)
        {
            Debug.LogError("Not a summon!");
            return;
        }
        GameObject sumon =  Instantiate(summonEnemy, transform.position, transform.rotation);
        //sumon.GetComponent<Animator>().SetInteger("custom", Random.Range(1, 3));
        sumon.GetComponent<Movement_basis>().SetPlanet(summPlanet, dirNew_);
        //sumon.GetComponent<CrypLowSummon>().SetSummonPlanet(summPlanet);


    }
    public void SetNewDir(int dir_, Transform masterTransform, Planet _planet)
    {
        masterTrans = masterTransform;
        dirNew_ = dir_;
        summPlanet = _planet;
    }



    // Update is called once per frame
    void Update () {
		
	}
}
