﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cloud_folder : MonoBehaviour {
    Camera cam;
    Vector3 camLastPos;
    public Transform littleCloudFolder;
   // SpriteRenderer littleCloud;
   private GameObject[] cloudList;
    private int[] swingClockWise;
    private int maxSwing = 40;
	// Use this for initialization
	void Start () {
        cam = Camera.main;
        camLastPos = cam.transform.position;
        //transform.position = camLastPos + transform.forward;
        cloudList = new GameObject[littleCloudFolder.childCount];
        //littleCloudFolder.parent = cam.transform.GetChild(0);
        //littleCloudFolder.localPosition = Vector2.zero;
        InitLittleClouds();
        StartCoroutine(ReSpawnLittleClouds());
    }
    private void InitLittleClouds()
    {
        for(int c = 0; c < cloudList.Length; c++)
        {
            float camSizeX = Random.Range(-cam.orthographicSize * 2.3f, cam.orthographicSize * 2.3f);
            float camSizeY = Random.Range(-cam.orthographicSize * 1.2f, 0);
            //Debug.Log("CamSize: " + camSize);
            cloudList[c] = littleCloudFolder.GetChild(c).gameObject;
           // cloudList[c].AddComponent<SpriteRenderer>();
           // cloudList[c].GetComponent<SpriteRenderer>().sprite = cloudSprites[c];
           // cloudList[c].transform.parent = cam.transform.GetChild(0);
            cloudList[c].transform.rotation = Quaternion.identity;
            cloudList[c].transform.position = new Vector3(camSizeX, camSizeY, 0);
            //swingClockWise[c] = Random.Range(0, 2) == 0 ? -1 : 1;
            cloudList[c].SetActive(false);
           // littleCloud = spriteGO.AddComponent<SpriteRenderer>();
           // littleCloud.sprite = cloudSprites[0];
        }
    }
    IEnumerator ReSpawnLittleClouds()
    {
        while(true)
        {
            yield return new WaitForSeconds(Random.Range(2f, 5f));
            int nextCloud = Random.Range(0, 6);
            ActivateCloud(nextCloud);

        }
    }
    private void ActivateCloud(int index)
    {
        if(!cloudList[index].activeSelf)
        {
            cloudList[index].SetActive(true);
            float camSizeX = Random.Range(-cam.orthographicSize * 2.3f, cam.orthographicSize * 2.3f);
            float camSizeY = Random.Range(-cam.orthographicSize * 1.2f, 0);
            cloudList[index].transform.position = cam.transform.position +  new Vector3(camSizeX, camSizeY, 0);
            //moveVectors[index] = cloudList[index].transform.right;

            cloudList[index].transform.rotation = cam.transform.rotation;
            cloudList[index].GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0);
            //StartCoroutine(SwingLittleCloud(index));
            StartCoroutine(OpacityLittleClouds(index));
        }
    }

    IEnumerator OpacityLittleClouds(int index)
    {
        SpriteRenderer currSprite = cloudList[index].GetComponent<SpriteRenderer>();

        float _alpha = currSprite.color.a;
        while (_alpha <= .5f)
        {
            yield return new WaitForSeconds(.1f);
            _alpha += .01f;
            currSprite.color = new Color(1, 1, 1, _alpha);
        }
        yield return new WaitForSeconds(3f);

        while (_alpha >= 0f)
        {
            yield return new WaitForSeconds(.1f);
            _alpha -= .02f;
            currSprite.color = new Color(1, 1, 1, _alpha);
        }
        cloudList[index].SetActive(false);
    }

    IEnumerator SwingLittleCloud(int index)
    {
        int swingDirect = 1;
        int swingTimes = 40;
        int currSwing = swingTimes/2;
        int lifeTimer = Random.Range(600, 1200);
        while (lifeTimer > 0)
        {
            while(currSwing < swingTimes)
            {
                yield return new WaitForSeconds(.01f);
                cloudList[index].transform.Rotate(transform.forward * swingDirect, .2f);
                currSwing++;
                lifeTimer--;
            }
            swingDirect *= -1;
            currSwing = 0;
        }
        cloudList[index].SetActive(false);
    }

    private void Update()
    {
        for (int cl = 0; cl < cloudList.Length; cl++)
        {
            if (cloudList[cl].activeSelf)
            {

                cloudList[cl].transform.position += cloudList[cl].transform.up * Time.deltaTime * .3f;


            }
        }
    }

    // Update is called once per frame
    void LateUpdate()
    {
       float deltaX = cam.transform.position.x - camLastPos.x;
       float deltaY = cam.transform.position.y - camLastPos.y;
       // transform.position = new Vector3(transform.position.x + deltaX * .8f, transform.position.y + deltaY * .8f); //Time.deltaTime * (camLastPos - transform.position);
       // transform.rotation = cam.transform.rotation;
        camLastPos = cam.transform.position;
    }

}
