﻿using UnityEngine;

public class UI_Weapon : MonoBehaviour {
    public bool left;
    private Animator _anim;
    private bool checkStart;
    private void OnEnable()
    {
        EventManager.AddListener("Ulf attack", UlfAttack_UI);
        EventManager.AddListener("Switch weapon", SwitchWeapons);
        _anim = GetComponent<Animator>();
        _anim.SetBool("leftHand", left);
    }
    public void bowCheck()
    {

    }
    private void UlfAttack_UI(BecomeEvent BE)
    {
        if (BE.come == left || BE.power >= 100 )//>100 - 2hands weapon
        {
            //Debug.Log("BE.come: " + BE.come + " BE.power: " + BE.power);

            switch (BE.id)
            {
                case -1:
                    _anim.SetTrigger("cancelAttack");
                    _anim.ResetTrigger("startAltAttack");
                    _anim.ResetTrigger("startAttack");
                    _anim.ResetTrigger("attack");
                    checkStart = false;

                    break;

                case 1:
                        _anim.SetTrigger("startAttack");
                    if(BE.power >= 100)
                        checkStart = true;
                    break;

                case 2:
                        _anim.SetTrigger("startAltAttack");

                    break;

                case 0:
                        _anim.SetTrigger("attack");
                    if (BE.power >= 100)
                    {
                        if (checkStart)
                            checkStart = false;
                        else
                            _anim.SetTrigger("startAttack");
                    }
                    break;
            
            }
        }
    }

    private void SwitchWeapons(BecomeEvent BE)
    {
        if (BE.come == left || BE.power >= 100)//>100 - 2hands weapon
        {
            _anim.ResetTrigger("attack");
            _anim.ResetTrigger("cancelAttack");
            _anim.ResetTrigger("startAttack");
            _anim.ResetTrigger("startAltAttack");

            ChooseWeapon(BE.power);
        }
    }

    private void ChooseWeapon(int weaponNum)
    {

        _anim.SetInteger("weapon", weaponNum);

    }

}
