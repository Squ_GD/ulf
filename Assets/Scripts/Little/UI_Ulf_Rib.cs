﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_Ulf_Rib : MonoBehaviour {

    private Animator _animator;
    // Use this for initialization
    private void OnEnable()
    {
        EventManager.AddListener("rib", RibChange);

    }

    void Start()
    {
        _animator = GetComponent<Animator>();

        // StartCoroutine(Cardio());
    }
    private void RibChange(BecomeEvent BE)
    {
        _animator.SetInteger("rib", BE.power);
    }
}
