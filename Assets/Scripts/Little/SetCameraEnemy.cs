﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetCameraEnemy : MonoBehaviour {
    private Animator _animator;
    private Control_enemy control_Enemy;
    private TestControl test;
    private bool stopMove;
    private int ID;

    private void Awake()
    {
        EventManager.AddListener("MouseOver", SetTarget);
    }

    private void OnDisable()
    {
        EventManager.RemoveListener("MouseOver", SetTarget);

    }

    void SetTarget(BecomeEvent BE)
    {
        Debug.Log("Targeting");
        if (BE.come)
        {
            if (ID == BE.id)
                return;
            if (test)
            {
                test.GetComponent<Animator>().SetBool("move", false);
                test.enabled = false;
            }
            ID = BE.id;
            int lastDirect;
            float speed;
            control_Enemy = Statistic.GetUnit(ID).GetComponent<Control_enemy>();
            speed = control_Enemy.SpeedLinear;
            if (control_Enemy.GetComponent<TestControl>())
            {
                control_Enemy.GetComponent<TestControl>().enabled = true;
                lastDirect = control_Enemy.GetComponent<TestControl>().lastDir;
            }
            else
            {
                control_Enemy.gameObject.AddComponent<TestControl>();
                lastDirect = control_Enemy.lastDir;
            }
            test = control_Enemy.GetComponent<TestControl>();
            test.disableShadow(false);
            test.setPlan(control_Enemy.GetPlanet(), lastDirect, speed);
            control_Enemy.enabled = false;
            //battle_Enemy.GetComponent<TestAnimControl>() = this;
            StartTest();
        }
    }

    void StartTest () {
        _animator = GetComponent<Animator>();
        CameraUlf.cameraUlf.SetTarget(control_Enemy.transform);
        Debug.Log("Start");
       // GetComponent<Control_enemy>().enabled = false;
       // GetComponent<Battle_enemy>().enabled = false;
	}


    // Update is called once per frame
    void Update () {

    }
}
