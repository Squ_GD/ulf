﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.Rendering.PostProcessing;

public class DayNight : MonoBehaviour {
    /*
    private ColorGrading colorGrading;
    public float NightTemperature , NightTint , NightContrast ;
    public float CycleTime , StepTime;
    private const float DayTemperature = 0, DayTint = 0, DayContrast = 0;


   
    // Use this for initialization
    void Start () {
        PostProcessVolume volume = gameObject.GetComponent<PostProcessVolume>();
        volume.profile.Reset();
        volume.profile.TryGetSettings(out colorGrading);
        //volume.profile.isDirty = true;
        StartCoroutine(DayNightCycle(true));
    }


    
    IEnumerator DayNightCycle(bool Day)
    {
        float stepTemp, stepTint, stepContrast;
       // ColorGrading set = profile;
        if (Day)
        {
            stepTemp = (DayTemperature + NightTemperature) / CycleTime;
            stepTint = (DayTint + NightTint) / CycleTime;
            stepContrast = (NightContrast - DayContrast) / CycleTime;
        }
        else
        {
            stepTemp = (DayTemperature - NightTemperature) / CycleTime;
            stepTint = (DayTint - NightTint) / CycleTime;
            stepContrast = (DayContrast - NightContrast) / CycleTime;
        }

        for(int t = 0; t < CycleTime; t++)
        {

            colorGrading.temperature.value += stepTemp;
            colorGrading.tint.value += stepTint;
            colorGrading.contrast.value += stepContrast;

            yield return new WaitForSeconds(StepTime);

        }
        StartCoroutine(DayNightCycle(!Day));
    }
    */
}
