﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class wispi_spheres : Weapon_Range { //Отделяется от мастер-объекта, 3 сферы для выстрела
    private Transform masterWisp;
    private Vector3 localStartPos, relativeRight;
    public Transform sphere1, sphere2, sphere3;
    private bool SR1, SR2, SR3, SL1, SL2, SL3, SA1, SA2, SA3;
    private Movement_basis masterMove; //После отделения от хозяина, нужен его ГО
     
	// Use this for initialization
	void Start () {
        masterWisp = transform.parent;
        localStartPos = transform.localPosition;
        transform.parent = null;
        relativeRight = transform.InverseTransformDirection(masterWisp.right);
        masterMove = masterWisp.GetComponent<Movement_basis>();

        missile = Instantiate(sphere1.gameObject);
        missile.SetActive(false);
    }
    public void CanRightShoot(int sphereNum)
    {
        if (sphereNum == 1) SR1 = true;
        else if (sphereNum == 2) SR2 = true;
        else if (sphereNum == 3) SR3 = true;
    }
    public void CanLeftShoot(int sphereNum)
    {
        if (sphereNum == 1) SL1 = true;
        else if (sphereNum == 2) SL2 = true;
        else if (sphereNum == 3) SL3 = true;
    }
    public void DontShoot(int sphereNum)
    {
        if (sphereNum == 1) SR1 = SL1 = false;
        else if (sphereNum == 2) SR2 = SL2 = false;
        else if (sphereNum == 3) SR3 = SL3 = false;

    }

    // Update is called once per frame
    void Update () {
        if (!masterMove)
        {
            Destroy(gameObject);
            enabled = false;
            return;
        }
        float angle = Vector3.Angle(transform.right, -masterWisp.right) * Time.deltaTime;
        transform.rotation.SetLookRotation(masterWisp.up, transform.right);
        transform.LookAt(transform.position + transform.forward, (masterWisp.up + transform.up) * Time.deltaTime);
        transform.position = masterWisp.position + masterWisp.up * localStartPos.y * transform.localScale.y;
        SA1 = sphere1.gameObject.activeSelf;
        SA2 = sphere2.gameObject.activeSelf;
        SA3 = sphere3.gameObject.activeSelf;

        //3 сферы уничтожены
        if(!SA1 && !SA2 && !SA3)
        {
            masterWisp.GetComponent<Battle_enemy>().InvertPrioritiesWeapon(true);
            Debug.Log("Invert Priorities");
            this.enabled = false;
        }

        Vector3 NewRelativeRight = transform.InverseTransformDirection(masterWisp.right);
        
        if (relativeRight != NewRelativeRight)
        {

            relativeRight = NewRelativeRight;

            return;
        }

        if (!shoot)
            return;

        tryAttack();
    }

    private void tryAttack()
    {
        int wispDir = masterMove.lastDir;

        bool s1 = SR1 && wispDir == 1 || SL1 && wispDir == -1;
        bool s2 = SR2 && wispDir == 1 || SL2 && wispDir == -1;
        bool s3 = SR3 && wispDir == 1 || SL3 && wispDir == -1;


        if (s1 && SA1)
        {
            missile.transform.position = sphere1.position;
            missile.transform.rotation = sphere1.rotation;
            
            sphere1.gameObject.SetActive(false);
        }
        else if (s2 && SA2)
        {
            missile.transform.position = sphere2.position;
            missile.transform.rotation = sphere2.rotation;

            sphere2.gameObject.SetActive(false);
        }
        else if (s3 && SA3)
        {
            missile.transform.position = sphere3.position;
            missile.transform.rotation = sphere3.rotation;

            sphere3.gameObject.SetActive(false);
        }
        else
        {
            return;
        }
        missile.transform.localScale = masterWisp.localScale;
        if (wispDir == -1)
        {
            missile.transform.Rotate(Vector3.forward, 180);
            missile.transform.Rotate(Vector3.forward, -30f);

        }
        else
            missile.transform.Rotate(Vector3.forward, 30f);
        missile.SetActive(true);
        missile.GetComponent<Missile_fly>().enabled = true;
        missile.GetComponent<CapsuleCollider2D>().enabled = true;
        shoot = false;

        missile = Instantiate(sphere1.gameObject);
        missile.SetActive(false);
    }

    public override void Attack()
    {
        Missile_fly _missile = missile.GetComponent<Missile_fly>();
        _missile.SetStartParam(gameObject.tag, weapon, planetCenter, missileStartImpulse);
        shoot = true;
        
    }

    protected override void DisableWeapon(BecomeEvent BE)
    {
        if(BE.id == myID)
        {
            StartCoroutine(CollapseOverDie());
        }
    }
    IEnumerator CollapseOverDie()
    {
        while(true)
        {
            transform.localScale *= .97f;
            yield return new WaitForSeconds(.08f);
        }
    }
}