﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_Hp_Ulf : MonoBehaviour {
    public Transform HP_line;

    private float hpLine_MaxY, hpLine_MinY, hpLineX;
    private float piece;
    private int maxHeal;
    private Animator _animator;
    private float cardioTime = 5f;
    // Use this for initialization
    private void OnEnable()
    {
        EventManager.AddListener("TakeDamage", TakeDamageUlf);
        EventManager.AddListener("HealMax", SetMaxHeals);
        EventManager.AddListener("Death", DeathUlf);

        hpLine_MaxY = HP_line.localPosition.y;
        hpLine_MinY = hpLine_MaxY - 1.3f;
        hpLineX = HP_line.localPosition.x;
    }

    void Start () {
        _animator = GetComponent<Animator>();
        
       // StartCoroutine(Cardio());
    }

    private void DeathUlf(BecomeEvent BE)
    {
        if (BE.id != 0)
            return;
        //StopAllCoroutines();
        GetComponent<Animator>().enabled = false;
    }

    IEnumerator Cardio()
    {
        while (true)
        {
            _animator.SetTrigger("cardio");
            yield return new WaitForSeconds(cardioTime);
        }
    }

    private void SetMaxHeals(BecomeEvent evt)
    {
        if (evt.id != 0)
            return;

        maxHeal = evt.power;
        piece = (hpLine_MaxY - hpLine_MinY) / maxHeal;

        // Debug.Log("min HP: " + hpLine_MinY + " hpLine_MaxY: " + hpLine_MaxY +" piece: " + piece);
    }
    private void TakeDamageUlf(BecomeEvent evt)
    {
        if (evt.id != 0 )
            return;
        StopAllCoroutines();

        StartCoroutine(changeHPLine(evt.power));
    }

    IEnumerator changeHPLine(float change)
    {

        float iter = .1f;
        for (int i = 0; i < 1 / iter; i++)
        {
            yield return new WaitForSeconds(.01f);
            float newPosY = HP_line.transform.localPosition.y - change * piece * iter;
            HP_line.transform.localPosition = new Vector2(hpLineX, newPosY);
        }
    }
}
