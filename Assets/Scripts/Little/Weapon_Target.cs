﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon_Target : Weapon_Basis {
    private Transform _target;
    private bool damageCoolDown;
    public float forwardPlus, upPlus;
    public bool ItsMe;
	// Use this for initialization
	void Start () {
		
	}

    public override void Attack(bool attack_)
    {
        //throw new NotImplementedException();
    }

    public override void Attack()
    {
        transform.parent = null;
        float targetColliderHeight = _target.GetComponent<Movement_basis>().getColliderHeight();
        float targetColliderWidth = _target.GetComponent<Movement_basis>().getColliderWidth();
        float targetColliderOffset = _target.GetComponent<Movement_basis>().getColliderOffsetY();
        transform.position = _target.position;
        transform.position -= _target.right * targetColliderWidth * forwardPlus;
        transform.position -= _target.up * targetColliderHeight * upPlus;

        if (!ItsMe)
        {
            transform.position -= _target.up * (targetColliderHeight - targetColliderOffset);
            transform.rotation = _target.rotation;
            transform.localScale = _target.localScale;
        }
        else
        {
            GetComponent<Control_enemy>().rotateToUlf();
        }

        for (int c = 0; c < transform.childCount; c++)
        {
            transform.GetChild(c).gameObject.SetActive(true);
        }

        _target.GetComponent<Battle_Basis>().SetRune(rune);
       
    }
    
    
    IEnumerator coolDownDamage(float coolDownTime)
    {
        damageCoolDown = true;
        yield return new WaitForSeconds(coolDownTime);
        damageCoolDown = false;
    }
    public void SetTarget(Transform currTarget)
    {
        _target = currTarget;
    }
    // Update is called once per frame
    void Update () {
		
	}
}
