﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Managers : MonoBehaviour {
    private GameObject[] ObjectArray;
    private List<GameObject> objectList;
    protected int reserveMuiltiplier = 3;
    protected string Path;
    
    // Use this for initialization
    void Start() {
        ObjectArray = Resources.LoadAll<GameObject>(Path);
        objectList = new List<GameObject>(reserveMuiltiplier * ObjectArray.Length);
        for (int r = 0; r < ObjectArray.Length; r++)
        {
            for (int m = 0; m < reserveMuiltiplier; m++)
            {
                GameObject missile_ = Instantiate(ObjectArray[r]);
                missile_.name = ObjectArray[r].name;
                objectList.Add(missile_);
                missile_.transform.position = transform.position;
                missile_.SetActive(false);
            }
        }
	}

    public GameObject GetObject(string missileName)
    {
        GameObject gettingMis = null;

        foreach (GameObject gm in objectList)
        {

            if (gm.name != missileName)
                continue;
            if (gm.activeSelf)
                continue;
            //Debug.Log("getting: " + gm.name);

            gettingMis = gm;
            break;
        }
        if (!gettingMis)
            Debug.Log("Wrong missile name: " + missileName);
        return gettingMis;
    }
    
}
