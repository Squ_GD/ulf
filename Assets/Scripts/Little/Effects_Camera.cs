﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Effects_Camera : MonoBehaviour {
    private ParticleSystem leavesParticle;
    private Material[] leaves;
    // Use this for initialization
    private void OnEnable()
    {
        EventManager.AddListener("Planet stand", event_Stand);
        leaves = new Material[4];
    }

    void Start () {
        leavesParticle = transform.GetChild(0).GetComponent<ParticleSystem>();

    }
	private void loadLeaves(string _type)
    {

        for(int i = 0; i < leaves.Length; i++)
        {
            leaves[i] = Resources.Load<Material>("Planets\\" + _type + "\\leave" + i);

        }
    }
    void event_Stand(BecomeEvent obj)
    {
        if (!obj.come)
        {
            leavesParticle.gameObject.SetActive(false);

            StopCoroutine(changeLivesOverTime());
        }
        switch(obj.id)
        { 
            case 0:
                loadLeaves("green");
                leavesParticle.gameObject.SetActive(true);
                //StartCoroutine(changeLivesOverTime());
                break;
            default:
                break;
        }
    }

    IEnumerator changeLivesOverTime()
    {
        while(true)
        {
            yield return new WaitForSeconds(2f);
            Renderer _render = leavesParticle.GetComponent<Renderer>();
            _render.material = leaves[Random.Range(0,leaves.Length)];
        }
    }
    // Update is called once per frame
    void Update () {
		
	}
}
