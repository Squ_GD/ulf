﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestControl : Movement_basis {
    public Planet Planet_current;
    private Battle_enemy battleComponent;
    Animator animator;
    private bool move, attack;
    private int newDir;
    public static TestControl testControl;

    private void Awake()
    {
        testControl = this;
    }


    // Use this for initialization
    void Start () {
        battleComponent = GetComponent<Battle_enemy>();
        animator = GetComponent<Animator>();
        testControl = this;

    }

    public void AttackAnim(int numAttack)
    {
        battleComponent.SetUsedWeapon(numAttack);
        animator.SetInteger("attack", numAttack);
        animator.SetBool("death", false);
    }

    public void ActionsAnimIdle(string action_, int num_)
    {
        animator.SetTrigger("idle");
        animator.SetInteger("custom", num_);
    }

    public void ActionsAnimDamage()
    {
        animator.SetTrigger("damageLow");
        animator.SetBool("death", false);
    }

    public void ActionsAnimDeath()
    {
        animator.SetBool("death", true);
    }
    public void ActionsAnimRebind()
    {
        animator.Rebind();
        animator.GetComponent<Battle_Basis>().enabled = true;
        animator.speed = 1f;
        //animator.WriteDefaultValues();
        //animator.StartPlayback();
        animator.SetBool("death", false);

    }

    public void setPlan(Planet planet, int dir, float speed)
    {
        Debug.Log("LastDir: " + lastDir);
        lastDir = dir;
        Planet_current = planet;
        set_Planet(planet);
        planet.Get_Object_degree(this);
        Move(lastDir);
        newDir = lastDir;
        SpeedLinear = speed;
    }
    // Update is called once per frame
    void Update()
    {
        float directHorizontal = Input.GetAxis("Horizontal");
        if (directHorizontal != 0 && battleComponent.CanMove())
        {
            newDir = (int)(Mathf.Abs(directHorizontal) / directHorizontal);
            if (Planet_current)
            {
                // Debug.Log("move planet: " + newDir);
                Planet_current.Get_Object_degree(this);
                Move(newDir);
            }

            lastDir = newDir;
            animator.SetBool("move", true);
            animator.SetBool("death", false);
            
        }
        else
        {
            animator.SetBool("move", false);

        }

        if (Input.GetKeyDown(KeyCode.Keypad0))
        {
            //battleComponent.SetUsedWeapon(0);
            //animator.SetInteger("attack", 0);
            animator.SetBool("flipPosition", !animator.GetBool("flipPosition"));
            animator.SetBool("fire", !animator.GetBool("fire"));
        }

        if (Input.GetKeyDown(KeyCode.Keypad1))
        {
            battleComponent.SetUsedWeapon(0);
            animator.SetInteger("attack", 0);
            animator.SetBool("death", false);
        }
        if (Input.GetKeyDown(KeyCode.Keypad2))
        {
            battleComponent.SetUsedWeapon(1);
            animator.SetInteger("attack", 1);
            animator.SetBool("death", false);
        }
        if (Input.GetKeyDown(KeyCode.Keypad3))
        {
            battleComponent.SetUsedWeapon(2);
            animator.SetInteger("attack", 2);
            animator.SetBool("death", false);
        }
        if (Input.GetKeyDown(KeyCode.Keypad4))
        {
            animator.SetTrigger("idle");
            animator.SetInteger("custom", 1);
        }
        if (Input.GetKeyDown(KeyCode.Keypad5))
        {
            animator.SetTrigger("idle");
            animator.SetInteger("custom", 2);
        }
        if (Input.GetKeyDown(KeyCode.Keypad6))
        {
            animator.SetTrigger("idle");
            animator.SetInteger("custom", 3);
        }
        if (Input.GetKeyDown(KeyCode.Q))
        {
            animator.SetTrigger("damageLow");
            animator.SetBool("death", false);
        }
        if (Input.GetKeyDown(KeyCode.E))
        {
            animator.SetBool("death", true);
        }
        if (Input.GetKey(KeyCode.LeftShift))
        {
            animator.SetBool("run", true);
        }
        else
            animator.SetBool("run", false);

        if(Input.GetKeyDown(KeyCode.R))
        {
            animator.Rebind();
        }
    }


}
