﻿using UnityEngine;

public abstract class Weapon_Basis : MonoBehaviour {
    public Weapon weapon;
    public Rune rune;
    public WeaponItem item { get; private set; }
    //public int strength { get; protected set; }
    protected int ulfWeapon;
    protected int myID;
    public abstract void Attack(bool attack_);
    public abstract void Attack();
    protected bool activated;
    protected float missChance;


    private void OnEnable()
    {
        EventManager.AddListener("Death", DisableWeapon);
        EventManager.AddListener("RuneSet", SetChanceMiss);
        activated = true;

    }
    private void OnDisable()
    {
        EventManager.RemoveListener("Death", DisableWeapon);
        EventManager.RemoveListener("RuneSet", SetChanceMiss);

    }
    public void SetItem(WeaponItem weaponItem_)
    {
        item = weaponItem_;
    }
    public void SetChanceMiss(BecomeEvent BE)
    {
        if (BE.power != (int)Rune.TypeRune.blind)
            return;
        if (BE.come && ulfWeapon > 0 && BE.id == 0)
        {
            //Debug.Log("Miss chance");
            missChance = .5f;
        }
        else if (ulfWeapon > 0 && BE.id == 0 && !BE.come)
            missChance = 0;

    }

    public void Deactivate()
    {
        activated = false;
    }
    protected virtual void DisableWeapon(BecomeEvent BE)
    {
        if(BE.come && BE.id == myID)
        {
            //Debug.Log("disable weapon: " + gameObject.name);
            if(!gameObject.GetComponent<Movement_basis>())
                gameObject.SetActive(false);
        }
    }
    public void ChangeStrength(int plus_)
    {
        strengthChange(plus_);
    }

    protected void strengthChange(int plus)
    {
        //strength += plus;
        item.strength += plus;
        bool leftHand = ulfWeapon == 1 ? true : false;
        if (item.strength <= 0)
            item.strength = 0;
        if(ulfWeapon > 0)
            EventManager.Invoke("Weapon strength", new BecomeEvent(leftHand, 0, item.strength));
    }

    public void initID(int id)
    {
        myID = id;
    }
    public void IsUlfWeapon(int hand)
    {
        ulfWeapon = hand;
    }
}

[System.Serializable]
public class Weapon
{
    public enum Type { hand, slash, blunt, spear, dagger, melee_magic, bow, magic, buff, shield, fly_axe };
    public Type type_weapon;
    public float Recharge, Distance, minDistance;
    public int Power, Priority;
    public string tagWeapon;
    private bool isRecharge;

    public Weapon()
    {
        isRecharge = false;
    }

    public void Recharging(bool start)
    {
        isRecharge = start;
    }
    public bool Recharging()
    {
        return isRecharge;
    }

    public bool IsMagic()
    {
        int weapType = (int)type_weapon;
        //SHIELD
        if (weapType > 4 && weapType != 6 && weapType != 9)
            return true;
        else
            return false;
    }

    public bool IsSHield()
    {
        if ((int)type_weapon == 9)
            return true;
        else
            return false;
    }

    public bool IsMelee()
    {
        if ((int)type_weapon < 6)
            return true;
        else
            return false;
    }
    public bool IsBow()
    {
        if ((int)type_weapon == 6)
            return true;
        else
            return false;
    }
    public bool Is2Handed()
    {
        if ((int)type_weapon == 6)
            return true;
        else
            return false;
    }
}
[System.Serializable]
public class Rune
{
    public enum TypeRune { empty, root, blind, stun, fire }
    public TypeRune RuneType;
    //public float Time;
    public float PeriodTime;
    public float Damage;
    public Rune()
    {
        RuneType = TypeRune.empty;
    }
}