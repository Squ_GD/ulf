﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lootObject : MonoBehaviour {
    public Item lootItem1 { get;  set; }
    public Item lootItem2 { get;  set; }

    public Item lootItem3 { get;  set; }
    public int planetType, EnemyType;

    public void SetItems(LootList loot)
    {
        if(loot.drop1 != "" && loot.stuck1 != 0)
        {
            lootItem1 = Inventory.inventory.GetSprItem(loot.drop1);
            if(lootItem1 is CraftItem)
                ((CraftItem)lootItem1).SetStuck(loot.stuck1);
            else if (lootItem1 is MissileItem)
                ((MissileItem)lootItem1).ChangeCount(loot.stuck1);
        }
        if (loot.drop2 != "" && loot.stuck2 != 0)
        {
            lootItem2 = Inventory.inventory.GetSprItem(loot.drop2);
            if (lootItem2 is CraftItem)
                ((CraftItem)lootItem2).SetStuck(loot.stuck2);
            else if (lootItem2 is MissileItem)
                ((MissileItem)lootItem2).ChangeCount(loot.stuck2);
        }
        if (loot.drop3 != "" && loot.stuck3 != 0)
        {
            lootItem3 = Inventory.inventory.GetSprItem(loot.drop3);
            if (lootItem3 is CraftItem)
                ((CraftItem)lootItem3).SetStuck(loot.stuck3);
            else if (lootItem3 is MissileItem)
                ((MissileItem)lootItem3).ChangeCount(loot.stuck3);
        }
    }
    public bool IsEmpty()
    {
        if (lootItem1 == null && lootItem2 == null && lootItem3 == null)
            return true;
        else
            return false;
    }
}
