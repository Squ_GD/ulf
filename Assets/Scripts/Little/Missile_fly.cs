﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Missile_fly : MonoBehaviour {
    public float speedFly, fallRatio;
    private float boneFly, impulse, impulseCompens;// flyDist;
    public float destroyTime;
    public Sprite stuckSprite;
    public SpriteMask tracerMask, stuckMask;
    public Transform Explosive;
   // private int damage;
    private Weapon weaponFrom;
    private bool inTheGround;
    private float ScaleX;
    private Vector3 startVectorRotate, planetCenter;
    private string throwerTag;
    private CapsuleCollider2D myCollider;
    public Transform tracer;
    private Transform tracerBone, firstBone;
    private Vector3 maskDefaultScale = new Vector3(1, 1, 1);

    private void OnDisable()
    {
        
    }

    private void OnEnable()
    {
        if (inTheGround)
            ResetMissile();

        //flyDist = 0;
        if (tracer)
        {
           // tracer = transform.GetChild(0);
            tracer.position = transform.position;
            firstBone = tracerBone = tracer.GetChild(1).GetChild(0);
            boneFly = 0;
            tracer.parent = null;

        }
        
        //Debug.Log("Enable");
    }

    void Start() {
        ScaleX = transform.localScale.x;

        //distanceFly = weaponFrom.Distance;

        startVectorRotate = transform.right;
        inTheGround = false;
        myCollider = GetComponent<CapsuleCollider2D>();



    }

    private void TouchThePlanet()
    {

    }

    public void SetStartParam(string _tag, Weapon _weapon, Vector3 _planCent, float startImpulse_)
    {

        throwerTag = _tag;
        weaponFrom = _weapon;
       // damage = weaponFrom.Power;
        planetCenter = _planCent;
        impulse = startImpulse_;
        impulseCompens = 23f;
        //distanceFly = _distanceFly;
    }
    //For shield
    public Weapon GetWeapon()
    {
        return weaponFrom;
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (!myCollider)
            return;
        float scaleX = transform.localScale.x;
        Vector3 contactX = transform.position + transform.right * myCollider.offset.x * scaleX + transform.up * myCollider.offset.y * scaleX;
        //Debug.Log("Contact missile: " + collider.name);
        Stick(collider, contactX);
    }

    public void Stick(Collider2D collider, Vector3 myColliderContact)
    {
        if (collider.tag == "lootable" || collider.tag == "Sound")
            return;
        if (collider.tag != throwerTag && collider.tag != weaponFrom.tagWeapon) //"Player")
        {
            // tracer.gameObject.SetActive(false);
            Debug.Log("Collide: " + collider.tag + " myTag: "+ throwerTag);
            if(stuckMask)
            {
                stuckMask.gameObject.SetActive(true);
            }

            inTheGround = true;
            Transform stick;
            stick = collider.transform;
            if (collider.tag == "Player" || collider.tag == "enemy")
            {
                collider.GetComponent<Battle_Basis>().Attacking(weaponFrom, myColliderContact);
                stick = collider.GetComponent<Battle_Basis>().Get_StickFor_Missle();
            }
            if (!Explosive)
            {

                transform.parent = stick;
            }
            else
            {
                //GameObject explosion = transform.Find("explosion").gameObject;
                Explosive.gameObject.SetActive(true);
                Explosive.transform.parent = null;
            }

            if (stuckSprite)
            {
                GetComponent<SpriteRenderer>().sprite = stuckSprite;
            }

            StartCoroutine(DestroyTimer());
            if (tracerMask)
            {
                tracerMask.transform.parent = null;
                StartCoroutine(MaskCollapse());
            }
            if(GetComponent<Rigidbody2D>())
                GetComponent<Rigidbody2D>().simulated = false;
        }
    }

    IEnumerator MaskCollapse()
    {
        tracerMask.transform.localScale = maskDefaultScale;

        float scale = tracerMask.transform.localScale.x;
        for(int c = 0; c < 10; c++)
        {
            scale -= .1f;
            yield return new WaitForSeconds(.05f);
            tracerMask.transform.localScale = new Vector3(scale, scale, 1);
            //Debug.Log("Scale tracer: " + scale);
        }
    }

    IEnumerator DestroyTimer()
    {
        yield return new WaitForSeconds(destroyTime);
        if (tracer)
        {
            //while(tracer.parent)
            //{
              //  tracer = tracer.parent;
            //}
            //Destroy(tracer.gameObject);
        }
        SpriteRenderer SR = GetComponent<SpriteRenderer>();
        if (!SR)
            SR = transform.GetChild(0).GetComponent<SpriteRenderer>();
        if (!Explosive && SR)
        for(float f = 1; f >= 0; f -= .1f)
        {
            yield return new WaitForSeconds(.02f);
            SR.color = new Color(1, 1, 1, f);
        }
        //Destroy(gameObject);
        SR.color = new Color(1, 1, 1, 1);
        ResetMissile();

        
        gameObject.SetActive(false);
    }

    private void ResetMissile()
    {

        if (tracerMask)
        {
            tracerMask.transform.parent = transform;
            tracerMask.transform.localPosition = Vector3.zero;
            tracerMask.transform.localScale = maskDefaultScale;
        }
        inTheGround = false;

        transform.parent = null;
        if (tracerMask)
        {
            tracerMask.transform.localScale = maskDefaultScale;

        }
        if (tracer)
        {
            resetTracer();
        }
        if (stuckMask)
        {
            stuckMask.gameObject.SetActive(false);
        }
        if (GetComponent<Rigidbody2D>())
            GetComponent<Rigidbody2D>().simulated = true;

        gameObject.SetActive(false);
    }
    // Update is called once per frame
    void Update()
    {
        if (inTheGround)
            return;

        float _velocity = (speedFly + impulse) * Time.fixedDeltaTime;
        transform.position += transform.right * _velocity;        
        float deltaVelocity = _velocity * (1f / ScaleX);
        //flyDist += _velocity;
        Vector3 currentRotVect = transform.right;
        Vector3 toCenterVect = planetCenter - transform.position;
        float deltaAngle = Vector3.Angle(toCenterVect, currentRotVect);

        float relationDeg = (impulseCompens - impulse) * fallRatio * (deltaAngle < 60f ? .5f : 1) * (deltaAngle < 45f ? .3f : 1);

        if (impulse > 0)
            impulse -= .1f;
        if(impulseCompens > 7f)
        impulseCompens -= .15f;
        bool revertFly = false;

        

        if (transform.InverseTransformPoint(planetCenter).y > 0)
            revertFly = true;

        if (revertFly)
            transform.Rotate(Vector3.forward, relationDeg);
        else
            transform.Rotate(-Vector3.forward, relationDeg);
        
            if(tracerBone)
            {
                //transform.InverseTransformDirection(tracerBone.right);
            //float tracerRotate = Vector3.Angle(transform.right, tracerBone.right);
                boneFly += deltaVelocity;
                if (boneFly >= 1f)
                {
                    boneFly -= 1f;
                if (tracerBone.childCount > 0)
                    tracerBone = tracerBone.GetChild(0);
                else
                {
                    for (int i = 0; i < 5; i++)
                    {
                        tracerBone = tracerBone.parent;
                    }
                    firstBone.rotation = tracerBone.GetChild(0).rotation;
                    Vector3 intent = transform.position - tracerBone.parent.position;
                    firstBone.position += intent;
                }
                   /* else
                    {
                        GameObject newTracer = (GameObject)Instantiate(tracer.gameObject);
                        newTracer.transform.parent = tracer;
                        tracer = newTracer.transform;
                        tracer.localScale = new Vector3(1, 1, 1);
                        //tracerBone = tracer.GetChild(1).GetChild(0);
                        //resetTracer();
                    }*/
                }
                changeTracer();
            }
        
    }

    private void resetTracer()
    {
        tracer.parent = transform;
        tracer.localPosition = Vector3.zero;
        tracer.localRotation = Quaternion.identity;
        tracer.localScale = new Vector3(1, 1, 1);

        Transform _tracerbone = tracer.GetChild(1).GetChild(0);
        while (_tracerbone.childCount > 0)
        {
            _tracerbone.position = _tracerbone.parent.position + _tracerbone.parent.right;
            _tracerbone.localRotation = Quaternion.Euler(Vector3.zero);
            _tracerbone = _tracerbone.GetChild(0);
        }
    }

    private void changeTracer()
    {


        tracerBone.position = transform.position;
        tracerBone.rotation = transform.rotation;
    }

}
