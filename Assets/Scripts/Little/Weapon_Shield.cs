﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Weapon_Shield : Weapon_Basis {
    private CapsuleCollider2D shieldCollider;
    private GameObject firstPart, secondPart;
    public CapsuleCollider2D masterCollider;
    private AudioSource audioPlayer;

    // Use this for initialization
    void Start () {
        firstPart = transform.GetChild(0).gameObject;
        secondPart = transform.GetChild(1).gameObject;

        shieldCollider = secondPart.transform.GetChild(0).GetComponent<CapsuleCollider2D>();
        if (ulfWeapon > 0)
            strengthChange(0);

        audioPlayer = gameObject.AddComponent<AudioSource>();
        audioPlayer.rolloffMode = AudioRolloffMode.Custom;
        audioPlayer.maxDistance = 100;
        audioPlayer.playOnAwake = false;
    }

    private void IgnoreUlfCollider()
    {

        if (!masterCollider)
            masterCollider = Control_Ulf.ulf_static.GetComponent<CapsuleCollider2D>(); 
        Physics2D.IgnoreCollision(masterCollider, shieldCollider, true);
    }

    public override void Attack()
    {

    }

    public override void Attack(bool attack_)
    {

        if (attack_)
        {
            Debug.Log("Attack shield");
            IgnoreUlfCollider();
            secondPart.SetActive(true);
            firstPart.SetActive(false);
        }
        else
        {
            Debug.Log("Attack shield over");

            secondPart.SetActive(false);
            firstPart.SetActive(true);
        }
        //ActivateCollider(!myCollider.enabled);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Debug.Log("Shield collision: "+collision.gameObject.name);
    }
    protected virtual void ShieldContact(Collider2D collider)
    {
        if (collider.tag == "weapon" ) //!= weapon.tagWeapon)
        {
            //return;
            Weapon EnemyWeapon;// = collider.GetComponent<Weapon_Basis>().weapon;
            //AudioClip audioClip = Resources.Load<AudioClip>("Sound\\General\\shield_wooden");
            if (collider.GetComponent<Weapon_Basis>())
            {
                Debug.Log("Shield trigger WEAPON");
                EnemyWeapon = collider.GetComponent<Weapon_Basis>().weapon;
                collider.GetComponent<Weapon_Basis>().Deactivate();
                //collider.GetComponent<Weapon_Basis>().GetComponent<CapsuleCollider2D>().enabled = false;
                ParticlePlay();
                //audioPlayer.PlayOneShot(audioClip);
                if (ulfWeapon > 0)
                {
                    strengthChange(-1);
                }
                else
                    EnemyShieldStrengthChange(-1);
            }
            else if (collider.GetComponent<Missile_fly>())
            {
                //Debug.Log("Shield trigger STICK");
                collider.GetComponent<Missile_fly>().Stick(collider, transform.position);
                collider.transform.parent = transform;
                EnemyWeapon = collider.GetComponent<Missile_fly>().GetWeapon();
                ParticlePlay();
                //ParticlePlay();
                //audioPlayer.PlayOneShot(audioClip);
                if (ulfWeapon > 0)
                {
                    strengthChange(-1);
                }
                else
                    EnemyShieldStrengthChange(-1);
            }
            else
                return;

            //int powerEnemyWeapon = EnemyWeapon.Power;

            
            // if (strength < 0)
            //   Debug.LogError("Strength of shield: " + strength);
        }

    }

    private void EnemyShieldStrengthChange(int strPlus)
    {
        item.strength += strPlus;
        if(item.strength <= 0)
        {
            masterCollider.GetComponent<Battle_enemy>().ShieldBreak();
        }
    }
    private void ParticlePlay()
    {

        GameObject particleGO = (GameObject)Instantiate(Resources.Load("Tooling\\particle_wooden"));
        particleGO.transform.position = transform.position;
        particleGO.transform.rotation = transform.rotation;

       
        ParticleSystem PS = particleGO.GetComponent<ParticleSystem>();

        //SetParticle(PS, _weapon.type_weapon);
        PS.Play();
    }

    public void OnTriggerEnter2D(Collider2D collider)
    {
        Debug.Log("Shield contact");
        ShieldContact(collider);

    }
}