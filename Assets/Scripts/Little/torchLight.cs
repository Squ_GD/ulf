﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class torchLight : MonoBehaviour
{
    int intenseCycle = 0;
    float startIntense = .75f;
    float intensePower = .01f;
    private Light light;
    // Start is called before the first frame update
    void Start()
    {
        light = GetComponent<Light>();
        StartCoroutine(lightStart(.02f, .1f));
    }

    IEnumerator lightStart(float _seconds, float _plus)
    {
        light.intensity = 0;
        while(light.intensity < startIntense)
        {
            light.intensity += _plus;
            yield return new WaitForSeconds(_seconds);
        }
        StartCoroutine(lightFlicker());
    }

    IEnumerator lightFlicker()
    {
        while(true)
        {
            if (++intenseCycle > 15)//new System.Random().Next(10,15))
            {
                intenseCycle = 0;
                intensePower *= -1f;
            }

            light.intensity += intensePower;
            yield return null;
        }
    }
}
