﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon_Range : Weapon_Basis {
    private float bowStringTension = 1f; //Change from animator
    public float missileStartImpulse;
    public bool tempRevertMissile;
    public string missile_name;
    private Transform Missile_Hand_Transform;
    protected Vector3 planetCenter;
    //protected float opponentDistance;
    protected GameObject missile;
    protected bool shoot;
    protected float Scale_missile;

    // Use this for initialization
    void Awake()
    {
        Scale_missile = 1;
        Missile_Hand_Transform = transform;

    }
    private void Start()
    {
        //if (ulfWeapon > 0)
          //  gameObject.name = "arrow";
    }

    public void ChangeBowStringTension(float plus_)//from anim_bow_PowerShoot
    {
        bowStringTension += plus_;
        if (bowStringTension > 1f)
            bowStringTension = 1f;
        else if (bowStringTension < .1f)
            bowStringTension = .1f;
    }

    protected override void DisableWeapon(BecomeEvent BE)
    {
        
        return;
    }
    public override void Attack(bool attack_)
    {

    }

    public override void Attack()
    {
        shoot = true;

        //if (ulfWeapon > 0)
          //  strengthChange(-1);
        switch (weapon.type_weapon)
        {
            case Weapon.Type.bow:
                //string missileName = gameObject.name;
                missile = MissileManager.missileManager.GetObject(missile_name); //Resources.Load<GameObject>("Weapons\\Missiles\\" + missileName);
                if(!missile)
                    Debug.LogError("wrong missile");
                missile.transform.localScale = new Vector3(Scale_missile, Scale_missile, 1);
                missile.transform.position = Missile_Hand_Transform.position;
                missile.transform.rotation = Missile_Hand_Transform.rotation;
                

                shoot = false;
                break;
            default:
                //missile = null;
                //Debug.LogError("Set a weaponType in Animation Event");
                break;
        }
        Missile_fly _missile = missile.GetComponent<Missile_fly>();
        _missile.SetStartParam(gameObject.tag, weapon, planetCenter, bowStringTension * missileStartImpulse);
        tempRevertMissile = false;
        _missile.gameObject.SetActive(true);
    }

    public void ChangePlanetCenter(Vector3 newCenter_)
    {
        planetCenter = newCenter_;
    }


    public void setParameters(Vector3 planet_center, float distToOpponent, float _scale_missile)
    {
        Scale_missile = _scale_missile;
        planetCenter = planet_center;
        /*if (distToOpponent > 0)
            opponentDistance = distToOpponent + 1f;
        else
            opponentDistance = 5f;
        */
    }

    //handle sounds
    public void SoundPlay(string clipName)
    {
        FMODUnity.RuntimeManager.PlayOneShotAttached(clipName, GetComponent<Transform>().gameObject);
    }
}
