﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Weapon_Hybrid : Weapon_Melee {
    public GameObject ThrowingWeapon;
    public float distFly;
    //private Transform ulfTransform;

    public void Throw(Vector3 PC, float speed, float rotateAngle, Collider2D col_, Transform ulfTransform)
    {
        
        //ulfTransform = Control_Ulf.ulf_static.transform;
        if (ThrowingWeapon)
        {
            ThrowingWeapon.transform.rotation = ulfTransform.rotation;
            //float rotateAngle = chooseAngleForThrow();
            //Debug.Log("Angle: " + rotateAngle);
            ThrowingWeapon.transform.Rotate(Vector3.forward, rotateAngle);
            ThrowingWeapon.SetActive(true);
            AxeFly axe = ThrowingWeapon.GetComponent<AxeFly>();
            axe.SetWeapon(weapon);
            axe.SetSpeed(speed);
            axe.SetPlanet(PC);
            axe.SetIgnoredCollisions(col_);
            ThrowingWeapon.transform.parent = null;
            strengthChange(-item.strength);
        }


    }
    public void ThrowDisconnect()
    {
        ThrowingWeapon.transform.parent = null;

    }

    public float chooseAngleForThrow()
    {
        Vector3 mouseV = Input.mousePosition;
        mouseV.z = 10;
        Vector3 mouseVector =  Camera.main.ScreenToWorldPoint(mouseV);
        bool mousePointIsHight = ThrowingWeapon.transform.InverseTransformPoint(mouseVector).y > 0;

        //Debug.Log("Mouse: " + mouseVector);
        float angleTReturn = Vector3.Angle(-ThrowingWeapon.transform.right, ThrowingWeapon.transform.position - mouseVector);

        if (angleTReturn > 75)
            angleTReturn = 75;
        if (angleTReturn < -30)
            angleTReturn = -30;


        if (mousePointIsHight)
            return angleTReturn;
        else
            return -angleTReturn;


    }

}