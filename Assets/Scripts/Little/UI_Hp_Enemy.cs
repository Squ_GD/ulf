﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_Hp_Enemy : MonoBehaviour {
    private Animator _animator;
    private int EnemyOverID, EnemyAgrID, EnemyType, EnemyHeal;
    private float visibleTime = 2f, tempVisTime;

    private bool visible, mouseStay, isLook, isAgressive;
    public Transform[] hpList;
    //AudioSource audioSource;
    // Use this for initialization
    private void OnEnable()
    {
        //audioSource = GetComponent<AudioSource>();
        tempVisTime = visibleTime;
        EventManager.AddListener("MouseOver", IsMouseOver);
        EventManager.AddListener("TakeDamage", damaged);
        EventManager.AddListener("Death", enemyDeath);
        EventManager.AddListener("EnemyLook", ulfLook);
        EventManager.AddListener("Planet stand", planetLeaveUlf);
        EnemyOverID = -1;
        isLook = false;
    }
    private void setVisible(int _heal, int _type, bool _visible)
    {
        //Debug.Log("Visible: " + _visible + " _type: " + _type);
        for (int h = 0; h < hpList.Length; h++)
        {
            if (h < _heal)
            {
                Transform HP = hpList[h].GetChild(_type);

                HP.gameObject.SetActive(_visible);
                if(!_visible)
                {
                    for (int t = 0; t < HP.childCount; t++)
                    {
                        HP.GetChild(t).localScale = Vector2.zero;
                    }
                }
                /*hpList[h].gameObject.SetActive(true);
                for (int t = 0; t < hpList[h].childCount; t++)
                {
                    if (t == _type)
                    {
                        hpList[h].GetChild(t).GetComponent<Animator>().Rebind();
                        hpList[h].GetChild(t).gameObject.SetActive(true);
                    }
                    else
                    {
                        hpList[h].GetChild(t).GetComponent<Animator>().Rebind();
                        hpList[h].GetChild(t).gameObject.SetActive(false);
                    }
                }*/
            }
            //else
            //hpList[h].gameObject.SetActive(false);

        }
    }

    private void enemyDeath(BecomeEvent BE)
    {
        //Debug.Log("Death: " + BE.id + " EnemyOverID: "+ EnemyOverID);
        if(BE.id == EnemyOverID)
        {
            StopAllCoroutines();
            setVisible(6, EnemyType, false);

            EnemyOverID = -1;
            visible = false;
            _animator.SetBool("visible", false);
            EventManager.Invoke("EnemyHP Visible", new BecomeEvent(false, 1, 0));
        }
    }

    public void StayMouse(bool enable_)
    {
        //Debug.Log("Over: " + enable_);
        mouseStay = enable_;
    }


    private void damaged(BecomeEvent BE)
    {
        if (BE.id != EnemyOverID)
            return;
        int damageBuffer = BE.power;
        int countHeals = hpList.Length-1;
        while(countHeals >= EnemyHeal - BE.power && countHeals >= 0)
        {
            hpList[countHeals--].GetChild(EnemyType).GetComponent<Animator>().SetTrigger("damage");
        }
        EnemyHeal -= BE.power;

    }

    private void planetLeaveUlf(BecomeEvent BE)
    {
        if (!BE.come)
            ulfLook(BE);
    }

    private void ulfLook(BecomeEvent BE)
    {

        if (BE.come)
            visibleHPEnemy(BE, false);
        else if(isLook)
        {

            StopAllCoroutines();
            StartCoroutine(WhileVisible());

            EnemyOverID = -1;
            visible = false;
        }
        isLook = BE.come;

    }
    private void agressive(BecomeEvent BE)
    {
        Debug.Log("Argessive: " + BE.come);
        isAgressive = BE.come;
        if(!isLook)
            visibleHPEnemy(BE, false);
    }
    private void IsMouseOver(BecomeEvent BE)
    {
        //Debug.Log("MouseOver: " + BE.come);
        //if (!isLook && !isAgressive)
            visibleHPEnemy(BE, true);
    }
    private void visibleHPEnemy(BecomeEvent BE, bool isMouse)
    {

        if (BE.come)
        {
            visible = true;
            _animator.SetBool("visible", true);
            EventManager.Invoke("EnemyHP Visible", new BecomeEvent(true, 1, 0));
            if (BE.id == EnemyOverID)
                return;
           int _enemyHeal = BE.power % 10;
           int _enemyType;
           //Debug.Log("Heal: " + _enemyHeal);
           if (BE.power < 10)
                _enemyType = 0;
            else
                _enemyType = (BE.power - _enemyHeal) / 10;

            //if (EnemyOverID != -1)
            //{

            //}

            if (EnemyOverID == -1 || isMouse)
            {
                setVisible(6, EnemyType, false);
                EnemyOverID = BE.id;
                //Debug.Log("heal: " + _enemyHeal + " type: " + _enemyType);
                setVisible(_enemyHeal, _enemyType, true);
                EnemyHeal = _enemyHeal;
                EnemyType = _enemyType;
            }
        }
        else 
        {
            if (visible)
            {
                StopAllCoroutines();
                StartCoroutine(WhileVisible());
            }
            else
                EnemyOverID = -1;
            // stopVisible = true;
            visible = false;
        }

    }

    IEnumerator WhileVisible()
    {
        while (visible || (mouseStay && EnemyOverID != -1))
        {
            //visible = false;
            yield return new WaitForSeconds(visibleTime);
        }
        setVisible(EnemyHeal, EnemyType, false);
        _animator.SetBool("visible", false);
        EventManager.Invoke("EnemyHP Visible", new BecomeEvent(false, 1, 0));
        EnemyOverID = -1;
    }
    void Start () {
        _animator = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
