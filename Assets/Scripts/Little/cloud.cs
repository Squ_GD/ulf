﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cloud : MonoBehaviour {
    public float rotateSpeed;
    private float summaryInDirect, maxInDirect;
    private int direct;
    private bool withParent, withChild;
    private float acceleration = .2f;

    private void OnEnable()
    {
       // if (transform.parent && transform.parent.tag != "Folder")
       //     withParent = true;
       // else
       //     withParent = false;

        if (transform.childCount > 0)
        {
            withChild = true;
        }
        else
            withChild = false;
    }

    // Use this for initialization
    void Start () {
        // rotateSpeed = .5f;
        //rotateSpeed -= transform.childCount / 10f;
        acceleration = rotateSpeed / 2f;
        maxInDirect = .8f;
        maxInDirect += rotateSpeed;
        summaryInDirect = maxInDirect / 2f;
        direct = 1;
        if (withChild)// && !withParent)
            changeDirectChildrens();
    }
    public void ChangeDirect(int dir)
    {
        while(transform.parent.tag != "Folder")
            transform.parent = transform.parent.parent;
        direct = dir;
        //summaryInDirect = 0;
        if (withChild)
            changeDirectChildrens();
    }
    // Update is called once per frame
    void Update() {
        if(summaryInDirect >= maxInDirect/2f)
        {
            rotateSpeed -= Time.deltaTime * acceleration;
        }
        else
            rotateSpeed += Time.deltaTime * acceleration;
        if (rotateSpeed < .1f)
            rotateSpeed = .1f;
        if (summaryInDirect < maxInDirect)// || withParent)
        { 
            transform.Rotate(Vector3.forward * direct, Time.deltaTime * rotateSpeed);
            summaryInDirect += Time.deltaTime * rotateSpeed;
        }
        else //if(!withParent)
        {
            summaryInDirect = 0;
            direct *= -1;

            //if (withChild)
              //  changeDirectChildrens();
        }
    }

    private void changeDirectChildrens()
    {
        for(int c = 0; c < transform.childCount; c++)
        {
            transform.GetChild(c).GetComponent<cloud>().ChangeDirect(-direct);
        }
    }
}
