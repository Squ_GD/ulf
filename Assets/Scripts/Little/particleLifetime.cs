﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class particleLifetime : MonoBehaviour {

	// Use this for initialization
	void Start () {
        float timeOfLife = GetComponent<ParticleSystem>().main.startLifetime.constantMax;
        StartCoroutine(lifeTime(timeOfLife));
    }
	
    IEnumerator lifeTime(float time)
    {
        yield return new WaitForSeconds(time);
        gameObject.SetActive(false);
    }
}
