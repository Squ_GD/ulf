﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Planet : MonoBehaviour {

    public List<GameObject> bridges;
    private const float ulf_bridge_step_wide = 12f, ulf_step = .5f;//5f диапазон
    private GameObject lastBridge;
    protected List<Bridge_Point> bridgePoints = new List<Bridge_Point>();

    private Dictionary<int,Transform> enemyList;
    private PlanetSound planetSound;
    private List<Control_Ulf> multiUlfs;
    private void Awake()
    {

        enemyList = new Dictionary<int, Transform>(10);
        multiUlfs = new List<Control_Ulf>();
    }

    private void OnEnable()
    {

        bridgePoints = new List<Bridge_Point>();
        bridgePoints_Calc();
        //Planets Layer
        gameObject.layer = 8;

        planetSound = GetComponent<PlanetSound>();
        
    }
    void Start () {

        //ulfTransform = Control_Ulf.ulf_static;
        //ulf_deg_new = ulf_deg_old  = -1;
        if(Converter.converter)
        {
            Converter.converter.RegisterPlanet(this);
        }
    }

    

    public float Get_Random_Degree(Control_Ulf ulf)
    {
        float newDeg = new System.Random().Next(359);
        ulf.SetDegree(newDeg);
        Stand_To_Planet(ulf);
        ulf.Set_New_Planet(this);
        return newDeg;
    }
    

    public void AddEnemy(int _id, Transform enemyTransform)
    {
        enemyList.Add(_id, enemyTransform);

    }
    public void RemoveEnemy(int enemyID)
    {
        if (enemyID == 0 || enemyList.Count == 0)
            return;
        /*foreach(int i in enemyList.Keys)
        {
            Debug.Log("ID: " + i);
        }
        Debug.Log("Planet: " + gameObject.name + " ID: " + BE.id);
        */
        if(enemyList[enemyID])
            enemyList[enemyID].GetComponent<Battle_enemy>().UlfLook(false);
        enemyList.Remove(enemyID);

    }
    //From control ulf
    public void CheckEnemyList(Transform ulfLook)
    {
        if (enemyList.Count == 0)
        {
            //Debug.Log("Count 0");
            return;
        }
        Transform _transMinDist = null;// = enemyList[0];
        float minDist = 3f;

        foreach (Transform _enemy in enemyList.Values)
        {
            float _dist = (ulfLook.position - _enemy.position).magnitude;
            float look = Vector3.Angle((_enemy.position - ulfLook.position), -ulfLook.right);
            if (look < 120f)
            {
                _dist *= .7f;
            }
                if (_dist < minDist)
                {
                    _transMinDist = _enemy;
                    minDist = _dist;
                }
        }

        if (_transMinDist != null)
        {

            _transMinDist.GetComponent<Battle_enemy>().UlfLook(true);
        }
        else
        {
            EventManager.Invoke("EnemyLook", new BecomeEvent(false, 0, 0));
        }


    }

    public List<float> GetBridgesPoints()
    {
        if (bridgePoints.Count == 0)
            return null;
        List<float> vs = new List<float>();
        foreach(Bridge_Point bp in bridgePoints)
        {
            vs.Add(bp.degStart);
        }
        return vs;
    }

    //Расчёт и запись в массив углов, под которыми находятся мосты
    private void bridgePoints_Calc()
    {

        bridgePoints.Clear();

        foreach (GameObject bri in bridges)//(int bri = 0; bri < bridges.Count; bri++)
        {
            if (!bri)
                return;

                Bridge currBridge = bri.GetComponent<Bridge>();
                Vector3 bridgePos = bri.transform.position;
                
                float angleBetween = Get_Object_degree(currBridge.Get_Center());
                Bridge_Point newBP = new Bridge_Point();

                Vector3 vectorToPlanet = (transform.position - bridgePos).normalized;
                float angleRight = Vector3.Angle(bri.transform.right, vectorToPlanet);
            float newAngleEnd = angleBetween;

            if (angleRight >= 179)
            {
                newBP.SetLeft(false);
                newAngleEnd += ulf_bridge_step_wide;
            }
            else
            {
                newBP.SetLeft(true);
                newAngleEnd -= ulf_bridge_step_wide;
            }
            newBP.SetStartDeg(angleBetween);

            if (newAngleEnd >= 360f)
                newAngleEnd -= 360f;
            else if (newAngleEnd < 0)
                newAngleEnd += 360f;

            newBP.SetEndDeg(newAngleEnd);

            bridgePoints.Add(newBP);
        }
       // Debug.Log("bridgePoints: " + bridgePoints[0]);
    }
    //Проверка встал ли на точку моста какого-либо
    protected int Ulf_Stand_Bridge(Control_Ulf ulf)
    {
        UlfDeg deg = ulf.deg;
        

        for(int b = 0; b < bridgePoints.Count; b++)
        {
            float bridgeAngle = bridgePoints[b].degStart;//Начальный угол
            if (bridgeAngle == -1f)
                continue;

            
            float degBetBri = Mathf.Abs(bridgeAngle - deg.new_);
            
            //Минимальное расстояние к мосту, с учётом длины шага
            if(degBetBri <= ulf_step || degBetBri >= 360f - ulf_step)
            {
                //Debug.Log("bridgeAngle: " + bridgeAngle);
                return b;
            }

            bridgeAngle = bridgePoints[b].degEnd;//Конечный угол
            degBetBri = Mathf.Abs(bridgeAngle - deg.new_);
            if (degBetBri <= ulf_step || degBetBri >= 360f - ulf_step)
            {
                //Debug.Log("bridgeAngle: " + bridgeAngle);
                return b;
            }
        }
        return -1;
    }

    //Расчёт угла объекта на планете
    public float Get_Object_degree(Vector3 objectPos)
    {
        Vector3 vecToObject = (objectPos - transform.position).normalized;
        float angleBetween = Vector3.Angle(transform.up, vecToObject);
        if (objectPos.x < transform.position.x)
            angleBetween = 360 - angleBetween;
        return angleBetween;
    }
    //Запрос угла для Ulf'a, с записью в переменную
    public void Get_Object_degree(Control_Ulf ulf)
    {
        ulf.deg.old_ = ulf.deg.new_;
        ulf.deg.new_ = Get_Object_degree(ulf.transform.position);
        ulf.SetDegree(ulf.deg.new_);

        if (ulf.deg.old_ == -1)
            ulf.deg.old_ = ulf.deg.new_;

        if (ulf.lastDir == 0)//Если стоит на месте, не проверяем мосты
            return;

        UlfDeg deg = ulf.deg;
        bool startPoint = false;
        bool endPoint = false;

        int bridgeNum = -1;
        for (int b = 0; b < bridgePoints.Count; b++)
        {
            float bridgeAngle = bridgePoints[b].degStart;//Начальный угол
            if (bridgeAngle == -1f)
                continue;


            float degBetBri = Mathf.Abs(bridgeAngle - deg.new_);
            //Минимальное расстояние к мосту, с учётом длины шага
            if (degBetBri <= ulf_step || degBetBri >= 360f - ulf_step)
            {
                bridgeNum = b;
                startPoint = true;
            }

            bridgeAngle = bridgePoints[b].degEnd;//Конечный угол
            degBetBri = Mathf.Abs(bridgeAngle - deg.new_);
            if (degBetBri <= ulf_step || degBetBri >= 360f - ulf_step)
            {
                bridgeNum = b;
                endPoint = true;
            }
        }

        //int bridgeNum = Ulf_Stand_Bridge(ulf);
        if (bridgeNum > -1)
        {
            Bridge getBridge = bridges[bridgeNum].GetComponent<Bridge>();

            bool enter_bridge_zone_from_start = bridgePoints[bridgeNum].left && ulf.lastDir == -1 || !bridgePoints[bridgeNum].left && ulf.lastDir == 1;
            enter_bridge_zone_from_start &= startPoint;
            bool enter_bridge_zone = bridgePoints[bridgeNum].left && ulf.lastDir == 1 || !bridgePoints[bridgeNum].left && ulf.lastDir == -1;
            enter_bridge_zone &= endPoint;
            ulf.Stand_To_Bridge(getBridge, bridgeNum, enter_bridge_zone_from_start, enter_bridge_zone);

        }
    }
    public void SetDistToUlfSound(float distance_, float widthBridge_)
    {
        if(planetSound)
        {
            distance_ = 1 - distance_ / widthBridge_; //1 + Mathf.Log(1f/distance_, 5f);
            //Debug.Log("sound vol: " + (distance_));
            planetSound.SetSoundVolume(distance_);
        }
    }
    public void SoundOFF()
    {
        if (planetSound)
        {
            Debug.Log("Sound off: " + gameObject.name);
            planetSound.StopPlanetSound();
        }
    }
    public void SoundON()
    {
        if (planetSound)
        {
            Debug.Log("Sound start: " + gameObject.name);
            planetSound.PlayPlanetSound();
        }
    }


    public void Stand_To_Planet(Control_Ulf newUlf)
    {
        if (!multiUlfs.Contains(newUlf))
        {
            multiUlfs.Add(newUlf);
            if (planetSound)
            {
                planetSound.PlayPlanetSound();//CHECK
                planetSound.SetSoundVolume(1f);
            }
        }
        
    }
    public void Set_Ulf_Deg(float newDeg_, Control_Ulf ulf)
    {
        UlfDeg deg = ulf.deg;

        deg.old_ = deg.new_ = newDeg_;
        //ulf_deg_new = Get_Object_degree(ulf.transform.position);
        ulf.SetDegree(deg.new_);
        // Debug.Log("ulf deg: " + ulf_deg_new);
        if (deg.old_ == -1)
            deg.old_ = deg.new_;

    }

    public void Ulf_Leave(Control_Ulf ulf)
    {
        ulf.deg.new_ = ulf.deg.old_ = -1;
        ulf.deg.planet = null;
        //Debug.Log("Ulf leave");
        
    }
    //Запрос угла для моба, с записью в переменную
    public void Get_Object_degree(Control_enemy mob)
    {

        mob.SetDegree(Get_Object_degree(mob.transform.position));
    }
    public void Get_Object_degree(Movement_basis mob)
    {

        mob.SetDegree(Get_Object_degree(mob.transform.position));
    }
    public void Get_Object_degree(TestControl mob)
    {

        mob.SetDegree(Get_Object_degree(mob.transform.position));
    }

    //Добавление моста (Editor)
    public Bridge Add_bridge(bool mirror)
    {
        //planet_count++;
        float radiusPlanet = GetRadius();

        int bestSlot = getBridgeSlot();

        
        GameObject bridgeNew = (GameObject)Instantiate(Resources.Load("bridge"));
        bridgeNew.GetComponent<Bridge>().SetStartPlanet(this);
        if (bestSlot == bridges.Count)
            bridges.Add(bridgeNew);
        else
            bridges[bestSlot] = bridgeNew;

        lastBridge = bridgeNew;
        Bridge_rotate(0, 1, mirror);
        return lastBridge.GetComponent<Bridge>();
    }

    public void RemoveBridge(Bridge _bridge)
    {
        bridges.Remove(_bridge.gameObject);
        bridgePoints_Calc();
    }

    public void Add_Bridge_from_planet(Bridge bridge_new)
    {
        int bestSlot = getBridgeSlot();
        if (bestSlot == bridges.Count)
            bridges.Add(bridge_new.gameObject);
        else
            bridges[bestSlot] = bridge_new.gameObject;
        bridgePoints_Calc();
    }

    private int getBridgeSlot()
    {
        int bridge_slot = 0;
        foreach (GameObject br in bridges)//(int i = 0; i < bridges.Count; i++)
        {
            if (br)
            {
                bridge_slot++;
            }
            else
                break;
        }

        return bridge_slot;
    }
    //Используется из Editor
    public void Bridge_rotate(float deg, int degAbs, bool mirrored)
    {
        float widthCollider = lastBridge.GetComponent<BoxCollider2D>().size.x * lastBridge.transform.localScale.x;
        //Расчитываем точку на окружности
        float radiusRotation = GetRadius() + widthCollider/2;

        if (GetRadius() > 6.8f)
            radiusRotation += .05f;

        float rad = deg / 180 * Mathf.PI;
        float x = radiusRotation * Mathf.Sin(rad) + transform.position.x;
        float y = radiusRotation * Mathf.Cos(rad) + transform.position.y;

        lastBridge.transform.position = new Vector2(x, y);

        //Вычисления угла поворота на планету
        Vector3 vectorToPlanet = (transform.position - lastBridge.transform.position);
        Vector3 bridgeRotateVector = -lastBridge.transform.right;
        if (mirrored)
        {
            bridgeRotateVector *= -1;

            degAbs *= -1;
        }

        float angleToPlanet = Vector3.Angle(bridgeRotateVector, vectorToPlanet);

        lastBridge.transform.Rotate(Vector3.forward, degAbs * angleToPlanet);

        bridgePoints_Calc();

    }
    //Зеркально отразить мост из Editor
    public void Bridge_mirror(bool isMirror)
    {
        lastBridge.transform.Rotate(Vector3.forward, 180);
    }

    public Bridge Get_Bridge_From_Num(int bridgeNum_)
    {
        return bridges[bridgeNum_].GetComponent<Bridge>();
    }

    public float GetRadius()
    {
        return GetComponent<CircleCollider2D>().radius; 
    }

    public float Circumference(float totalDeg)
    {
        float circumTotal = 2 * Mathf.PI * GetRadius();
        return (circumTotal / 360f) * totalDeg;
    }


    private Control_Ulf Much_Closed_Ulf(Transform currTrans)
    {
        Control_Ulf ulf_min_dir = null;
        float minDist = 99999f;
        foreach (Control_Ulf ulf_ctrl in multiUlfs)
        {
            if (!ulf_ctrl.deg.planet || ulf_ctrl.deg.planet != this)//Если не на этой планете
                continue;
            float currDIst = (ulf_ctrl.transform.position - currTrans.position).magnitude;
            if (currDIst < minDist)
            {
                minDist = currDIst;
                ulf_min_dir = ulf_ctrl;
            }
        }

        return ulf_min_dir;
            
    }

    public float Get_Ulf_Deg(Transform currTrans)
    {
        Control_Ulf ulf_ctrl = Much_Closed_Ulf(currTrans);

        if (ulf_ctrl == null)
            return -1;
        else
            return ulf_ctrl.deg.new_;
    }
    //Направление к Ulf
    public bool Get_Ulf_Dir(Transform currTrans)
    {
        Control_Ulf ulf_min_dir = Much_Closed_Ulf(currTrans);

        if (ulf_min_dir == null)
            return false;
        

        Vector3 ulf_pos = ulf_min_dir.transform.position;
        Vector3 transedUlfPos = currTrans.InverseTransformPoint(ulf_pos);

        //return Get_Best_Dir(transedUlfPos, currTrans.position);
        if (transedUlfPos.x < 0)
            return true;
        else
            return false;
    }

    public bool GetReturnDir(Transform currTrans, Vector3 returnPos)
    {
        Vector3 transRetPos = currTrans.InverseTransformPoint(returnPos);
        if (transRetPos.x < 0)
            return true;
        else
            return false;
    }

    // Update is called once per frame
    void Update () {
		
	}
}
public struct Bridge_Point
{
   public float degStart, degEnd;
   public bool left;
    public void SetStartDeg(float _deg)
    {
        degStart = _deg;
    }
    public void SetEndDeg(float _deg)
    {
        degEnd = _deg;
    }
    public void SetLeft(bool _left)
    {
        left = _left;
    }
}