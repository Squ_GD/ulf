﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Runes : MonoBehaviour
{
    private GameObject[] RunesGO;

    private void OnEnable()
    {
        EventManager.AddListener("RuneSet", runeActivate);
    }

    // Use this for initialization
    void Start()
    {
        RunesGO = new GameObject[transform.childCount];
        for (int c = 0; c < transform.childCount; c++)
            RunesGO[c] = transform.GetChild(c).gameObject;

    }

    private void runeActivate(BecomeEvent BE)
    {
        if (RunesGO.Length - 1 < BE.power)
        {
            //Debug.LogError("Rune not set!");
            return;
        }
        if (BE.come)
            startRune(BE.power);
    }

    private void startRune(int numRune)
    {
        if (RunesGO.Length - 1 >= numRune)
        {
            RunesGO[numRune].SetActive(true);
            //Debug.Log("Rune: " + RunesGO[numRune].name);
        }
    }
    private void Update()
    {
        if(Input.GetKeyDown("[1]"))
        {
            startRune(1);
        }
        if (Input.GetKeyDown("[2]"))
        {
            startRune(2);
        }
        if (Input.GetKeyDown("[3]"))
        {
            startRune(3);
        }
        if (Input.GetKeyDown("[4]"))
        {
            startRune(4);
        }
        if (Input.GetKeyDown("[5]"))
        {
            startRune(5);
        }
        if (Input.GetKeyDown("[6]"))
        {
            startRune(6);
        }
    }
}
