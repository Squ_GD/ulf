﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class anim_ulf_attack : StateMachineBehaviour {
    public bool extra_hand;
    public bool shield, bow;
    //public AudioClip audio;
    //public float changeWeight;
    //private float startWeight;
	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {

        if (!extra_hand)
        {
            if (!bow)
                animator.GetComponent<Battle_Ulf>().AttackingMain(1);
            
            animator.GetComponent<Battle_Ulf>().StartRechargeWeapon(0);
            animator.SetBool("altMain", false);
            //animator.ResetTrigger("attackMain");
        }
        else
        {
            if (shield)
            {
                shieldResetStates(animator);
                animator.GetComponent<Battle_Ulf>().AttackingBack(0);
                animator.GetComponent<Battle_Ulf>().SetActiveLeftHand(1);
                //animator.SetInteger("weaponExtra", -1);
                animator.GetComponent<Battle_Ulf>().ResetUsedWeapon(extra_hand);
            }
            else
                animator.GetComponent<Battle_Ulf>().AttackingBack(1);
            animator.GetComponent<Battle_Ulf>().StartRechargeWeapon(1);
            animator.SetBool("altExtra", false);

            //animator.ResetTrigger("attackExtra");
        }


    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
       // float newWeight = animator.GetLayerWeight(layerIndex) - changeWeight;
       // animator.SetLayerWeight(layerIndex, newWeight);
    }
    private void shieldResetStates(Animator animator)
    {
        animator.ResetTrigger("gnawing");
        animator.ResetTrigger("heal");
        animator.ResetTrigger("switchMain");
        animator.ResetTrigger("switchExtra");
        animator.ResetTrigger("attackMain");
        animator.ResetTrigger("attackExtra");
        animator.GetComponent<Battle_Ulf>().ResetUsedWeapon(false);
    }
    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {

        if (!extra_hand)
        {
            //animator.GetComponent<Battle_Ulf>().AttackingMain(0);
        }
        else 
        {
            //if (!shield)
           
            //animator.GetComponent<Battle_Ulf>().AttackingBack(0);
        }
        animator.GetComponent<Battle_Ulf>().ResetUsedWeapon(extra_hand);
        //animator.GetComponent<Battle_Ulf>().AttackingBack(0);
        //animator.SetLayerWeight(layerIndex, startWeight);
    }

    // OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}
}
