﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class anim_attack : StateMachineBehaviour {

    public int ulf_attack_used, tracerStart;
    //Атаковать, не останавливая персонажа
    public bool dontRecharge;
	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        if (animator.GetBool("death"))
        {
            return;
        }
        if(tracerStart > 0)
        {
            int layerN = animator.GetLayerIndex("Tracer");
            //animator.StopPlayback();
            animator.Play("tracer"+tracerStart, layerN);
        }
        //animator.GetComponent<Movement_basis>().Stop_Move(true);

        Battle_Basis battle = animator.GetComponent<Battle_Basis>();

        battle.IsAttacked(true);

        if(!dontRecharge)
            battle.StartRechargeWeapon(ulf_attack_used);
    }

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	//override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	//OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        animator.GetComponent<Battle_Basis>().IsAttacked(false);
        //animator.GetComponent<Movement_basis>().Stop_Move(false);
        if (animator.GetComponent<Battle_enemy>())
            animator.GetComponent<Battle_enemy>().ResetUsedWeapon();

    }

    // OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}
}
