﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class anm_activate : StateMachineBehaviour {
    private float speedCash;
    public bool controlActivate, battleActivate, colliderActivate, speedStop, attackedFalse;
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (speedStop)
        {
            speedCash = animator.gameObject.GetComponent<Movement_basis>().SpeedLinear;
            animator.gameObject.GetComponent<Movement_basis>().SpeedLinear = 0;
            Debug.Log("Speed chash = " + speedCash);
        }
        if (attackedFalse)
        {
            animator.GetComponent<Battle_enemy>().IsAttacked(true);
        }
    }

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	//override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	 //OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        if (controlActivate)
            animator.gameObject.GetComponent<Movement_basis>().enabled = true;
        if (battleActivate)
            animator.gameObject.GetComponent<Battle_Basis>().enabled = true;
        if (colliderActivate)
            animator.gameObject.GetComponent<CapsuleCollider2D>().enabled = true;

        if (speedStop)
        {
            Debug.Log("Speed chash = " + speedCash);
            animator.gameObject.GetComponent<Movement_basis>().SpeedLinear = speedCash;
        }
        if(attackedFalse)
        {
            animator.GetComponent<Battle_enemy>().IsAttacked(false);
        }
    }

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
