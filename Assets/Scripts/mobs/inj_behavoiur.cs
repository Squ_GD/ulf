﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class inj_behavoiur : behavior {
    private int myID;


    private void Start()
    {
        EventManager.AddListener("TakeDamage", damageInj);
        Control = GetComponent<Control_enemy>();
        Battle = GetComponent<Battle_enemy>();
        _animator = GetComponent<Animator>();
        myID = Battle.GetMyID();
        //Debug.Log("injID: " + myID);
    }
    private void damageInj(BecomeEvent BE)
    {
        if(BE.id == myID)
        {
            Battle.LookDistance = 3;
            GetComponent<CapsuleCollider2D>().isTrigger = false;
        }
    }
    // Update is called once per frame
    void Update () {
		
	}
}
