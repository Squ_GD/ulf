﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class executioner_behaviour : behavior {
    private float speedJump = 7f;
    private bool jump;
    float dist;

    public void Jump_Executioner()
    {
       jump = !jump;
       Battle.IsAttacked(false);
        if (jump)
        {

            Control.SetRunFromAnim(speedJump);
            Debug.Log("Speed: " + Control.SpeedLinear);

        }
        else
        {

            Control.SetRunFromAnim(1f/speedJump);
            Debug.Log("Speed: " + Control.SpeedLinear);

        }

    }
    // Update is called once per frame
    void Update () {
		if(jump)
        {
            Control.MoveOver();
        }
    }
}
