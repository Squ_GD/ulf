﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrypLowBehaviour : behavior {
    public Transform circleRotate;
    public GameObject[] Masks;
    private int currentMask;
    private int _frame;

    private void Start()
    {
        for(int i = 0; i < Masks.Length; i++)
        {
            if(Masks[i].activeSelf)
            {
                currentMask = i;
                return;
            }
        }
    }

    private void Update()
    {
        if(_animator.GetBool("move"))
        {
            circleRotate.Rotate(Vector3.forward, -7f * Time.deltaTime);
            if(++_frame > Random.Range(3,6))
            {
                _frame = 0;
                Masks[currentMask].SetActive(false);
                if(++currentMask == Masks.Length)
                {
                    currentMask = 0;
                }
                Masks[currentMask].SetActive(true);
            }
        }
    }
}
