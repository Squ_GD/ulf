﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hivel_bee : Movement_basis {
    private int attackIter = 0;
    public int damage;
    public int iterForDamage;
    private bool ulfContact;
    private Battle_Basis ulfBasis;
	// Use this for initialization
	void Start () {
        set_Planet(planet);
        planet.Get_Object_degree(this);

        Move(lastDir);
        disableShadow(false);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {



        if (collision.tag == "Player" && collision.GetComponent<Battle_Basis>())
        {
            ulfContact = true;
            ulfBasis = collision.GetComponent<Battle_Basis>();
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player" && ulfBasis)
        {
            ulfContact = false;
        }
    }

    // Update is called once per frame
    void Update () {
        
        planet.Get_Object_degree(this);
        Move(lastDir);
        //Debug.Log("HivelBee move: " + planet.name);
        if (ulfContact)
        {
            if(SpeedLinear > 0)
                SpeedLinear -= .2f;


            if (ulfBasis && ++attackIter > iterForDamage)
            {
                attackIter = 0;
                ulfBasis.Attacking(damage, 180);
            }
        }
	}
}
