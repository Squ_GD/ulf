﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rat_archer : behavior {
    private int spearNum = 0;

    public SpriteRenderer spear1, spear2;

    
    public void NextSpear()
    {
        
        _animator.SetInteger("bullet", ++spearNum);
        if(spearNum == 1)
        {
            //Debug.Log("Disable spear1");
            spear1.enabled = false;
        }
        else if(spearNum == 2)
        {
            spear2.enabled = false;
            flipped(true);

        }
    }

    public override void SetDistUlf(float newDistUlf)
    {
        if (newDistUlf == -1)
            return;
        if(lastDistUlf == 0)
        {
            lastDistUlf = newDistUlf;
            return;
        }
        if(newDistUlf > lastDistUlf)
        {
            flipped(false);

        }
        else if (newDistUlf < lastDistUlf -.1f)
        {
            flipped(true);
        }
        lastDistUlf = newDistUlf;
    }
    private void flipped(bool doFlip)
    {
        if (spearNum >= 2 && !doFlip)
            return;
        _animator.SetBool("flipPosition", doFlip);
        Battle.InvertPrioritiesWeapon(doFlip);
        
    }
    // Update is called once per frame
    void Update () {
		
	}
}
