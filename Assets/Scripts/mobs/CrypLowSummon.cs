﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrypLowSummon : Movement_basis {
    public float moveIterations;
   // public int newDir;
    public Transform dartTransform;
	// Use this for initialization
	void Start () {
        set_Planet(planet);
        MoveIter();
        shadow.SetActive(false);
	}
	private void MoveIter()
    {
        for(int m = 0; m < moveIterations; m++)
        {
            planet.Get_Object_degree(this);
            Move(lastDir);
        }
    }

    public void SelfDestroy()
    {
        Destroy(gameObject);
    }
    public void ShootDart()
    {
        Weapon_Range _weapon = dartTransform.GetComponent<Weapon_Range>();
        Missile_fly dart = dartTransform.GetChild(0).GetComponent<Missile_fly>();
        dart.SetStartParam(gameObject.tag, _weapon.weapon, getPlanetCenter(), _weapon.weapon.Distance);
        dart.gameObject.SetActive(true);
        dart.transform.parent = null;
    }
    public void AttackRandom()
    {
        string _attack = "attack" + Random.Range(1, 4);

        int layerNum = GetComponent<Animator>().GetLayerIndex("attack");
        GetComponent<Animator>().Play(_attack, layerNum);
        //GetComponent<Animator>().SetInteger("custom", Random.Range(1, 3));
    }
	
}
