﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class awalro_bullet : Movement_basis {
    
	// Use this for initialization
	void Start () {
        float toPlanetCenter = (transform.position - planet.transform.position).magnitude;
        colliderHeight = toPlanetCenter - planet.GetRadius();

        //planet.Get_Object_degree(this);
       // Move(lastDir);
        set_Planet(planet);
        disableShadow(false);
    }

    // Update is called once per frame
    void Update () {
        planet.Get_Object_degree(this);
        Move(lastDir);	
	}
}
