﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class egger_attacks : MonoBehaviour {
    private Control_enemy moveComponent;
    public float mainAttackPeriod;
    private bool periodAttack;
	// Use this for initialization
	void Start () {
        moveComponent = GetComponent<Control_enemy>();
             
    }

    public void InstEnt()
    {
        GameObject Ent = (GameObject)Instantiate(Resources.Load("Enemy\\Summon\\ent"));
        Ent.transform.position = transform.position;
        //Ent.transform.rotation = transform.rotation;
        Planet _planet = moveComponent.GetPlanet();
        //Ent.GetComponent<Movement_basis>().set
        Ent.GetComponent<Battle_enemy>().IsAttacked(true);
        Ent.GetComponent<Control_enemy>().lastDir = GetComponent<Control_enemy>().lastDir;
        Ent.GetComponent<Control_enemy>().SetPlanet(_planet, true);
        Ent.GetComponent<Control_enemy>().SetStartPos(GetComponent<Control_enemy>().GetStartPos());
        //Ent.GetComponent<Control_enemy>().enabled = false;
        //Ent.GetComponent<Animator>().Play("Riseup");
    }

    public void StartMain(int isStart)
    {
        if (isStart == 1)
        {
            periodAttack = true;
            StartCoroutine(MainAttack());
        }
        else
        {
            periodAttack = false;
           // Debug.Log("period OFF");
        }
        //StopCoroutine(MainAttack());
    }
	
    IEnumerator MainAttack()
    {
        while(periodAttack)
        {
            //battleComponent.TryAttack();
            yield return new WaitForSeconds(mainAttackPeriod);

        }
        //Debug.Log("period exit");

    }
    // Update is called once per frame
    void Update () {
		
	}
}
