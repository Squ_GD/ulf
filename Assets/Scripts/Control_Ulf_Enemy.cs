﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class Control_Ulf_Enemy : Control_Ulf {
    new private void Awake()
    {
        //MultiPlayer = true;
    }
    new private void OnDestroy()
    {

    }
    new protected IEnumerator checkForLookEnemy()
    {
        yield return null;
    }


    public override void Set_New_Planet(Planet planet_new)
    {
        deg.bridge = null;

        deg.planet = planet_new;
        deg.planet.Stand_To_Planet(this);
        set_Planet(deg.planet);

        Move(0);
        Debug.Log("Enemy stand planet");

        gameObject.GetComponent<SortingGroup>().enabled = false;

    }

    public void CorrectDegree(float newCorrectDeg_)
    {
        Debug.Log("Before deg: " + deg.old_);
        deg.new_ = deg.old_ = newCorrectDeg_;
        Debug.Log("Set correct deg: " + deg.old_);
    }

    public void AttackAlt(bool isTrue)
    {
        animator.SetBool("alt", isTrue);
    }

    public void Set_Bridge(int numBridge_)
    {
        Debug.Log("Stand bridge: " + numBridge_);
        Stand_Bridge_Inner(deg.planet.Get_Bridge_From_Num(numBridge_));
    }

    public void Set_Old_Direct(float direct_)
    {
        oldDirect = direct_;
    }

    protected override void Update()//FixedUpdate()
    {
        if (oldDirect != 0)
        {
            Control_Move(oldDirect, false);
            //Debug.Log("OldDirect: " + oldDirect);
        }
        else
        {
            SpeedLinear = 0;
            animator.SetFloat("speed", SpeedLinear);

            // playMoveSound(null);
            if (!deg.bridge)
            {
                deg.planet.Get_Object_degree(this);
                Move(0);

            }
        }
    }
}
