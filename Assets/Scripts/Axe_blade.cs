﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Axe_blade : MonoBehaviour {


    private void OnTriggerEnter2D(Collider2D collision)
    {
        transform.parent.GetComponent<AxeFly>().collisionBlade(collision, transform);
    }
}
