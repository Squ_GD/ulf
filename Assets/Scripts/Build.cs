﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Build : MonoBehaviour {
    public float ScaleFactor, percUpCompReg;
    public Planet planet;
    public GameObject[] innerUnits;
    private float rightLimit, leftLimit;
    public Animator doorAnim;
	// Use this for initialization
	void Start () {
        BoxCollider2D myCollider = GetComponent<BoxCollider2D>();

        rightLimit = myCollider.size.x / 2f + myCollider.offset.x;
        rightLimit *= transform.localScale.x;
        leftLimit = -myCollider.size.x / 2f + myCollider.offset.x;
        leftLimit *= transform.localScale.x;


        if (innerUnits.Length > 0)
        for(int i = 0; i < innerUnits.Length; i++)
        {
            innerUnits[i] = Instantiate(innerUnits[i]);
            innerUnits[i].GetComponent<Control_enemy>().EnterBuild(ScaleFactor, this, planet);
            float height = innerUnits[i].GetComponent<CapsuleCollider2D>().size.y * innerUnits[i].transform.localScale.y;
            float rndCenter = Random.Range(leftLimit, rightLimit);
            innerUnits[i].transform.position = transform.position + transform.up * height / 2 + transform.right * rndCenter;
            innerUnits[i].transform.rotation = transform.rotation;
            if(ScaleFactor == 0)
            {

                innerUnits[i].GetComponent<Control_enemy>().ExitBuild(0);
            }
        }
        //doorAnim = transform.FindChild("Door").GetComponent<Animator>();
        
    }


    public bool Walk(Transform _unit, float speed, int direct)
    {
        Vector3 relativePos = transform.InverseTransformPoint(_unit.position);
        /* Vector3 relativeDirect = transform.InverseTransformDirection(_unit.right);

         int toDir = relativeDirect != transform.right ? -1 : 1;
         if (toDir != direct)
             _unit.Rotate(Vector3.up, 180f);*/

        float vectRot = Vector3.Angle(_unit.right, transform.right);

        int retVar = vectRot > 120 ? 1 : -1;

        if (retVar != direct)
            _unit.Rotate(Vector3.up, 180f);

        if (relativePos.x < 0 && direct == -1 && Mathf.Abs(relativePos.x) >= leftLimit)
        {
            return false;
        }
        else if (relativePos.x >= rightLimit && direct == 1)
        {
            return false;
        }
        else
            _unit.position += transform.right * speed * Time.deltaTime * direct;
        return true;
    }

    public float Exit(Control_enemy _Enemy)
    {
        doorOpen();

        return ScaleFactor;
    }
	
    public void SetPlanet(Planet _planet)
    {
        planet = _planet;
    }

    public Vector3 GetCenter()
    {
        BoxCollider2D _myCollider = GetComponent<BoxCollider2D>();
        float X = transform.position.x + _myCollider.offset.x * transform.localScale.x;
        float Y = transform.position.y + _myCollider.offset.y * transform.localScale.y;
        return new Vector3(X, Y, transform.position.z);
    }

    private void doorOpen()
    {
        if(!doorAnim)
        {
            //Debug.LogError("Не привязана анимация двери");
            return;
        }

        doorAnim.Play("Door");
    }

    // Update is called once per frame
    void Update () {
		
	}
}