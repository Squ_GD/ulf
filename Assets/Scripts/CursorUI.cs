﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorUI : MonoBehaviour {
    Animator _animator;

    private void OnEnable()
    {
        EventManager.AddListener("EnemyHP Visible", IsMouseOver);
        EventManager.AddListener("Item drag", IsLooting);
        _animator = GetComponent<Animator>();
    }
    // Use this for initialization
    void Start () {
		
	}

    private void IsLooting(BecomeEvent BE)
    {
        _animator.SetBool("loot", BE.come);
    }

    private void IsMouseOver(BecomeEvent BE)
    {

        _animator.SetBool("target", BE.come);
    }

    // Update is called once per frame
    void Update () {
		
	}
}
