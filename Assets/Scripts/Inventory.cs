﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using FMOD.Studio;

public class Inventory : MonoBehaviour { //Синглтон для клиента
    public static Inventory inventory;
    public inventoryChild[] lootPanel;
    public SpriteRenderer spriteTemplate;
    public Transform targetingTR;
    ItemCell targetPos;
    private Coroutine horizonCoroutine, verticalCoroutine;
    private static List<Item> itemSPR, craftList;
    private ItemCell[,] items = new ItemCell[3, 5], craftItems = new ItemCell[1, 5], leftItems = new ItemCell[3,1], rightItems = new ItemCell[3, 1];
    //private SpriteRenderer[,] itemRenders, craftRenders, leftRenders, rightRenders;
    private RectTransform[] lootRects;
    private Item craftedItem;
    private GameObject inventGO;
    private Animator inventAnimator;
    private Vector3 oldPosition;
    private Vector3 mousePos;
    private Vector2 startDragPosItem;
    private int startDragItemNum;
    private const int craftCells = 3;
    private bool pointerInventory, dragingLoot, deactiveBattle, justOpen;
    private int plusHor = 0, plusVer = 0;
    private Equip ulfEquip;//Initialize from himself
    private delegate ItemCell cellDelegate();
    private cellDelegate moveCell;
    private float lastClickTime;
    private const float doubleClickTimer = .27f;
    private bool leftClick, closeDelay, targetHorizonDelay, targetVerticalDelay;
    private const int maxStuck = 3;
    //private AudioSource audioSource;
    private bool opened;
    private LootList currLoot;
    private delegate ItemCell moveCursore();

    public delegate void delegateMethod();
    public delegate void itemNamedMethod(string nameItem);
    public delegateMethod OnCursoreMove, OnInventoryOpen;
    public itemNamedMethod OnAddItem, OnCraftItem, OnEquipItem;
    public Animator animatorBoneHeal, animatorCraft;
    public WeaponItem[] WeaponItemsSPR = {
        new WeaponItem("hand", null),
        new WeaponItem("shield",  new Recipe(new Ingredient[] { new Ingredient("wood", 2), new Ingredient("skin", 2) }), WeaponItem.TypeWeapon.shield),
        new WeaponItem("axe", new Recipe(new Ingredient[] { new Ingredient("steel", 1), new Ingredient("wood", 1) }), WeaponItem.TypeWeapon.simple),
        new WeaponItem("glovesred", new Recipe(new Ingredient[] { new Ingredient("redcry", 1), new Ingredient("skin", 1), new Ingredient("steel", 1) })),
        new WeaponItem("torch", new Recipe(new Ingredient[] { new Ingredient("wood", 1), new Ingredient("gum", 1), new Ingredient("fabric", 1) }), WeaponItem.TypeWeapon.simple),
        new WeaponItem("bow_trash", new Recipe(new Ingredient[] { new Ingredient("trash", 2), new Ingredient("skin", 1) }), WeaponItem.TypeWeapon.bow)
        };
    [Header("Sounds")]
    [SerializeField, Tooltip("FMOD event path for inventory opening SFX")]
    private string openInventorySound;
    [SerializeField, Tooltip("FMOD event path for inventory cursor change position SFX")]
    private string cursorInventorySound;
    

    private void Awake()
    {
        inventory = this;

        inventGO = transform.GetChild(0).gameObject;
        inventAnimator = GetComponent<Animator>();
        opened = inventGO.activeSelf;

        foreach (inventoryChild child in lootPanel)
        {
            SetItemCells(child.getCells(), child.getTransforms(), ItemCell.typeCell.loot);
        }
        InitCraftList();

    }
    private void OnDestroy()
    {
        inventory = null;
    }
    private void CheckDisable()
    {

        if (startDragItemNum >= 0)
        {
            
            
            //SetImage(itemTransforms[startDragItemNum], items[startDragItemNum]);
            startDragItemNum = -1;
            EventManager.Invoke("Item drag", new BecomeEvent(false, 1, 0));
            //draglootChange(false);
            //LootClose();
            
        }

        changeTargetPos(items[0,0]);
    }
    public void SetEquip(Equip newEquipScript)
    {
        ulfEquip = newEquipScript;
    }


    private void InitCraftList()
    {
        itemSPR = new List<Item>();
        //Добавляем в справочник крафтовых предметов
        //Item.typeItem currType = Item.typeItem.craft;
        itemSPR.Add(new CraftItem("steel"));
        itemSPR.Add(new CraftItem("bone"));
        itemSPR.Add(new CraftItem("fabric"));
        itemSPR.Add(new CraftItem("gum"));
        itemSPR.Add(new CraftItem("honey"));
        itemSPR.Add(new CraftItem("redcry"));
        itemSPR.Add(new CraftItem("bottle"));
        itemSPR.Add(new CraftItem("heart"));
        itemSPR.Add(new CraftItem("trash"));
        itemSPR.Add(new CraftItem("sundust"));
        itemSPR.Add(new CraftItem("wood"));
        itemSPR.Add(new CraftItem("gold"));
        itemSPR.Add(new CraftItem("skin"));        

        itemSPR.Add(new MissileItem("arrow"));

        for(int i = 0; i < WeaponItemsSPR.Length; i++)
        {
            itemSPR.Add(WeaponItemsSPR[i]);
        }
    }

    private Item GetRNDCraftItem()
    {
        Item nextItem = null;
        while(!(nextItem is CraftItem))
        {
            int nextInt = Random.Range(0, itemSPR.Count);
            nextItem = itemSPR[nextInt];
        }
        nextItem = new CraftItem((CraftItem)nextItem);
        return nextItem;
    }

    public bool isOpened()
    {
        return opened;
    }

    private void firstInit()
    {

        craftList = new List<Item>(8);
        Transform itemsParent = transform.GetChild(0).GetChild(0);

        int line = 0;
        SetItemCells(craftItems, itemsParent.GetChild(line++), ItemCell.typeCell.craft);
        SetItemCells(leftItems, itemsParent.GetChild(line++), ItemCell.typeCell.missile);
        SetItemCells(rightItems, itemsParent.GetChild(line++), ItemCell.typeCell.bottle);
        SetItemCells(items, itemsParent.GetChild(line++), ItemCell.typeCell.allItems);
        //ConnectCells(items, leftItems, rightItems, craftItems);

        changeTargetPos(items[0, 0]);




        targetingTR.gameObject.SetActive(false);


        oldPosition = transform.position;
        //GetItemTransforms();
        Item axe = GetSprWeapon("axe");
        AddItem(axe);
        //Item shield = itemSPR.Find(p => p.name == "shield");
        //AddItem(shield, -1);
        Item torch = GetSprWeapon("torch");
        AddItem(torch);
        Item heart = itemSPR.Find(p => p.name == "heart");
        AddItem(heart);
        AddItem(heart);
        Item bone = itemSPR.Find(p => p.name == "bone");
        AddItem(bone);
        AddItem(bone);

        Item steel = itemSPR.Find(p => p.name == "steel");
        AddItem(steel);
        Item wood = itemSPR.Find(p => p.name == "wood");
        AddItem(wood);

        Item bow = GetSprWeapon("bow_trash");
        AddItem(bow);

        Item arrow = itemSPR.Find(p => p.name == "arrow");
        ((MissileItem)arrow).ChangeCount(8);
        AddItem(arrow);

        

        
    }

    // Use this for initialization
    void Start () {
        

        firstInit();

        if (Control_Ulf.ulf_static == null)
        {
            Debug.Log("Test inv");
            AddTestLoot();//For test inventory scene
        }
        ConnectCells();
    }

    private void ConnectCells()
    {
        ConnectArraysHorizontal(leftItems, items);
        ConnectArraysHorizontal(items, rightItems);
        ConnectArraysVertical(craftItems, items);

        ConnectArraysHorizontal(craftItems, craftItems);
        ConnectArraysVertical(leftItems, leftItems);
        ConnectArraysVertical(rightItems, rightItems);

        for(int n = lootPanel.Length -1; n > 0; n--)
        {
            ConnectArraysVertical(lootPanel[n].getCells(), lootPanel[n - 1].getCells());
        }
        for(int n = 0; n < lootPanel.Length/2; n++)
        {
            ConnectArraysHorizontal(lootPanel[lootPanel.Length/2 + n].getCells(), lootPanel[n].getCells());
        }

        //for (int i = 0; i < lootPanel[0].getCells().Length; i++)
        {
            rightItems[0, 0].setUp(lootPanel[0].getCells()[0, lootPanel[0].getCells().Length-1]);
            craftItems[0, craftItems.GetLength(1) - 1].setUp(lootPanel[0].getCells()[0, lootPanel[0].getCells().Length - 2]);
            craftItems[0, craftItems.GetLength(1) - 2].setUp(lootPanel[0].getCells()[0, lootPanel[0].getCells().Length - 3]);
        }
        //for (int i = lootPanel[lootPanel.Length / 2].getCells().Length -1; i >= 0; i--)
        {
            leftItems[0, 0].setUp(lootPanel[lootPanel.Length / 2].getCells()[0, 0]);
            craftItems[0, 0].setUp(lootPanel[lootPanel.Length / 2].getCells()[0, 1]);
            craftItems[0, 1].setUp(lootPanel[lootPanel.Length / 2].getCells()[0, 2]);
        }
    }
    /*
    private ItemCell getNewPos(cellDelegate onTrueSide, cellDelegate onBackSide)
    {
        if(onTrueSide != null)
        {
            ItemCell cell = onTrueSide();
        }
    }
    */
    private void OnCellDisable(ItemCell cell_)
    {
        if(targetPos == cell_)
        {
            ItemCell newCell = TryMoveCursore(cell_, 'u');
            if(newCell == null)
                newCell = TryMoveCursore(cell_, 'd');
            if (newCell == null)
                newCell = items[0, 0];
            targetPos = newCell;
            changeTargetPos();
        }
    }
    private void changeTargetPos()
    {
        
        targetPos.onDisableCell -= OnCellDisable;//Отписка

        targetingTR.position = targetPos.renderer.transform.position;

        targetPos.onDisableCell += OnCellDisable;//Подписка

        //targetingTR.GetComponent<Animator>().SetTrigger("finish");
        targetingTR.gameObject.SetActive(true);
        //FMODUnity.RuntimeManager.PlayOneShot(cursorInventorySound, transform.position);
    }
    private void changeTargetPos(ItemCell CellPos_)
    {
        targetPos = CellPos_;
        targetingTR.position = targetPos.renderer.transform.position;
    }


    public bool AddItem(Item _item)
    {
        string name = _item.name;
        if (_item is CraftItem)
        {
            int stk = ((CraftItem)_item).stuck;
            ItemCell stuckPos = SearchItemForStuck(name, stk);
            if (stuckPos != null)
            {
                if (OnAddItem != null && stuckPos.cellType != ItemCell.typeCell.loot)
                    OnAddItem(_item.name);
                ((CraftItem)stuckPos.item).SetStuck(((CraftItem)stuckPos.item).stuck + stk);
                SetImage(stuckPos, stuckPos.item);
                CraftListCheck();
                return true;
            }

        }


        ItemCell free_pos = GetEmptyPos(_item);
        if (free_pos != null)
        {
            
            MoveItem(free_pos, _item);
            //Debug.Log("item stuck free pos: " + ((CraftItem)free_pos.item).stuck);

            return true;
        }
        else
        {
            Debug.Log("Error ADD Item: no free space");
            return false;
        }

    }    

    public void SetImage(ItemCell cell, Item item_)
    {
        if (cell == null)
        {
            Debug.Log("cell is null");
            return;
        }
        SpriteRenderer RT = cell.renderer;
        if (item_ == null)
        {
            RT.color = new Color(1, 1, 1, 0);
            return;
        }
        string itemName = item_.name;
        if (item_ is CraftItem)
        {
            CraftItem itemForStuck = (CraftItem)item_;
            itemName += itemForStuck.stuck;
        }
        else if(item_ is MissileItem)//Arrow
        {
            itemName += ((MissileItem)item_).strength;
        }
        Sprite _sprite = Resources.Load<Sprite>("UI\\" + itemName);
        if (RT == null)
        {
            Debug.LogError("Sprite is null: ");
            return;
        }
        RT.sprite = _sprite;
        RT.color = new Color(1, 1, 1, 1);
    }


    public void ReturnWeapon(Item weaponReturn)
    {
        Debug.Log("Return Weapon Inventory");
        ItemCell cell = GetEmptyPos(weaponReturn);
        if(cell != null)
        {
            MoveItem(cell, weaponReturn);
        }
    }

    public void CraftItem()
    {
        if (targetPos.item == null)
        {
            return;
        }

        if (GetEmptyPos(targetPos.item) != null)
        {
            ItemCell itemCell_ = GetEmptyPos(targetPos.item);
            ((WeaponItem)targetPos.item).recipe.CraftMinusItems(items);
            MoveItem(itemCell_, targetPos.item);
            RemoveItem(targetPos);
            if(OnCraftItem != null)
                OnCraftItem(itemCell_.item.name);
        }
    }
    /*
    private void craftedItemEnable()
    {
        for(int i = items.Length - craftCells -1; i < items.Length -1; i++)
        {
            if (items[i] == null)
                continue;
            items[i] = craftedItem.recipe.ItemChange(items[i]);
            if(items[i].strength <= 0)
            {
                items[i] = null;
            }
            //SetImage(itemTransforms[i], items[i]);
        }
        items[items.Length - 1] = craftedItem;
        
        //itemTransforms[items.Length - 1].GetComponent<Image>().raycastTarget = true;
        //itemTransforms[items.Length - 1].GetComponent<Image>().color = new Color(1, 1, 1, 1);
        craftedItem = null;
    }
    
    private void SetCraftedItem(Item craftItem)
    {
        craftedItem = craftItem;
        Item.typeItem typeItem = craftItem.type;
        //int _str = -1;
        //Debug.Log(craftItem.resultName);
        if (typeItem == Item.typeItem.one_hand_weapon)
            craftedItem.strength = -1;
        //Item newItem = new Item(craftItem.resultName, typeItem, _str);
        //SetImage(itemTransforms[items.Length -1], craftedItem);
        //items[items.Length - 1] = newItem;
        //itemTransforms[items.Length - 1].GetComponent<Image>().raycastTarget = false;
        //itemTransforms[items.Length - 1].GetComponent<Image>().color = new Color(1, 1, 1, .35f);
        //MoveItem(items.Length - 1, new Item(craftItem.resultName, typeItem, _str));
    }
    */
    private void CraftListCheck()
    {
        craftList.Clear();

        List<Item> items_ = new List<Item>();
        
        for (int j = 0; j < items.GetLength(0); j++)
            for (int i = 0; i < items.GetLength(1); i++)
            {
                if (items[j, i].item != null)
                    items_.Add(items[j, i].item);
            }

        foreach (Item CrIt in itemSPR)
        {
            if (!(CrIt is WeaponItem))
                continue;
                    
            if (((WeaponItem)CrIt).recipe == null)
                continue;
            Recipe recipe = ((WeaponItem)CrIt).recipe;
            
            if (recipe.MatchIngredients(items_))
            {
                craftList.Add(CrIt);

                
            }
        }
        if (craftList.Count == 0)
        {

            return;
        }
        ShowCraftList(0);
    }
    private void ShowCraftList(int startPos_)
    {
        if(startPos_ > craftList.Count-1)
        {
            Debug.LogError("Craft count");
            return;
        }
        int currCell = 0;
        for(int i = startPos_; i < startPos_ + 5 && i < craftList.Count; i++)
        {
            Item craftItem_ = craftList[currCell++];

            SetImage(craftItems[0, i], craftItem_);
            craftItems[0, i].AddItem(craftItem_);

        }
    }

    public void MoveItem(ItemCell pos, Item item_)
    {
        SetImage(pos, item_);
        pos.AddItem(item_);

        if(OnAddItem != null && pos.cellType != ItemCell.typeCell.loot && pos.cellType != ItemCell.typeCell.craft)
            OnAddItem(item_.name);

        CraftListCheck();
        
    }

    private void RemoveItem(ItemCell pos)
    {
        pos.RemoveItem();
        SetImage(pos, null);
        CraftListCheck();
        
    } 

   

    private void ConnectArraysVertical(ItemCell[,] upArray, ItemCell[,] downArray)
    {
        for(int j = 0; j < upArray.GetLength(0); j++)
            for(int i = 0; i < upArray.GetLength(1); i++)
            {
                if (j == upArray.GetLength(0) - 1)
                {
                    upArray[j, i].setDown(downArray[0, i]);
                }
                else
                    upArray[j, i].setDown(upArray[j + 1, i]);
            }

        for (int j = 0; j < downArray.GetLength(0)-1; j++)
            for (int i = 0; i < downArray.GetLength(1); i++)
            {
                downArray[j, i].setDown(downArray[j + 1, i]);
            }
    }
    private void ConnectArraysHorizontal(ItemCell[,] leftArray, ItemCell[,] rightArray)
    {
        for (int j = 0; j < leftArray.GetLength(0); j++)
            for (int i = 0; i < leftArray.GetLength(1); i++)
            {
                if (i == leftArray.GetLength(1) - 1)
                {
                    leftArray[j, i].setRight(rightArray[j, 0]);
                }
                else
                    leftArray[j, i].setRight(leftArray[j, i+1]);
            }
        for (int j = 0; j < rightArray.GetLength(0); j++)
            for (int i = 0; i < rightArray.GetLength(1)-1; i++)
            {
                rightArray[j, i].setRight(rightArray[j, i + 1]);
            }
    }

    private void ConnectCells(ItemCell[,] arrayFor, ItemCell[,] arrayLeft, ItemCell[,] arrayRight, ItemCell[,] arrayTop)
    {//Initialize cells navigations
        for (int j = 0; j < arrayFor.GetLength(0); j++)
            for (int i = 0; i < arrayFor.GetLength(1); i++)
            {
                if (i == 0)
                {
                    arrayFor[j, i].setLeft(arrayLeft[j, 0]);
                }
                else
                {
                    arrayFor[j, i].setLeft(arrayFor[j, i-1]);
                }
                if(j == 0)
                {
                    arrayFor[j, i].setUp(arrayTop[0, i]);
                }
                else
                {
                    arrayFor[j, i].setUp(arrayFor[j-1, i]);

                }
                if (i == arrayFor.GetLength(1) -1)
                {
                    arrayFor[j, i].setRight(arrayRight[j, 0]);
                }
            }
        for (int j = 0; j < arrayLeft.GetLength(0); j++)
            for (int i = 0; i < arrayLeft.GetLength(1); i++)
            {
                if (j < arrayLeft.GetLength(0) - 1)
                    arrayLeft[j, i].setDown(arrayLeft[j + 1, i]);
            }
        for (int j = 0; j < arrayRight.GetLength(0); j++)
            for (int i = 0; i < arrayRight.GetLength(1); i++)
            {
                if (j < arrayRight.GetLength(0) - 1)
                    arrayRight[j, i].setDown(arrayRight[j + 1, i]);
            }
        for (int j = 0; j < arrayTop.GetLength(0); j++)
            for (int i = 0; i < arrayTop.GetLength(1); i++)
            {
                if (i < arrayTop.GetLength(1) - 1)
                    arrayTop[j, i].setRight(arrayTop[j, i+1]);
            }
    }

    public void SetItemCells(ItemCell[,] arrayFor, Transform parentFrom, ItemCell.typeCell typeCell_)
    {
        int cTr = 0;
        for (int j = 0; j < arrayFor.GetLength(0); j++)
            for (int i = 0; i < arrayFor.GetLength(1); i++)
            {

                Transform currTansItem = parentFrom.GetChild(cTr++);

                SpriteRenderer newSprite;
                if (currTansItem.gameObject.GetComponent<SpriteRenderer>())
                    newSprite = currTansItem.gameObject.GetComponent<SpriteRenderer>();
                else
                    newSprite = currTansItem.gameObject.AddComponent<SpriteRenderer>();
                arrayFor[j, i] = new ItemCell(typeCell_, true, newSprite);
                newSprite.sprite = spriteTemplate.sprite;
                newSprite.sortingOrder = spriteTemplate.sortingOrder;

                newSprite.sortingLayerName = spriteTemplate.sortingLayerName;
                newSprite.color = new Color(1, 1, 1, 0);
            }
    }
    /*
    private void setImageEvents(GameObject imageGO, int num, bool lootable_)
    {
        EventTrigger trigger = imageGO.GetComponent<EventTrigger>();
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.Drag;
        entry.callback.AddListener((data) => { EventItemDrag((PointerEventData)data, num); });
        trigger.triggers.Add(entry);

        entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.BeginDrag;
        entry.callback.AddListener((data) => { EventItemStartDrag((PointerEventData)data, num); });
        trigger.triggers.Add(entry);
        //if (num != items.Length - 1)
        {
            entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.EndDrag;
            entry.callback.AddListener((data) => { EventItemEndDrag((PointerEventData)data, num); });
            trigger.triggers.Add(entry);
        }

        entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerClick;
        entry.callback.AddListener((data) => { EventItemClick((PointerEventData)data, num); });
        trigger.triggers.Add(entry);
    }
    public void EventItemDrag(PointerEventData eventData, int num)
    {
        if (startDragItemNum == -1)
            return;
        var v3 = Input.mousePosition;
        v3.z = 10f;
        v3 = Camera.main.ScreenToWorldPoint(v3);

        Vector3  currentPositon = v3;
        Vector3 deltaPositon = currentPositon - mousePos;
        //Debug.Log("item: " + num +  " deltaPositon: " + deltaPositon);
        mousePos = currentPositon;
        itemTransforms[num].position += deltaPositon;
       
    }

    public void StartDragLoot(int lootNum)
    {
        EventManager.Invoke("Item drag", new BecomeEvent(true, 1, 0));
        draglootChange(true);
        lootRects[lootNum].GetComponent<Image>().color = new Color(1, 1, 1, 0);
        Debug.Log("Start drag loot");
    }


    /*
    private void deleteLoot(int lootNum_)
    {
        lootItems[lootNum_] = null;
        if (lootNum_ == 0)
            currLoot.drop1 = "";
        else
            currLoot.drop2 = "";

        if (currLoot.IsEmpty())
        {
            LootClose();
        }
    }
    
    public void EndDragLoot(int lootNum)
    {
        EventManager.Invoke("Item drag", new BecomeEvent(false, 1, 0));
        draglootChange(false);

        Debug.Log("End drag loot");
        var v3 = Input.mousePosition;
        v3.z = 10f;
        v3 = Camera.main.ScreenToWorldPoint(v3);
        Vector2 endDragPosItem = v3;
        float minDistItem = 10000f;
        int minPos = -1;
        for (int i = 0; i < items.Length - 1; i++)
        {
            float tempDelta = (endDragPosItem - (Vector2)itemTransforms[i].position).magnitude;
            if (tempDelta < minDistItem)
            {
                minPos = i;
                minDistItem = tempDelta;
            }
        }

        if (AddItem(lootItems[lootNum], minPos))
        {
            deleteLoot(lootNum);
            return;
        }
    }

    public void EventItemStartDrag(PointerEventData eventData, int num)
    {
        if (items[num] == null)
            return;
        var v3 = Input.mousePosition;
        v3.z = 10f;
        v3 = Camera.main.ScreenToWorldPoint(v3);
        mousePos = v3;
        startDragItemNum = num;
        startDragPosItem = itemTransforms[num].position;
        itemTransforms[num].SetSiblingIndex(itemTransforms[num].parent.childCount-1);
        //itemTransforms[num].GetComponent<Image>().raycastTarget = false;
        EventManager.Invoke("Item drag", new BecomeEvent(true, 1, 0));
        draglootChange(true);
        itemTransforms[startDragItemNum].GetComponent<Image>().color = new Color(1, 1, 1, 0);
    }

    public void EventItemEndDrag(PointerEventData eventData, int num)
    {
        
        if (startDragItemNum == -1)
            return;
        itemTransforms[startDragItemNum].GetComponent<Image>().color = new Color(1, 1, 1, 1);

        EventManager.Invoke("Item drag", new BecomeEvent(false, 1, 0));
        draglootChange(false);
        var v3 = Input.mousePosition;
        v3.z = 10f;
        v3 = Camera.main.ScreenToWorldPoint(v3);
        Vector2 endDragPosItem = v3;

        // Vector3 currentPositon = v3;
        //Vector2 endDragPosItem = itemTransforms[num].position;
        float startEndDelta = (startDragPosItem - endDragPosItem).magnitude;
        float minDistItem = 10000f;
        int minPos = -1;
        for(int i = 0; i < items.Length -1; i++)
        {
            if (i == num)
                continue;
            float tempDelta = (endDragPosItem - (Vector2)itemTransforms[i].position).magnitude;
            if(tempDelta < startEndDelta && tempDelta < minDistItem)
            {
                minPos = i;
                minDistItem = tempDelta;
            }
        }
        //itemTransforms[startDragItemNum].position = startDragPosItem;
        //itemTransforms[startDragItemNum].GetComponent<Image>().raycastTarget = true;

        if (minPos != -1 && minDistItem < .5f)
        {
            if(items[num].type == Item.typeItem.craft && items[minPos] != null
                && CanStuckItems(items[minPos].name, items[minPos].strength, num))//searchItemForStuck(items[num].name, num) == minPos)
            {
                stuckItems(num, minPos);
            }
            else if(startDragItemNum == items.Length -1)
            {
                int freepos = GetEmptyPos(0, items.Length - craftCells -1);
                if(freepos != -1)
                {
                    MoveItem(freepos, items[startDragItemNum]);
                    RemoveItem(startDragItemNum);
                    return;
                }
            }
            else
                ChangeItemPos(num, minPos);

        }
        startDragItemNum = -1;
        
    }

    private void EventItemClick(PointerEventData eventData, int num)
    {
        //ChildDisable();

        if (Time.realtimeSinceStartup - lastClickTime < doubleClickTimer
            && leftClick == Input.GetMouseButtonUp(0))
        {
            
            StopAllCoroutines();
            //DoubleClickedItem(num, leftClick);
            lastClickTime = 0;

            return;
        }
        leftClick = Input.GetMouseButtonUp(0);
        lastClickTime = Time.realtimeSinceStartup;
        if(leftClick)
            StartCoroutine(clickCoroutine(num));
    }
    
    private void PlayInvSound(string soundName)
    {
        AudioPlayer.playOneClip("UI\\" + soundName);
       // Debug.Log("Play: " + "Sound\\UI\\" + soundName  );
    }

    IEnumerator clickCoroutine(int num_)
    {
        yield return new WaitForSeconds(doubleClickTimer);
        //Debug.Log("single Click");
        SingleClickedItem(num_);
    }
    */
    IEnumerator targetHorDelayed()
    {
        
        targetHorizonDelay = true;
        yield return new WaitForSeconds(.5f);
        targetHorizonDelay = false;
    }

    IEnumerator targetVerDelayed()
    {
        targetVerticalDelay = true;
        yield return new WaitForSeconds(.5f);
        targetVerticalDelay = false;
    }

    public Item GetSprItem(string itemName)
    {
        Item retImet = itemSPR.Find(p => p.name == itemName);
        if (retImet is WeaponItem)
            retImet = new WeaponItem((WeaponItem)retImet);
        return retImet;
    }

    public WeaponItem GetSprWeapon(string name_)
    {
        WeaponItem weaponItem = new WeaponItem((WeaponItem)itemSPR.Find(p => p.name == name_));

        return weaponItem;
    }

    public int GetNumSprWeapon(string name_)
    {
        for(int i = 0; i < itemSPR.Count; i++)
        {
            if(itemSPR[i].name == name_)
            {
                return i;
            }
        }
        Debug.Log("NOT FIND!");
        return -1;
    }

    public string GetNameSprNumWeapon(int num_)
    {
        return itemSPR[num_].name;
    }


    /*
    private void SingleClickedItem(int num)
    {
        Item item_ = items[num];
        if (items[num] == null)
            return;
        // EventManager.Invoke("Inventory", new BecomeEvent(true, 2, 1));
       // if(item_.type == Item.typeItem.craft)
       // {
            if(num < items.Length - craftCells -1)
            {
                int pos = GetEmptyPos(items.Length - craftCells - 1, items.Length -1);
                if (pos == -1)
                    return;
                MoveItem(pos, items[num]);
                RemoveItem(num);
            }
            else //if( num < 13)
            {
                int pos = GetEmptyPos(0, items.Length - craftCells - 1);
                if (pos == -1)
                    return;
                MoveItem(pos, items[num]);
                RemoveItem(num);
            }
        //}
        PlayInvSound("click");
        
}

private void stuckItems(ItemCell cellFrom, ItemCell cellTo)
    {
        Item fromItem = cellFrom.item, toItem = cellTo.item;
        while(fromItem.strength > 0 && toItem.strength < maxStuck)
        {
            fromItem.strength--;
            toItem.strength++;
            Debug.Log("Stuck str: " + fromItem.strength);
        }
        if (fromItem.strength == 0)
        {
            RemoveItem(cellFrom);
        }
        else
        {
            RefreshItem(cellFrom);
        }
        RefreshItem(cellTo);

    }
    
    private void RefreshItem(ItemCell posItem_)
    {
        string itemName = posItem_.item.name + posItem_.item.strength;
        Sprite _sprite = Resources.Load<Sprite>("UI\\" + itemName);
        posItem_.renderer.sprite = _sprite;
    }
    */
    private bool CanStuckItems(string name, int str_, int posX, int posY)
    {
        if (items[posX, posY].item == null)
            return false;
        if (items[posX, posY].item is CraftItem
            && items[posX, posY].item.name == name 
            && ((CraftItem)items[posX, posY].item).stuck + str_ <= maxStuck)
        {
            return true;
        }
        else
            return false;
    }


    private ItemCell SearchItemForStuck(string name, int str_)
    {
        for(int j = 0; j < items.GetLength(0); j++)
            for (int i = 0; i < items.GetLength(1); i++)
            {
                if (items[j,i] == null)
                continue;
                if (CanStuckItems(name, str_, j,i))
                return items[j,i];
            }
        return null;
    }

    private void ChangeItemPos(int start, int end)
    {
        /*
        if (end == items.Length - 1)
            return;
        Item tempItem = null;
        if(items[end] != null)
        {
            tempItem = items[end];
        }
        MoveItem(end, items[start]);
        if (tempItem != null)
        {
            MoveItem(start, tempItem);
        }
        else
            RemoveItem(start);
            */
    }

    





    

    private ItemCell GetEmptyPos(Item item_)
    {
        ItemCell[,] itemsArray = null;
        if (item_ is MissileItem)
            itemsArray = leftItems;
        else
            itemsArray = items;

        for (int j = 0; j < itemsArray.GetLength(0); j++)
            for (int i = 0; i < itemsArray.GetLength(1); i++)
            {
            if (itemsArray[j,i].item == null)
                return itemsArray[j, i];
            }
        //Debug.Log("inventory full");

        return null;
    }
    /*
    public void TryDisarmWeapon(int partBody)
    {
        if (!inventGO.activeSelf)
            return;
        if (!DisarmWeapon(partBody, false))
            return;
        //handlerInventory.SwitchingWeapon(partBody == 1?true:false);

        if(Converter.converter)
        {
            bool left_ = partBody == 1 ? true : false;
            Converter.converter.SwitchWeaponFromInventory(left_, "hand", 1);
        }

        handlerInventory.CommandSwitchWeapon(partBody == 1 ? true : false, GetSprWeapon("hand"), false);
        bodyLoad[partBody] = null;
        //ulfBattle.WeaponLoad(partBody == 1 ? true : false, "hand", -1);
    }
    */
    private void CollectAllDrop()
    {
        for(int i = 0; i < lootPanel.Length; i++)
        {
            foreach (ItemCell cell in lootPanel[i].getCells())
                CollectDrop(cell);
        }
    }

    private bool CollectDrop(ItemCell lootCell)
    {

        if (lootCell.item == null)
            return false;
        
        if (AddItem(lootCell.item))
        {
            //Debug.Log("item stuck: " + ((CraftItem)lootCell.item).stuck);
            lootCell.RemoveItem();
            return true;
        }
        else
            return false;
        
    }

    private void AddTestLoot()
    {


        for (int i = 0; i < lootPanel.Length; i++)
        {
            lootObject lo = new lootObject();
            lo.lootItem1 = GetRNDCraftItem();
            
            lo.lootItem2 = GetRNDCraftItem();
            lo.lootItem3 = GetRNDCraftItem(); 

            lootPanel[i].gameObject.SetActive(true);
            lootPanel[i].SetLoot(lo);
        }

    }

    public bool LootOpen(List<lootObject> loot_)
    {

        if (!opened && !TryOpen())
            return false;

        int lootForward = 0, lootBackward = 0;

        foreach (lootObject lo in loot_)
        {
            float inversePos = Control_Ulf.ulf_static.transform.InverseTransformPoint(lo.transform.position).x;
            int dirUlf = Control_Ulf.ulf_static.lastDir;
            Debug.Log("dirUlf: " + dirUlf + " inversePos: " + inversePos);
            if (dirUlf == -1 && inversePos > 0 || dirUlf == 1 && inversePos < 0)
                for (int i = lootPanel.Length/2; i < lootPanel.Length; i++)
                {
                    if (lootPanel[i].gameObject.activeSelf)
                        continue;
                    if (dirUlf == -1)
                        lootForward++;
                    else
                        lootBackward++;
                    lootPanel[i].gameObject.SetActive(true);
                    lootPanel[i].SetLoot(lo);
                    break;
                }
            else
                for (int i = 0; i < lootPanel.Length/2; i++)
                {
                    if (lootPanel[i].gameObject.activeSelf)
                        continue;
                    if (dirUlf == 1)
                        lootForward++;
                    else
                        lootBackward++;
                    lootPanel[i].gameObject.SetActive(true);
                    lootPanel[i].SetLoot(lo);
                    break;
                }

            
        }
        if (lootBackward > 0 && lootForward == 0)
        {
            Control_Ulf.ulf_static.Flip_Ulf();
            Debug.Log("Only backward loot");
        }
        return true;
    }
    /*
    public void TryBone()//UI button
    {
        for (int i = 0; i < 9; i++)
        {
            if (items[i] != null && items[i].name == "bone")
            {
                if (--items[i].strength <= 0)
                {
                    RemoveItem(i);
                }
                else
                    SetImage(itemTransforms[i], items[i]);

                if(Converter.converter)//Сеть
                {
                    Converter.converter.HealUlf(false);
                }

                handlerInventory.CommandBoneEath();
                animatorBoneHeal.SetTrigger("bone");
                break;
            }
        }
    }

    public void TryHeal()//UI button
    {
        for(int i = 0; i < items.Length - craftCells -1; i++)
        {
            if(items[i] != null && items[i].name == "heart")
            {
                if (--items[i].strength <= 0)
                {
                    RemoveItem(i);
                }
                else
                    SetImage(itemTransforms[i], items[i]);

                if (Converter.converter)
                {
                    Converter.converter.HealUlf(true);
                }

                handlerInventory.CommandHeartEath();
                animatorBoneHeal.SetTrigger("heal");
                break;
            }
        }
    }*/

    private IEnumerator DelayAfterOpen()
    {
        closeDelay = true;
        yield return new WaitForSeconds(.5f);
        closeDelay = false;
    }

    public void TryClose()
    {
        if (opened && !closeDelay)
        {
            OpenInventory();
            //LootClose();
            
        }

    }
    public void PointerInventory(bool enable_)
    {
        pointerInventory = enable_;
    }

    public void OpenInventory()
    {

        //if (!ulfBattle.RiseUpHands())
        {
            opened = !inventGO.activeSelf;
            if (OnInventoryOpen != null)
                OnInventoryOpen();
            inventGO.SetActive(opened);
            EventManager.Invoke("Inventory", new BecomeEvent(opened, 1, 0));
            //FMODUnity.RuntimeManager.PlayOneShot(openInventorySound, transform.position);
            if (opened)
            {

                targetingTR.gameObject.SetActive(false);

                Vector3 newPosition = transform.position;
                if ((newPosition - oldPosition).magnitude > 2)
                {
                    oldPosition = newPosition;
                    inventAnimator.SetTrigger("start");
                }
                StartCoroutine(DelayAfterOpen());
            }
            else
            {
                CheckDisable();
            }

        }
        //else
          //  Debug.Log("Hand Raise");
    }

    public ItemCell GetMissile()
    {
        ItemCell arrowPos = null;
        for (int j = 0; j < leftItems.GetLength(0); j++)
            for(int i = 0; i < leftItems.GetLength(1); i++)
            {
                if (leftItems[j, i].item != null)
                {
                    arrowPos = leftItems[j, i];
                    break;
                }    //arrow = ((MissileItem)leftItems[j, i].item);
            }
        return arrowPos;
    }

    private void disactiveCursore()
    {
        if (horizonCoroutine != null)
            StopCoroutine(horizonCoroutine);
        horizonCoroutine = StartCoroutine(targetHorDelayed());

        if (verticalCoroutine != null)
            StopCoroutine(verticalCoroutine);
        verticalCoroutine = StartCoroutine(targetVerDelayed());

        targetingTR.gameObject.SetActive(false);
        if (OnCursoreMove != null)
            OnCursoreMove();
    }
    private ItemCell TryMoveCursore(ItemCell currCell, char moveSide)
    {
        moveCursore move;
        switch (moveSide)
        {
            case 'r':
                move = currCell.rightSide;
                break;
            case 'l':
                move = currCell.leftSide;
                break;
            case 'u':
                move = currCell.upSide;
                break;
            case 'd':
                move = currCell.downSide;
                break;
            default:
                move = currCell.rightSide;
                Debug.LogError("Ahtung");
                break;
        }
        if (move() != null)
        {
            if (move().isActive)
            {
                disactiveCursore();
                return move();
            }
            else
            {
                Debug.Log("recursion: " + moveSide);
                return TryMoveCursore(move(), moveSide);
            }
        }
        else
            return null;
    }

    private bool TryOpen()
    {

        OpenInventory();
        justOpen = opened;
        return opened;
    }

    private void Update()
    {
        if (plusHor != 0 || plusVer != 0)
        {
            if (justOpen)//Сразу после открытия инвентаря курсор вначале
            {
                targetPos = items[0, 0];
                justOpen = false;
            }

            changeTargetPos();
            plusHor = plusVer = 0;
        }

        if (Input.GetKeyDown(KeyCode.Tab))// || Input.GetKeyDown(KeyCode.I))
        {
            if(!TryOpen())
            {
                //lootPanel.gameObject.SetActive(false);
            }
        }
        else if(Input.GetKeyDown(KeyCode.E) && targetPos.cellType == ItemCell.typeCell.craft)//CRAFT
        {
            CraftItem();
        }
        if (opened)
        {

            float hor = Input.GetAxis("Horizontal");
            ItemCell nextCell = null;
            if (hor == 0)
            {
                if (horizonCoroutine != null)
                    StopCoroutine(horizonCoroutine);
                targetHorizonDelay = false;
            }
            else if (!targetHorizonDelay)
            {
                if (hor > 0)
                {
                    nextCell = TryMoveCursore(targetPos, 'r');
                    plusHor = 1;
                }
                else
                {
                    nextCell = TryMoveCursore(targetPos, 'l');
                    plusHor = -1;
                }
            }

            if (nextCell != null)
                targetPos = nextCell;

            float ver = Input.GetAxis("Vertical");

            if (ver == 0)
            {
                if (verticalCoroutine != null)
                    StopCoroutine(verticalCoroutine);
                targetVerticalDelay = false;
            }

            else if (!targetVerticalDelay)
            {
                if (ver > 0)
                {
                    nextCell = TryMoveCursore(targetPos, 'u');
                    plusVer = 1;
                }
                else
                {
                    nextCell = TryMoveCursore(targetPos, 'd');
                    plusVer = -1;
                }
            }

            if (nextCell != null)
                targetPos = nextCell;

            if (Control.control.dropCollect.start)
                if (targetPos.cellType == ItemCell.typeCell.loot)
                    CollectDrop(targetPos);
                else
                    CollectAllDrop();

            if (targetPos.cellType != ItemCell.typeCell.allItems || targetPos.item == null)
                return;

            if (!(targetPos.item is WeaponItem))
                return;

            if (Control.control.fire1.start && !justOpen)
            {
                if (OnEquipItem != null)
                    OnEquipItem(targetPos.item.name);

                ulfEquip.EquipWeapon(targetPos, true);
                
            }
            else if (Control.control.fire2.start && !justOpen)
            {
                if (OnEquipItem != null)
                    OnEquipItem(targetPos.item.name);

                ulfEquip.EquipWeapon(targetPos, false);
                
            }
            
        }
    }
}
public class Recipe
{

    public Ingredient[] ingredients;
    public Recipe(Ingredient[] ingredients_)
    {
        ingredients = ingredients_;
    }
   
    private bool matchCheck(int ingrMath)
    {
        for (int i = 0; i < ingredients.Length; i++)
        {
            ingredients[i].converge = false;
        }
        if (ingrMath >= ingredients.Length)
            return true;
        else
            return false;
    }

    public bool MatchIngredients(List<Item> items_)
    {
        if (ingredients.Length == 0)
            return false;

        int matched = 0;
        for (int t = 0; t < items_.Count; t++)
        {
            if(items_[t] == null)
                continue;
            string itemName = items_[t].name;
            int itemStr = 1;// items_[t].strength;
            if(items_[t] is CraftItem)
            {
                itemStr = ((CraftItem)items_[t]).stuck;
            }
            for (int i = 0; i < ingredients.Length; i++)
            {
                if (ingredients[i].Check(itemName, itemStr))
                {
                    matched++;
                    break;
                }
            }
            
            
        }
        if (matchCheck(matched))
            return true;
        else
            return false;
    }

    public void CraftMinusItems(ItemCell[,] itemCells)
    {

        for (int j = 0; j < itemCells.GetLength(0); j++)
            for (int i = 0; i < itemCells.GetLength(1); i++)
            {
               
                Item item_ = itemCells[j, i].item;
                if (item_ == null)
                    continue;

                string itemName = item_.name;
                int itemStr = 1;
                if (item_ is CraftItem)
                {
                    itemStr = ((CraftItem)item_).stuck;
                }
                for (int r = 0; r < ingredients.Length; r++)
                {
                    if (itemName == ingredients[r].ingr)
                    {
                        if (item_ is CraftItem)
                            ((CraftItem)item_).SetStuck(((CraftItem)item_).stuck- ingredients[r].count);
                        if (((CraftItem)item_).stuck <= 0 || item_ is WeaponItem)
                        {
                            itemCells[j, i].RemoveItem();
                        }
                        else
                            Inventory.inventory.SetImage(itemCells[j, i], itemCells[j, i].item);
                        
                    }
                }
            }
    }
}
public class ItemCell
{
    public enum typeCell { allItems, craft, bottle, missile, loot }
    public typeCell cellType { get; private set; }
    public bool isActive { get; private set; }
    public Item item { get; private set; }
    public SpriteRenderer renderer { get; private set; }

    public delegate void EventMethod(ItemCell cell);
    public delegate void simpleEventMethod();
    public simpleEventMethod onCollect;
    public EventMethod onDisableCell;

    private ItemCell left, right, up, down;
    public void activateCell(bool active_)
    {
        isActive = active_;
    }
    public void AddItem(Item item_)
    {
        item = item_;
        
    }
    public void RemoveItem()
    {
        SetItemNull();
        if(cellType == typeCell.loot && onCollect != null)
            onCollect();
    }
    public void SetItemNull()
    {
        item = null;
        renderer.sprite = null;
        renderer.color = new Color(1, 1, 1, 0);
    }
    public void ChangeStuckItem(int newStuck)
    {
        if (newStuck == 0)
            RemoveItem();
        else
        {
            ((CraftItem)item).SetStuck(newStuck);
            Inventory.inventory.SetImage(this, item);
        }
    }
    public ItemCell(typeCell type_, bool activate_, SpriteRenderer renderer_)
    {
        renderer = renderer_;
        cellType = type_;
        isActive = activate_;
    }
    public void disableCell()
    {
        if(onDisableCell != null)
            onDisableCell(this);
        isActive = false;
    }
    public void setLeft(ItemCell left_)
    {
        left = left_;
        left.right = this;
    }
    public void setRight(ItemCell right_)
    {
        right = right_;
        right.left = this;
    }
    public void setUp(ItemCell up_)
    {
        up = up_;
        up.down = this;
    }
    public void setDown(ItemCell down_)
    {
        down = down_;
        down.up = this;
    }
    
    public ItemCell rightSide()
    {

        return right;
    }
    public ItemCell leftSide()
    {
        return left;
    }
    public ItemCell upSide()
    {
        return up;
    }
    public ItemCell downSide()
    {
        return down;
    }
}
public class MissileItem : WeaponItem
{

    public void ChangeCount(int plusCount)
    {

        strength += plusCount;
    }

    public MissileItem( string name_)
    {
        type = TypeWeapon.bow;

        SetNameItem(name_);

    }
}
[System.Serializable]
public class WeaponItem : Item
{
    public enum TypeWeapon { hand, simple, bow, shield}
    public TypeWeapon type { get; protected set; }
    public Recipe recipe { get; private set; }
    public int strength;
    public WeaponItem()
    {

    }

    
    public WeaponItem(string name_, Recipe recipe_)
    {
        SetNameItem(name_);
        recipe = recipe_;
        type = 0;
        
    }
    public WeaponItem(string name_, Recipe recipe_, TypeWeapon type_)
    {
        SetNameItem(name_);
        recipe = recipe_;
        type = type_;
    }
    public WeaponItem(WeaponItem fromWeapon)
    {
        name = fromWeapon.name;
        recipe = fromWeapon.recipe;
        type = fromWeapon.type;
        strength = fromWeapon.strength;
    }
}

public class CraftItem : Item
{
    public int stuck { get; private set; }
    public CraftItem(CraftItem item)
    {
        SetNameItem(item.name);
        stuck = item.stuck;
    }
    public CraftItem(string name_)
    {
        SetNameItem(name_);
        stuck = 1;
    }
    public void SetStuck(int stuck_)
    {
        stuck = stuck_;
    }
}

public class Item
{
    public string name;
    protected void SetNameItem(string name_)
    {
        name = name_;
    }
}

public struct Ingredient
{
   public Ingredient(string ing, int c) : this()
    {
        ingr = ing;
        count = c;
    }
    public bool Check(string name_, int count_)
    {
        if (name_ == ingr && count_ >= count && !converge)
        {
            converge = true;
            return true;
        }
        else
            return false;
    }
    public string ingr;
    public int count;
    public bool converge;
}