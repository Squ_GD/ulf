﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class Movement_basis : MonoBehaviour {
    public float SpeedLinear;
    protected Planet planet;

    protected float colliderHeight, colliderWidth;
    protected float RadiusPlanet, RadiusBridge;
    protected float axisPlanetX, axisPlanetY, axisBridgeX, axisBridgeY;
    protected float currDegree;
    protected bool IsUlf;

    protected GameObject shadow;
    private SpriteRenderer shadowSR;
    private SortingGroup shadowSG;
   // protected bool stopMove = false;

    [HideInInspector]
    public int lastDir = 1;

    protected void OnEnable()
    {
        colliderHeight = GetComponent<CapsuleCollider2D>().size.y * transform.localScale.x / 2f;
        colliderWidth = GetComponent<CapsuleCollider2D>().size.x * transform.localScale.x / 2f;
        shadowCreate();
    }

    public void disableShadow(bool disable_)
    {
        shadow.SetActive(disable_);
    }

    private void shadowCreate()
    {
        shadow = new GameObject();
        shadowSG = shadow.AddComponent<SortingGroup>();
        shadowSR = shadow.AddComponent<SpriteRenderer>();
        shadow.transform.parent = transform;
        shadow.transform.position = transform.position - transform.up * colliderHeight;
        shadow.transform.rotation = Quaternion.identity;
        float shadowWidth = GetComponent<CapsuleCollider2D>().size.x / transform.localScale.x;
        shadow.transform.localScale = new Vector3(shadowWidth, 5f, 1);
        shadowSR.sprite = Resources.Load<Sprite>("Tooling\\shadow");
        shadowSG.sortingLayerName = "Default";
        shadowSG.sortingOrder = -1;
    }

    public void SetDegree(float degree)
    {
        currDegree = degree;

    }


    protected void Move(int direct)
    {
        //if (stopMove)
          //  return;

        float rotateRadius = RadiusPlanet + colliderHeight;
        //Угловая скорость (не забыть про множитель 10!)
        float speedRadial = direct * SpeedLinear * 10 * Time.deltaTime / rotateRadius;
        //Дельта угол
        float newDegree = (currDegree + speedRadial) / 180 * Mathf.PI;
       // Debug.Log("currDegree: " + currDegree + " newDegree: " + newDegree);

        float posX = rotateRadius * Mathf.Sin(newDegree) + axisPlanetX;
        float posY = rotateRadius * Mathf.Cos(newDegree) + axisPlanetY;
        transform.position = new Vector3(posX, posY, 0);
        Vector3 vectorToPlanet = new Vector3(axisPlanetX,axisPlanetY,0) - transform.position;
        vectorToPlanet.Normalize();

        transform.rotation = Quaternion.LookRotation(transform.forward, -vectorToPlanet);

        if (direct != lastDir && direct != 0 && !IsUlf)
            Flipping();
    }
    protected void Flipping()
    {
        transform.rotation *= Quaternion.AngleAxis(180, Vector3.up);
    }

    public virtual void SetSortGroup(string srtLayer, int srtOrder)
    {

    }
    public Planet GetPlanet()
    {
        return planet;
    }

    protected void Move_Bridge(int direct)
    {
        //if (stopMove)
          //  return;

        float rotateRadius = RadiusBridge - colliderHeight;
        float speedRadial = -direct * SpeedLinear * 10 * Time.deltaTime / rotateRadius;
        //Debug.Log("speedRadial: " + speedRadial + " rotateRadius: " + rotateRadius);
        float newDegree = (currDegree + speedRadial) / 180 * Mathf.PI;
        //Debug.Log("currDegree: " + currDegree + " newDegree: " + (currDegree + speedRadial));
        float posX = rotateRadius * Mathf.Sin(newDegree) + axisBridgeX;
        float posY = rotateRadius * Mathf.Cos(newDegree) + axisBridgeY;
        transform.position = new Vector3(posX, posY, 0);
        Vector3 vectorToPlanet = new Vector3(axisBridgeX, axisBridgeY, 0) - transform.position;
        vectorToPlanet.Normalize();

        transform.rotation = Quaternion.LookRotation(transform.forward, vectorToPlanet);

        if (direct != lastDir && !IsUlf)
            Flipping();
            //transform.rotation *= Quaternion.AngleAxis(180, Vector3.up);
    }
    //Summons
    public void SetPlanet(Planet newPlanet, int newDir_)
    {
        lastDir = newDir_;
        planet = newPlanet;
    }

    protected void set_Planet(Planet planet_curr)
    {
        RadiusPlanet = planet_curr.GetComponent<CircleCollider2D>().radius;
        axisPlanetX = planet_curr.transform.position.x;
        axisPlanetY = planet_curr.transform.position.y;
    }
    protected void set_Bridge(Bridge bridge_curr)
    {
        RadiusBridge = bridge_curr.GetComponent<CircleCollider2D>().radius * bridge_curr.transform.localScale.x;
        Vector3 centr = bridge_curr.Get_Center();
        axisBridgeX = centr.x;
        axisBridgeY = centr.y;
    }
    public Vector3 getPlanetCenter()
    {
        return new Vector3(axisPlanetX, axisPlanetY, 0);
    }
    public float getColliderHeight()
    {
        return colliderHeight;
    }
    public float getColliderWidth()
    {
        return colliderWidth;
    }
    public float getColliderOffsetY()
    {
        return GetComponent<CapsuleCollider2D>().offset.y * transform.localScale.y;
    }
   // public void Stop_Move(bool isEnable)
   // {
     //   stopMove = isEnable;
   // }
}