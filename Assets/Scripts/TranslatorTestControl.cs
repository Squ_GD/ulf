﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TranslatorTestControl : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
    }

    public void AttackAnim(int numAttack_)
    {
        if (TestControl.testControl)
            TestControl.testControl.AttackAnim(numAttack_);
        else
            Debug.Log("test Control not activate");
    }

    public void ActionsAnim(int num_)
    {
        if (TestControl.testControl)
        {
            //TestControl.testControl.ActionsAnim(act_, num_);
            switch(num_)
            {
                case 1:
                    TestControl.testControl.ActionsAnimIdle("idle", 1);
                    break;
                case 2:
                    TestControl.testControl.ActionsAnimIdle("idle", 2);
                    break;
                case 3:
                    TestControl.testControl.ActionsAnimIdle("idle", 3);
                   break;
                case 4:
                    TestControl.testControl.ActionsAnimRebind();
                    break;
                case 5:
                    TestControl.testControl.ActionsAnimDamage();
                    break;
                case 6:
                    TestControl.testControl.ActionsAnimDeath();
                    break;
            }
        }
        else
            Debug.Log("test Control not activate");
    }

}
