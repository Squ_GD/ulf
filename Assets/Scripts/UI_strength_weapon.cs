﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_strength_weapon : MonoBehaviour {
    private Sprite[] numbers;
    SpriteRenderer mainNumber, overlayNumber;
    private GameObject effectObj;
    public bool left;


    private void CopyArray()
    {
        Sprite[] temp_numbers = Resources.LoadAll<Sprite>("UI\\numbers");
        numbers = new Sprite[temp_numbers.Length / 2];

        //numbers = new Sprite[16];
        int start = 0;

        if (!left)
            start = 16;

        for (int c = 0; c < 16; c++)
        {
            numbers[c] = temp_numbers[start + c];
        }
    }

    private void OnEnable()
    {
        effectObj = transform.GetChild(1).gameObject;
        CopyArray();
        mainNumber = GetComponent<SpriteRenderer>();
        overlayNumber = transform.GetChild(0).GetComponent<SpriteRenderer>();
        EventManager.AddListener("Weapon strength", WEaponChangeStrength);
    }

    private void WEaponChangeStrength(BecomeEvent BE)
    {

        //Debug.Log("Strength: " + BE.power);
        if (left != BE.come )
            return;
        //StopAllCoroutines();
        effectObj.SetActive(true);
        if (BE.power > 0)
        {
            StopAllCoroutines();
            mainNumber.sprite = numbers[BE.power * 2 - 2];
            mainNumber.color = new Color(1, 1, 1, 1);
            StartCoroutine(overlayHide(BE.power * 2 - 1));
        }
        else
        {
            StartCoroutine(mainHide());
        }

    }

    IEnumerator mainHide()
    {

        for (float a = 1; a > 0; a -= .05f)
        {
            mainNumber.color = new Color(1, 1, 1, a);
            yield return new WaitForSeconds(.01f);
        }
        mainNumber.color = new Color(1, 1, 1, 0);

    }
    IEnumerator overlayHide(int num)
    {
        //yield return new WaitForSeconds(1f);
        overlayNumber.sprite = numbers[num];
        for (float a = .3f; a < 1; a += .1f)
        {
            overlayNumber.color = new Color(1, 1, 1, a);
            yield return new WaitForSeconds(.005f);
        }
        for (float a = 1; a > 0; a-=.05f)
        {
            overlayNumber.color = new Color(1, 1, 1, a);
            yield return new WaitForSeconds(.01f);
        }
        overlayNumber.color = new Color(1, 1, 1, 0);

    }
}
