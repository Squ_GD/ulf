﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class canvasSwitch : MonoBehaviour {
    Canvas canvas;
	// Use this for initialization
	void Start () {
        canvas = GetComponent<Canvas>();
        SetScale(canvas.enabled);

    }
    private void SetScale(bool enable)
    {
        if (enable)
        {
            Time.timeScale = 0;
        }
        else
            Time.timeScale = 1;
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Cursor.visible = false;
            bool isEnable = !canvas.enabled;
            canvas.enabled = isEnable;
            SetScale(isEnable);
        }
    }
}
