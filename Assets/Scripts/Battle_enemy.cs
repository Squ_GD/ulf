﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Battle_enemy : Battle_Basis {
    private bool touching;
    private float minDistAttack = 999f;
    private behavior behavior;

    public Weapon_Basis[] Enable_weapons;
    public enum Status_Attack { passive, agressive, runAway};
    public float LookDistance;
    public LootList loot;
    public int planetType, enemyTypeInPlanet;
    
    private Status_Attack status;
    private bool mouseOver;
    //private AudioSource audioPlayer;
    // Use this for initialization

    private void OnEnable()
    {
        //audioPlayer = gameObject.AddComponent<AudioSource>();
        //audioPlayer.rolloffMode = AudioRolloffMode.Custom;
        //audioPlayer.maxDistance = 20;
        //audioPlayer.playOnAwake = false;
        myID = Statistic.GetID(this);
        behavior = GetComponent<behavior>();
        status = Status_Attack.passive;
        EnableWeapons();
        controlMove = GetComponent<Control_enemy>();

        minDistAttack = GetMinDistAttack();

        StartCoroutine(DecidingAttack());
    }

    public void AddToPlanet(Planet _planet)
    {

        _planet.AddEnemy(myID, transform);
    }

    

    private void EnableWeapons()
    {
        int _length = Enable_weapons.Length;
        Weapon_array = new Weapon[_length];
        for(int e = 0; e < _length; e++)
        {
            Enable_weapons[e].initID(myID);
            Weapon_array[e] = Enable_weapons[e].weapon;
            Weapon_array[e].tagWeapon = gameObject.tag;
            //Enable_weapons[e].transform.localScale = transform.localScale;
        }
    }

    private float GetMinDistAttack()
    {
        float minimum = 999f;
        for (int w = 0; w < Weapon_array.Length; w++)
        {
            if (Weapon_array[w].Priority < 0)
                continue;
            if (calcWeaponRange(w) < minimum)
                minimum = calcWeaponRange(w);
        }
        if (minimum == 999f)
            minimum = 1f;

        return (minimum ) ;
    }
    

    private int GetBestAttack()
    {


        float distToUlf = controlMove.GetDistToUlf();
        int attackReady = -1;


        for (int wn = 0; wn < Weapon_array.Length; wn++)
        {

            if (Weapon_array[wn].Recharging() ||
                Weapon_array[wn].Priority < 0 ||
                //В здании и оружие ближнего боя
                (controlMove.InBuild() && (int)Weapon_array[wn].type_weapon < 6))
                continue;
            float weaponRange = calcWeaponRange(wn);
            if (weaponRange >= distToUlf && Weapon_array[wn].minDistance < distToUlf)
            {
                if (attackReady == -1)
                    attackReady = wn;
                else if (Weapon_array[attackReady].Priority == Weapon_array[wn].Priority)
                {
                    attackReady = Customise.GetInt(0, 2) == 1 ? attackReady : wn;
                }
                else if (Weapon_array[attackReady].Priority < Weapon_array[wn].Priority)
                    attackReady = wn;

            }
        }

        return attackReady;

}
    private float calcWeaponRange(int _weapon)
    {

        float _range = Weapon_array[_weapon].Distance + myCollider.size.x / 2f;
        if (Weapon_array[_weapon].IsMelee())
            _range *= ScaleX;

        return _range;
    }
    IEnumerator DecidingAttack()
    {
        int countToGetDistance = 0;
        while(true)
        {
            yield return new WaitForSeconds(.1f);
            float distToUlf = controlMove.GetDistToUlf();
            bool lookingUlf = controlMove.GetStatusLookUlf();
            bool rightDist = distToUlf <= LookDistance;

            if (rightDist && lookingUlf)
            {
                touching = distToUlf <= minDistAttack;
            }


            if (usedWeapon > -1 || Weapon_array.Length == 0)
                continue;

            
            if (distToUlf < 0)
            {
                controlMove.GoToUlf(false);
                ChangeStatus(Status_Attack.passive);
                continue;
            }
            //bool rightDist = distToUlf <= LookDistance;
            //В зоне досягаемости
            if (rightDist && status != Status_Attack.runAway)
            {
                if (behavior && countToGetDistance++ > 10)
                {
                    behavior.SetDistUlf(distToUlf);
                }
                controlMove.GoToUlf(true);
                ChangeStatus(Status_Attack.agressive);
            }
            else 
            {
                if(behavior)
                    behavior.SetDistUlf(-1);

                // Debug.Log("Not go ulf, status: " + status);
                controlMove.GoToUlf(false);
            }
            //В зоне досягаемости и видим
            if (rightDist && lookingUlf)
            {
                minDistAttack = GetMinDistAttack();
                int bestAttack = GetBestAttack();

                if (bestAttack != -1)
                    AttackingUlf(bestAttack);
            }
            else
                touching = false;
            
        }
    }

    public void AttackMelee()
    {
        if (!_animator.GetBool("death"))
        {
            Enable_weapons[usedWeapon].Attack();
        }
    }
    protected override void SetLoot()
    {
        GameObject lootGO = Instantiate(new GameObject());
        //lootGO.transform.parent = transform;
        lootGO.transform.position = transform.position;
        lootGO.AddComponent<CapsuleCollider2D>();
        CapsuleCollider2D lootCollider_ = lootGO.GetComponent<CapsuleCollider2D>();
        lootCollider_.isTrigger = true;
        lootCollider_.tag = "lootable";
        lootCollider_.size = new Vector2(1f, 1f);
        lootGO.AddComponent<lootObject>();
        lootGO.GetComponent<lootObject>().SetItems(loot);
        lootGO.GetComponent<lootObject>().EnemyType = enemyTypeInPlanet;
        lootGO.GetComponent<lootObject>().planetType = planetType;
    }
    //Когда все проверки пройдены, просто атакуем
    private void AttackingUlf(int _attack)
    {
        usedWeapon = _attack;

        _animator.SetInteger("attack", usedWeapon);
        IsAttacked(true);
    }

    public void ChangeStatus(Status_Attack _status)
    {
        if (_status != status)
        {
            int sendTypeCharacter = (int)bloodType * 10;
            sendTypeCharacter += heals;
            bool _agr = false;
            if (_status == Status_Attack.agressive)
                _agr = true;
           // EventManager.Invoke("EnemyAgressive", new BecomeEvent(_agr, myID, sendTypeCharacter));
        }

        status = _status;
    }
    //Вызывается из Planet
    public void UlfLook(bool isLook)
    {
        int sendTypeCharacter = (int)bloodType * 10;
        sendTypeCharacter += heals;

        EventManager.Invoke("EnemyLook", new BecomeEvent(isLook, myID, sendTypeCharacter));
    }
    public Status_Attack GetStatus()
    {
        return status;
    }

    public void InvertPrioritiesWeapon(bool closeFight)
    {
        for (int e = 0; e < Weapon_array.Length; e++)
        {
            if (Weapon_array[e].IsMelee() && closeFight)
                Weapon_array[e].Priority = Mathf.Abs(Weapon_array[e].Priority);
            else if(!Weapon_array[e].IsMelee() && !closeFight)
                Weapon_array[e].Priority = Mathf.Abs(Weapon_array[e].Priority);
            else if (!Weapon_array[e].IsMelee() && closeFight)
                Weapon_array[e].Priority = Mathf.Abs(Weapon_array[e].Priority) * -1;

            //Enable_weapons[e].transform.localScale = transform.localScale;
        }
        // Disarm();
       // Debug.Log("InvertPriorities, close: " + closeFight);
    }
    // from anim
    public void AttackShield(int attackEnable)
    {
        if (!_animator.GetBool("death"))
        {
            if(attackEnable == 1)
                Enable_weapons[usedWeapon].Attack(true);
            else
                Enable_weapons[usedWeapon].Attack(false);
        }


    }

    public void ShieldBreak()//from Shield script
    {
        _animator.SetTrigger("break shield");
    }
    public void AttackSummon()
    {
        if (!_animator.GetBool("death"))
        {
            Enable_weapons[usedWeapon].GetComponent<Weapon_Summon>().SetNewDir(controlMove.lastDir, transform, controlMove.GetPlanet());
            Enable_weapons[usedWeapon].Attack();
        }
    }

    public void Shoot()
    {
        if (!_animator.GetBool("death"))
        {
            Enable_weapons[usedWeapon].GetComponent<Weapon_Range>().setParameters(controlMove.getPlanetCenter(), controlMove.GetDistToUlf(), transform.localScale.x);
            Enable_weapons[usedWeapon].Attack();
            string weaponType = Enable_weapons[usedWeapon].weapon.type_weapon == Weapon.Type.spear ? "spear" : "bow";
            //AudioClip shootClip = Resources.Load<AudioClip>("Sound\\General\\" + weaponType);
            //audioPlayer.PlayOneShot(shootClip);
        }
    }

    public void AttackTargetMagic()
    {
        Enable_weapons[usedWeapon].GetComponent<Weapon_Target>().SetTarget(Control_Ulf.ulf_static.transform);
        Enable_weapons[usedWeapon].Attack();
    }

    public void ResetUsedWeapon()
    {
        usedWeapon = -1;
        _animator.SetInteger("attack", usedWeapon);
    }
    public int GetUsedWeapon()
    {
        return usedWeapon;
    }
    //To Test Control
    public void SetUsedWeapon(int used_)
    {
        usedWeapon = used_;
    }
    public void ResetAttack()
    {
        IsAttacked(false);
    }

    public override bool CanMove()
    {
        bool can = !touching && !IsAttacked() && !IsDamaged;

        if (can)
            return true;
        else
        {
            return false;

        }
    }
    private void OnMouseOver()
    {
        if (tag != "enemy")
            return;
        if (mouseOver)
            return;
        mouseOver = true;
        int sendTypeCharacter = (int)bloodType * 10;
        sendTypeCharacter += heals;
        EventManager.Invoke("MouseOver", new BecomeEvent(mouseOver, myID, sendTypeCharacter));
        Debug.Log("Over");
    }
    private void OnMouseExit()
    {
        if (tag != "enemy")
            return;
        mouseOver = false;
        EventManager.Invoke("MouseOver", new BecomeEvent(mouseOver, myID, 0));

    }
    
}
[System.Serializable]
public class LootList
{
    public string drop1, drop2, drop3;
    public int stuck1, stuck2, stuck3;
    public bool IsEmpty()
    {
        if (drop1 == "" && drop2 == "" && drop3 == "")
            return true;
        else
            return false;
    }
}
