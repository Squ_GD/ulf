﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Steamworks;

public class Converter : MonoBehaviour
{
    public static Converter converter;
    public GameObject enemyUlfPrefab;

    private Dictionary<int, Control_Ulf_Enemy> UlfEnemyList;

    private Dictionary<int,Planet> planets;

    private void Awake()
    {
        converter = this;
        UlfEnemyList = new Dictionary<int, Control_Ulf_Enemy>();
        planets = new Dictionary<int, Planet>();
    }

    private void OnEnable()
    {
        
    }
    // Start is called before the first frame update
    void Start()
    {
        EventManager.AddListener("Death", DeathUnit);
    /*
        NewPlayerData newData = new NewPlayerData();
        newData.typeData = "omenus";
        newData.paramData = 18;
        string jsonData = JsonUtility.ToJson(newData);
        NewPlayerData deserialized = new NewPlayerData();
        deserialized = JsonUtility.FromJson<NewPlayerData>(jsonData);

        Debug.Log("deserialized: " + deserialized.typeData + " " + deserialized.paramData);
    */
    }


    public void SetThrowAngleForAxe(float angle_, bool leftHand_)
    {
        NewPlayerData newData = new NewPlayerData();
        newData.typeData = "throwAngleAxe";
        newData.paramData = angle_;
        newData.leftRight = leftHand_;
        string jsonData = JsonUtility.ToJson(newData);
        //Debug.LogError("Send direct: " + direct_);
        PlayerNetSocket.playerNetSocket.SendAllPlayers(jsonData);
    }

    public void SwitchWeaponFromInventory(bool isLeft, string weaponName, int justDisarm)
    {
        NewPlayerData newData = new NewPlayerData();
        newData.typeData = "switchWeapon";
        newData.otherCondition = Inventory.inventory.GetNumSprWeapon(weaponName);
        newData.leftRight = isLeft;
        newData.paramData = justDisarm;
        string jsonData = JsonUtility.ToJson(newData);
        //Debug.LogError("Send direct: " + direct_);
        PlayerNetSocket.playerNetSocket.SendAllPlayers(jsonData);
    }

    public void AttackFromBattleUlf(bool leftHand_, bool riseHand, int customVar_)
    {
        NewPlayerData newData = new NewPlayerData();
        newData.typeData = "attackUlf";
        newData.otherCondition = customVar_;
        newData.paramData =    riseHand ? 1 : -1;
        newData.leftRight = leftHand_;
        string jsonData = JsonUtility.ToJson(newData);
        PlayerNetSocket.playerNetSocket.SendAllPlayers(jsonData);
    }

    public void AttackAlternative(bool alt_)
    {
        NewPlayerData newData = new NewPlayerData();
        newData.typeData = "attackAlt";
        newData.leftRight = alt_;
        string jsonData = JsonUtility.ToJson(newData);
        PlayerNetSocket.playerNetSocket.SendAllPlayers(jsonData);
    }

    public void RegisterPlanet(Planet planet_)
    {
        int planetNum = System.Convert.ToInt32(planet_.name);
        planets.Add(planetNum, planet_);
    }

    public void First_Stand_Ulf()
    {
        int rndNum = new System.Random().Next(planets.Keys.Count);
        Planet firstPlanet;
        while(!planets.TryGetValue(rndNum, out firstPlanet))
        {
            rndNum = new System.Random().Next(planets.Keys.Count);
        }

        float startDeg = firstPlanet.Get_Random_Degree(Control_Ulf.ulf_static);
        SendStartPosToAll(startDeg, rndNum);
    }

    public void SendCorrectDegUlf(float currDeg_)
    {
        NewPlayerData newData = new NewPlayerData();
        newData.typeData = "correctDegUlf";
        newData.paramData = currDeg_;
        string jsonData = JsonUtility.ToJson(newData);
        PlayerNetSocket.playerNetSocket.SendAllPlayers(jsonData);
    }

    public void ReceivedMessage(string JsonData_, int id_)
    {
        NewPlayerData deserialized = new NewPlayerData();
        deserialized = JsonUtility.FromJson<NewPlayerData>(JsonData_);
        //Debug.Log("typeData: " + deserialized.typeData);
        if (deserialized.typeData == "start")
        {
            GameObject ulf_ = Instantiate(enemyUlfPrefab);

            Planet startPlanet;
            planets.TryGetValue(deserialized.otherCondition, out startPlanet);

            startPlanet.Set_Ulf_Deg(deserialized.paramData, ulf_.GetComponent<Control_Ulf>());
            ulf_.GetComponent<Control_Ulf>().Set_New_Planet(startPlanet);

            UlfEnemyList.Add(id_, ulf_.GetComponent<Control_Ulf_Enemy>());
            ulf_.GetComponent<Battle_Ulf_Enemy>().SetMyID(id_);
        }
        else if (deserialized.typeData == "ready")
        {
            bool hisReady = deserialized.otherCondition == 1 ? true : false;
            PlayerNetSocket.playerNetSocket.EnemyReady(hisReady, id_);
        }
        else if (deserialized.typeData == "direct")
        {
            //Control_Ulf_Enemy ulfEnemy = UlfEnemyList[id_];
            UlfEnemyList[id_].Set_Old_Direct(deserialized.paramData);
        }
        else if (deserialized.typeData == "bridgeStand")
        {
            //Control_Ulf_Enemy ulfEnemy = UlfEnemyList[id_];
            UlfEnemyList[id_].Set_Bridge(deserialized.otherCondition);
        }
        else if (deserialized.typeData == "switchWeapon")
        {
            bool leftHand = deserialized.leftRight;
            if (deserialized.paramData != 1)
            {
                string nameWeapon = Inventory.inventory.GetNameSprNumWeapon(deserialized.otherCondition);
                //UlfEnemyList[id_].GetComponent<inventoryHandler>().CommandSwitchWeapon(leftHand, nameWeapon, false);
            }
            else
            {
                //UlfEnemyList[id_].GetComponent<inventoryHandler>().SwitchToHand(leftHand);
            }
        }
        else if (deserialized.typeData == "attackUlf")
        {
            bool leftHand = deserialized.leftRight;
            bool riseUpHand = deserialized.paramData > 0 ? true : false;
            UlfEnemyList[id_].GetComponent<Battle_Ulf_Enemy>().UlfAttacking(leftHand, riseUpHand, deserialized.otherCondition);
        }
        else if (deserialized.typeData == "attackAlt")
        {
            bool trueAlt = deserialized.leftRight;
            UlfEnemyList[id_].AttackAlt(trueAlt);
        }
        else if (deserialized.typeData == "throwAngleAxe")
        {
            UlfEnemyList[id_].GetComponent<Battle_Ulf_Enemy>().ThrowAxe(deserialized.paramData, deserialized.leftRight);
        }
        else if (deserialized.typeData == "Death")
        {
            UlfEnemyList[id_].GetComponent<Battle_Ulf_Enemy>().Death();
        }
        else if (deserialized.typeData == "healUlf")
        {
            if(deserialized.leftRight)//heart
            {
                UlfEnemyList[id_].GetComponent<Battle_Ulf_Enemy>().HeartEath();

            }
            else//bone
                UlfEnemyList[id_].GetComponent<Battle_Ulf_Enemy>().BoneEath();

        }
        else if (deserialized.typeData == "correctDegUlf")
        {
            UlfEnemyList[id_].CorrectDegree(deserialized.paramData);
        }
        
    }

    public void HealUlf(bool isHeart_)
    {
        NewPlayerData newData = new NewPlayerData();
        newData.typeData = "healUlf";
        newData.leftRight = isHeart_;

        string jsonData = JsonUtility.ToJson(newData);
        PlayerNetSocket.playerNetSocket.SendAllPlayers(jsonData);
    }

    public void SendStandBridge(int numBridge_)
    {
        NewPlayerData newData = new NewPlayerData();
        newData.typeData = "bridgeStand";
        newData.otherCondition = numBridge_;

        string jsonData = JsonUtility.ToJson(newData);
        PlayerNetSocket.playerNetSocket.SendAllPlayers(jsonData);
    }

    public void SendNewDirect(float direct_)
    {
        NewPlayerData newData = new NewPlayerData();
        newData.typeData = "direct";
        newData.paramData = direct_;
        //newData.otherCondition = 1;
        string jsonData = JsonUtility.ToJson(newData);
        //Debug.LogError("Send direct: " + direct_);
        PlayerNetSocket.playerNetSocket.SendAllPlayers(jsonData);
    }

    public void SendReadyToAll(bool enable_)
    {
        NewPlayerData newData = new NewPlayerData();
        newData.typeData = "ready";
        if(enable_)
        {
            newData.otherCondition = 1;
        }

        string jsonData = JsonUtility.ToJson(newData);
        PlayerNetSocket.playerNetSocket.SendAllPlayers(jsonData);
    }

    public void SendStartPosToAll(float degree_, int planetNum)
    {
        Control_Ulf.ulf_static.enabled = true;//ToDo

        NewPlayerData newData = new NewPlayerData();
        newData.typeData = "start";
        newData.paramData = degree_;
        newData.otherCondition = planetNum;
        string jsonData = JsonUtility.ToJson(newData);

        PlayerNetSocket.playerNetSocket.SendAllPlayers(jsonData);
    }

    private void DeathUnit(BecomeEvent be)
    {
        if(be.id == 0)
        {
            NewPlayerData newData = new NewPlayerData();
            newData.typeData = "Death";
            newData.otherCondition = 0;
            string jsonData = JsonUtility.ToJson(newData);

            PlayerNetSocket.playerNetSocket.SendAllPlayers(jsonData);
        }
    }
}
[System.Serializable]
public class NewPlayerData
{
    public string typeData;
    public float paramData;
    public int otherCondition;
    public bool leftRight;
}
