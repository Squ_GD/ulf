﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Equip : MonoBehaviour
{
    private Battle_Ulf ulfBattle;
    private WeaponItem handLeft, handRight;
    private WeaponItem weaponLeft, weaponRight;
    // Start is called before the first frame update

    private void OnEnable()
    {
        EventManager.AddListener("Weapon strength", changeStrength);
        
    }
    void Start()
    {
        ulfBattle = GetComponent<Battle_Ulf>();
        Inventory.inventory.SetEquip(this);

        handLeft = Inventory.inventory.GetSprWeapon("hand");
        handRight = Inventory.inventory.GetSprWeapon("hand");
        ulfBattle.WeaponLoad(true, handLeft);
        ulfBattle.WeaponLoad(false, handRight);
    }

    public void ChangeStrHand(bool main, int strength_plus)
    {
        if (main)
        {
            //((WeaponItem)handLeft).weaponScript.ChangeStrength(strength_plus);
        }
        //else
            //((WeaponItem)handRight).weaponScript.ChangeStrength(strength_plus);
    }

    public Item GetHand(bool left_)
    {
        if (left_)
            return handLeft;
        else
            return handRight;
    }

    public WeaponItem GetCurrentWeapon(bool left_)
    {

        if (left_)
        {
            if (weaponLeft == null)
                return handLeft;
            else
                return weaponLeft;
        }
        else
        {


            if (weaponRight == null)
                return handRight;
            else
                return weaponRight;
        }
    }

    public void EquipWeapon(ItemCell numPos, bool leftClick_)
    {
        
       

        WeaponItem _item = ((WeaponItem)numPos.item);
        if (_item.type == WeaponItem.TypeWeapon.shield && leftClick_)
            return;

        

        if (_item.type == WeaponItem.TypeWeapon.bow)
        {
            ItemCell arrowPos = Inventory.inventory.GetMissile();
            if (arrowPos == null)
                return;

            DisarmWeapon(true, false);
            weaponLeft = (WeaponItem)arrowPos.item;
            ulfBattle.SwitchingWeapon(true);
            arrowPos.RemoveItem();

            DisarmWeapon(false, false);
            weaponRight = _item;
            ulfBattle.SwitchingWeapon(false);

        }
        else// One handed
        {
            Debug.Log("One hand weapon equip");

            DisarmWeapon(leftClick_, false);
            bool bowEquiped = weaponLeft != null && weaponLeft.type == WeaponItem.TypeWeapon.bow;
            bowEquiped |= weaponRight != null && weaponRight.type == WeaponItem.TypeWeapon.bow;
            if (bowEquiped)
            {
                Debug.Log("Bow unequiped");

                DisarmWeapon(!leftClick_, false);
                ulfBattle.SwitchingWeapon(!leftClick_);
            }

            if (leftClick_)
            {
                weaponLeft = _item;
                ulfBattle.SwitchingWeapon(true);

            }
            else
            {
                weaponRight = _item;
                ulfBattle.SwitchingWeapon(false);
            }
        }
        numPos.RemoveItem();
    }


    private void changeStrength(BecomeEvent BE)
    {
        Item changingItem;
        if (BE.come)
            changingItem = weaponLeft == null ? handLeft : weaponLeft;
        else
            changingItem = weaponRight == null ? handRight : weaponRight;

        if(changingItem is MissileItem)//BOW(arrow in left hand)
        {
            //((MissileItem)changingItem).ChangeCount(BE.power);
            if(((MissileItem)changingItem).strength == 0)      
            {
                //DisarmWeapon(true, true);
                //DisarmWeapon(false, false);

                Control_Ulf.ulf_static.GetComponent<Battle_Ulf>().return_bow_InPos();
            }
            //return;
        }

        if (((WeaponItem)changingItem).type == WeaponItem.TypeWeapon.hand)
        {


            return;
        }
        if (changingItem is WeaponItem && ((WeaponItem)changingItem).strength <= 0)
        {
            
            int part_ = BE.come ? 1 : 2;

            if (Converter.converter)
            {
                Converter.converter.SwitchWeaponFromInventory(BE.come, "hand", 1);//TODO for bow
            }

            if (changingItem is MissileItem)
            {
                DisarmWeapon(!BE.come, false);
                //ulfBattle.WeaponLoad(!BE.come, handRight);
                ulfBattle.SwitchingWeapon(false);
            }

            DisarmWeapon(BE.come, true);
            ulfBattle.WeaponLoad(BE.come, BE.come ? handLeft : handRight);
            Debug.Log(changingItem.name + " strength 0");

        }
    }
    /*
    private void CommandSwitchWeapon(bool leftHand_, Item newWeapon_, bool destroyWeapon)
    {
        ulfBattle.SetNewWeapon(leftHand_, newWeapon_);


        ulfBattle.SwitchingWeapon(leftHand_);
        if (destroyWeapon)
        {
            ulfBattle.WeaponDisarm(leftHand_ ? 1 : 2);
        }

    }
    */
    private void DisarmWeapon(bool left_, bool breakWeapon)
    {
        if(left_)
        {
            if(weaponLeft == null)
             return;

            if (!breakWeapon)
               Inventory.inventory.ReturnWeapon(weaponLeft);
            weaponLeft = null;
                //MoveItem(_pos, bodyLoad[partBody]);
        }
        else
        {
            if (weaponRight == null)
                return;

            if (!breakWeapon)
                Inventory.inventory.ReturnWeapon(weaponRight);
            weaponRight = null;
        }


        

    }
    // Update is called once per frame
   
}
