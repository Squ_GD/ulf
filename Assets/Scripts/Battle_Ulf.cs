﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Battle_Ulf : Battle_Basis {
    public Transform bone_main, bone_extra, bone_extra_shield, bone_extra_bow, quiver_bow;
    private int rib = 6;
    protected int numWeaponMain, numWeaponExtra;
    protected bool healing;
    public GameObject rightHandEmpty, rightHandWeapon, leftHandEmpty, leftHandWeapon, arrowAnimGO;
    private GameObject mainWeaponGO, extraWeaponGO;
    protected Weapon_Basis weaponMain, weaponBack;
    //private AudioSource audioPlayer;
    private bool isInactive, inventoryOpened;
    //private Item.typeItem oldWeaponTypeMain, oldWeaponTypeExtra;
    private int[] DrPS;
    private float[] timeDrps;
    private int currCell;
    private List<lootObject> lootingEnemy;
    private Item newWeaponLeft, newWeaponRight;
    private Equip equipMy;
    private Transform torsoFalseParent;
    public delegate void weaponEquipEvent(string nameItem);
    public weaponEquipEvent OnEquipWeapon;
    public static Battle_Ulf battleUlf;
    private void Awake()
    {
        if(!battleUlf)
            battleUlf = this;

    }
    private void OnEnable()
    {
        EventManager.AddListener("Inventory", openInventory);
        lootingEnemy = new List<lootObject>();
        currCell = 0;
        DrPS = new int[5];
        timeDrps = new float[5];
        myID = Statistic.GetID(this);
        Weapon_array = new Weapon[2];
        _animator = GetComponent<Animator>();
        //Установить начальные оружия
        //WeaponLoad(1, "hand");
        numWeaponMain = -1;
        //WeaponLoad(2, "hand");
        numWeaponExtra = -1;
        //StartCoroutine(GetCustom());
        //audioPlayer = GetComponents<AudioSource>()[0];
        rightHandWeapon.GetComponent<SkinnedMeshRenderer>().enabled = false;
        leftHandWeapon.GetComponent<SpriteRenderer>().enabled = false;
        equipMy = GetComponent<Equip>();
    }
    //change weapon from Inventory
    public void WeaponLoad(bool main, WeaponItem weaponItem)//, int inv_strength)
    {
        if(OnEquipWeapon != null)
            OnEquipWeapon(weaponItem.name);
        //Debug.Log("weaponItem: " + weaponItem.name);
        string weaponName = weaponItem.name;
        if (main)
        {
            WeaponItem.TypeWeapon type_ = weaponItem.type;
            if (type_ == WeaponItem.TypeWeapon.hand)
                ChangeSkin(2, true);
            else
                ChangeSkin(2, false);

            if (mainWeaponGO && !mainWeaponGO.GetComponent<Quiver_Weapon>())
            {
                Destroy(mainWeaponGO);
            }

            if (type_ == WeaponItem.TypeWeapon.bow)
            {
                mainWeaponGO = quiver_bow.gameObject;
                SetMissile(weaponItem.strength);
            }
            else
            {
                weaponName += "_main";
                mainWeaponGO = (GameObject)Instantiate(Resources.Load("Weapons\\Ulf\\" + weaponName));
                Vector3 _scale = mainWeaponGO.transform.localScale;
                Vector3 _pos = mainWeaponGO.transform.localPosition;
                Quaternion _rotate = mainWeaponGO.transform.localRotation;
                mainWeaponGO.transform.parent = bone_main;
                mainWeaponGO.transform.localPosition = _pos;
                mainWeaponGO.transform.localRotation = _rotate;
                mainWeaponGO.transform.localScale = _scale;

                if (arrowAnimGO.activeSelf)//Если сняли лук - убираем стрелу
                {
                    arrowAnimGO.SetActive(false);
                }
            }
            weaponMain = mainWeaponGO.GetComponent<Weapon_Basis>();//mainWeaponGO.GetComponent<Weapon_Basis>();
            Weapon_array[0] = weaponMain.weapon;
            numWeaponMain = weaponMain.weapon.Priority;
            weaponMain.SetItem(weaponItem);

            if (!GetComponent<Battle_Ulf_Enemy>())
                weaponMain.IsUlfWeapon(1);

            if (type_ == WeaponItem.TypeWeapon.bow)
                weaponMain.ChangeStrength(0);//Init arrow in UI


            EventInvoking("Switch weapon", new BecomeEvent(true, 0, numWeaponMain));


            ResetUsedWeapon(false);

            mainWeaponGO.name = weaponName;
            _animator.SetInteger("weaponMain", Weapon_array[0].Priority);

        }
        else
        {
            WeaponItem.TypeWeapon type_ = weaponItem.type;

            if (type_ == WeaponItem.TypeWeapon.hand)
                ChangeSkin(1, false);
            else
                ChangeSkin(1, true);


            if (extraWeaponGO)
            {
                if(weaponBack.GetComponent<Weapon_Shield>())
                    _animator.SetInteger("weaponExtra", -1);

                Destroy(extraWeaponGO);
            }

            if (weaponName != "shield")
                weaponName += "_extra";

            extraWeaponGO = (GameObject)Instantiate(Resources.Load("Weapons\\Ulf\\" + weaponName));
            Vector3 _scale = extraWeaponGO.transform.localScale;
            Vector3 _pos = extraWeaponGO.transform.localPosition;
            Quaternion _rotate = extraWeaponGO.transform.localRotation;
            weaponBack = extraWeaponGO.GetComponent<Weapon_Basis>();//mainWeaponGO.GetComponent<Weapon_Basis>();
            weaponBack.SetItem(weaponItem);
            if (weaponBack && weaponBack.weapon.IsBow())
            {
                arrowAnimGO.SetActive(false);
            }


            //weaponBack = extraWeaponGO.GetComponent<Weapon_Basis>();

            //SHIELD
            if (weaponName == "shield")
                extraWeaponGO.transform.parent = bone_extra_shield;
            else if (weaponBack.weapon.IsBow())
            {
                weaponBack.ChangeStrength(Missile_Left());
                //quiver_bow.gameObject.SetActive(true);
                arrowAnimGO.SetActive(true);
                extraWeaponGO.transform.parent = bone_extra_bow;
                numWeaponMain = weaponBack.weapon.Priority;
            }
            else
                extraWeaponGO.transform.parent = bone_extra;

            extraWeaponGO.transform.localPosition = _pos;
            extraWeaponGO.transform.localRotation = _rotate;
            extraWeaponGO.transform.localScale = _scale;
            Weapon_array[1] = weaponBack.weapon;
            numWeaponExtra = weaponBack.weapon.Priority;

            if (!GetComponent<Battle_Ulf_Enemy>())
            weaponBack.IsUlfWeapon(2);


            EventInvoking("Switch weapon", new BecomeEvent(false, 0, numWeaponExtra));

            ResetUsedWeapon(true);

            extraWeaponGO.name = weaponName;

            _animator.SetInteger("weaponExtra", Weapon_array[1].Priority);

        }

    }
    public void WeaponDisarm(int bodyPart)
    {
        
        if (bodyPart == 1 && weaponMain)
        {
            Destroy(weaponMain.gameObject);
        }
        else if (bodyPart == 2 && weaponBack)
        {
            Destroy(weaponBack.gameObject);
        }
    }

    public void SetNewWeapon(bool inLeftHand, Item weaponItem)
    {
        if (inLeftHand)
            newWeaponLeft = weaponItem;
        else
            newWeaponRight = weaponItem;
    }

    public void HideExtraWeapon(int hide)
    {
        if (hide == 1)
            healing = true;
        else
            healing = false;
        if (weaponBack.weapon.type_weapon != Weapon.Type.hand)
        {
            DeactiveExtraZP(hide + 1);
            if (hide == 1)
                weaponBack.gameObject.SetActive(false);
            else
                weaponBack.gameObject.SetActive(true);
        }
    }

    public void ChangeSkin(int skinPart, bool activate)
    {
        switch(skinPart)
        {
            case 2:
                rightHandEmpty.GetComponent<SkinnedMeshRenderer>().enabled = activate;
                rightHandWeapon.GetComponent<SkinnedMeshRenderer>().enabled = !activate;
                break;
            case 1:
                leftHandWeapon.GetComponent<SpriteRenderer>().enabled = activate;
                break;
        }
    }

    public void HealBlood()
    {
        Weapon healWeapon = new Weapon();
        healWeapon.Power = -22;
        healWeapon.type_weapon = Weapon.Type.magic;
        Attacking(healWeapon, transform.position);
    }

    public void BoneRestore()
    {
        rib = rib < 6 ? rib + 1 : 6;
        EventInvoking("rib", new BecomeEvent(true, myID, rib));
        if(Weapon_array[0].Power == 1)
        {
            //if (weaponMain.strength < 8)
              //  weaponMain.ChangeStrength(1);
        }
        else
        {
            //Inventory.inventory.ChangeStrHand(true, 1);
        }
        if (Weapon_array[1].Power == 1)
        {
            //if (weaponBack.strength < 8)
              //  weaponBack.ChangeStrength(1);
        }
        else
        {
            
            //Inventory.inventory.ChangeStrHand(false, 1);
        }
    }

    public void ResetUsedWeapon(bool ExtraHand)
    {
        if (!ExtraHand)
        {
            //_animator.SetInteger("weaponMain", -1);
        }
        else
        {
            //_animator.SetInteger("weaponExtra", -1);
        }

    }

   
    public void SetMissile(int count_)
    {
        _animator.SetInteger("missileLeft", count_);
        
        for (int c = 0; c < quiver_bow.childCount; c++)
        {
            GameObject arrowGOForOff = quiver_bow.GetChild(c).gameObject;
            if (c < count_-1)
            {
                arrowGOForOff.SetActive(true);
            }
            else
                arrowGOForOff.SetActive(false);

        }
    }

    private int Missile_Left()
    {

        return weaponMain.item.strength;
    }

    public void Bow_Shoot_Finish(int notDestroyArrow)//And quiver cost
    {
        if (notDestroyArrow == 0)//From anim
        {
            //weaponMain.Attack(true);
            _animator.SetInteger("missileLeft", Missile_Left());
            SetMissile(Missile_Left());
            /*
            for (int c = 0; c < quiver_bow.childCount; c++)
            {
                _animator.SetInteger("missileLeft", quiver_bow.childCount - (c + 1));
                GameObject arrowGOForOff = quiver_bow.GetChild(c).gameObject;
                if (arrowGOForOff.activeSelf)
                {
                    arrowGOForOff.SetActive(false);

                    break;
                }
            }*/
        }
        else//cancel shoot
        {
            EventInvoking("Ulf attack", new BecomeEvent(false, -1, Weapon_array[1].Priority));

        }
        StartCoroutine(Bow_Return_InPos());
    }
    public void Bow_Shoot_Anim(int part_)
    {
        if (part_ == -1)
            weaponBack.GetComponent<Animator>().Rebind();
        else if (part_ == 1)
            weaponBack.GetComponent<Animator>().Play("ulf_bow_finish_last");
        else
            weaponBack.GetComponent<Animator>().SetTrigger("finishShoot");
    }
    public void return_bow_InPos()
    {
        StartCoroutine(Bow_Return_InPos());
    }
    private IEnumerator Bow_Return_InPos()
    {
        float minDeg = 0;
        float currAngleTorso = torsoFalseParent.localEulerAngles.z;
        int dir = 0;
        if (currAngleTorso < 180f)
        {
            minDeg = currAngleTorso;
            dir = -1;
        }
        else
        {
            minDeg = 360f - currAngleTorso;
            dir = 1;
        }
        const int steps = 25;
        float step = minDeg * dir / steps;
        for(int i = 0; i < steps; i++)
        {
            torsoFalseParent.Rotate(Vector3.forward, step);

            yield return null;
        }

    }

    public void Bow_Targeting()//From animation bow_start
    {
        //_animator.enabled = false;
        //_animator.cullingMode = AnimatorCullingMode.CullUpdateTransforms;
        Debug.Log("Targeting start");
        if(!torsoFalseParent)
        {
            torsoFalseParent = new GameObject("false").transform;
            torsoFalseParent.parent = Missile_stick_transform.parent;
            torsoFalseParent.localPosition = Missile_stick_transform.localPosition;
            torsoFalseParent.localRotation = new Quaternion();
            Missile_stick_transform.parent = torsoFalseParent;
            
            _animator.Play("bow_shot", _animator.layerCount);
        }

        StartCoroutine(LoopTargeting());
    }
    
    IEnumerator LoopTargeting()
    {

        const float maxRotationTorso = 37f, minRotationTorso = 335f;
        while (_animator.GetInteger("weaponMain") == 101 && (_animator.GetBool("attackMain") && !_animator.GetBool("attackExtra")))//Arrow
        {
            int directRotate = 0;
            if (Control.control.moveUp.press)
                directRotate = 1;
            else if (Control.control.moveDown.press)
                directRotate = -1;

            float currAngleTorso = torsoFalseParent.localEulerAngles.z;
            float rot_speed = 60f * Time.deltaTime * directRotate; // * angle;
            float futureAngle = currAngleTorso + rot_speed;
            if (futureAngle < 100f && futureAngle <  maxRotationTorso
                || futureAngle > 300f && futureAngle > minRotationTorso
                || futureAngle < 100f && 360f - futureAngle > minRotationTorso)
            {
                torsoFalseParent.Rotate(Vector3.forward,  rot_speed);
            }
            
            yield return null;
        }
        
    }
    
    private void downStack(int d)
    {
        for(int s = 0; s <= d; s++)
        {
            timeDrps[s] = timeDrps[s+1];
            DrPS[s] = DrPS[s+1];
            currCell--;
        }
    }

    private bool countDrps()
    {
        for (int d = currCell-1; d > 0; d--)
        {
            if ((timeDrps[currCell] - timeDrps[d]) >= 1f)
            {
                //Debug.Log("timeDrps: " + timeDrps[currCell] + " timeDrps: " + timeDrps[d]);

                downStack(d);
            }
        }
        int totalDamage = 0;
        for (int d = 0; d <= currCell; d++)
        {
            totalDamage += DrPS[d];
        }

        //Debug.Log("total DPS: " + totalDamage + " CurrCell: " + currCell);
        if (totalDamage >= 20)
        {
            currCell = 0;
            DrPS[currCell] = 0;
            timeDrps[currCell] = 0;
            return true;
        }
        else
            return false;
    }

    protected override int WeakDamage(int dmg, Weapon weapon)
    {
        if (weapon.IsMagic())
            return 1;
        if (rib > 0)
        {
            DrPS[currCell] = dmg;
            timeDrps[currCell] = Time.realtimeSinceStartup;
            if (countDrps())
                EventInvoking("rib", new BecomeEvent(true, myID, --rib));
            else
                currCell++;
        }
        else
        {
            return 100;
        }
        return 1;
    }

    //From anim_attack
    public void AttackingMain(int attacking)
    {
        bool isAttacking = attacking == 0 ? false : true;
        if (attacking == 2 && weaponMain.GetComponent<Weapon_Hybrid>())
        {
            float throwAngle = 0;

            if (!GetComponent<Battle_Ulf_Enemy>())
            {
                throwAngle = 0;// weaponMain.GetComponent<Weapon_Hybrid>().chooseAngleForThrow();

                if(Converter.converter)
                {
                    Converter.converter.SetThrowAngleForAxe(throwAngle, true);
                }

                weaponMain.GetComponent<Weapon_Hybrid>().
            Throw(GetComponent<Control_Ulf>().getPlanetCenter(), GetComponent<Control_Ulf>().SpeedLinear, throwAngle, GetComponent<Collider2D>(), transform);
            }
            else
                weaponMain.GetComponent<Weapon_Hybrid>().ThrowDisconnect();
        }
        if (attacking == 3 && Weapon_array[1].IsBow())
        {
            Vector3 planetCenter_ = checkPlanetBeforeShoot();

            weaponBack.GetComponent<Weapon_Range>().setParameters(planetCenter_, weaponBack.weapon.Distance, transform.localScale.x);

            weaponBack.GetComponent<Weapon_Range>().Attack();
            weaponMain.Attack(true);

        }

        if (isAttacking)
        {
            EventInvoking("Ulf attack", new BecomeEvent(true, 0, Weapon_array[0].Priority));
            Debug.Log("Attack main hand");

        }
        if (Weapon_array[0].IsMelee())
        {
            weaponMain.Attack(isAttacking);

        }
    }

    //From anim_attack
    public void AttackingBack(int attacking)
    {

        bool isAttacking = attacking == 0 ? false : true;
        if (attacking == 2 && weaponBack.GetComponent<Weapon_Hybrid>())
        {
            float throwAngle;


            if (!GetComponent<Battle_Ulf_Enemy>())
            {
                throwAngle = 0;// weaponBack.GetComponent<Weapon_Hybrid>().chooseAngleForThrow();

                if (Converter.converter)
                {
                    Converter.converter.SetThrowAngleForAxe(throwAngle, false);
                }

                weaponBack.GetComponent<Weapon_Hybrid>().
                  Throw(GetComponent<Control_Ulf>().getPlanetCenter(), GetComponent<Control_Ulf>().SpeedLinear, throwAngle, GetComponent<Collider2D>(), transform);

            }
            else
                weaponBack.GetComponent<Weapon_Hybrid>().ThrowDisconnect();
            

        }

        

        if (Weapon_array[1].IsMelee() || Weapon_array[1].IsSHield())
        {
            weaponBack.Attack(isAttacking);
        }

        if (isAttacking)
        {
            EventInvoking("Ulf attack", new BecomeEvent(false, 0, Weapon_array[1].Priority));
            Debug.Log("Attack extra hand");
        }

    }

    private Vector3 checkPlanetBeforeShoot()
    {
        UlfDeg deg_ = GetComponent<Control_Ulf>().deg;
        if (deg_.bridge)
        {
            Vector3 center_ = Vector3.zero;
            Debug.Log("On bridge");
            Vector3 trans1 = deg_.bridge.startPlanet.transform.position;
            float rad1 = deg_.bridge.startPlanet.GetRadius();

            Vector3 trans2 = deg_.bridge.finishPlanet.transform.position;
            float rad2 = deg_.bridge.finishPlanet.GetRadius();

            Vector3 contrPoint = bone_extra_bow.position + bone_extra_bow.right * 5f;
            float dist1 = (contrPoint - trans1).magnitude - rad1;
            float dist2 = (contrPoint - trans2).magnitude - rad2;

            if (dist1 < dist2)
            {
                center_ = deg_.bridge.startPlanet.transform.position;
            }
            else 
            {
                center_ = deg_.bridge.finishPlanet.transform.position;
            }
            return center_;
        }
        else
            return deg_.planet.transform.position;
    }
    public override bool CanMove()
    {
        bool can = !IsDamaged;// && !healing;// && !isInactive; //ToDo
        // Debug.Log("can move: " + can);
        if (can)
            return true;
        else
            return false;
    }

    public override void StartRechargeWeapon(int usedWeap)
    {
        usedWeapon = usedWeap;

        StartCoroutine(RechargeWeapon());
    }
    /*
    public void SetOldWeaponType(int handNum, Item.typeItem typeWeapon)
    {
        if(handNum == 1)
        {
            oldWeaponTypeMain = typeWeapon;
        }
        else
            oldWeaponTypeExtra = typeWeapon;

    }

    private void ChangeSkin(int partBody, bool activate, Item.typeItem typeWeapon)
    {
        if (partBody == 1)
        {
            if (typeWeapon == Item.typeItem.hand)
            {
                ChangeSkin(1, activate);
            }
            else if (typeWeapon == Item.typeItem.one_hand_weapon || typeWeapon == Item.typeItem.two_hand_weapon)
            {
                ChangeSkin(2, activate);
            }
        }
        else
        {
            if (typeWeapon != Item.typeItem.hand)
            {
                ChangeSkin(3, activate);
            }
        }
    }
    */

    /*
    public void ChangingWeapon(int handNum, WeaponItem _weaponItem)
    {
        //Item _weaponItem = Inventory. inventory.GetCurrentWeapon(handNum);
        string _name = _weaponItem.name;
        int str = _weaponItem.strength;
        bool mainHandLoad = handNum == 1 ? true : false;
        if (mainHandLoad)
        {
            ChangeSkin(handNum, false, oldWeaponTypeMain);
            oldWeaponTypeMain = _weaponItem.type;
            if(_weaponItem.type != Item.typeItem.two_hand_weapon)
                WeaponLoad(mainHandLoad, _name, str);
        }
        else
        {
            ChangeSkin(handNum, false, oldWeaponTypeExtra);
            oldWeaponTypeExtra = _weaponItem.type;
            WeaponLoad(mainHandLoad, _name, str);
        }
        ChangeSkin(handNum, true, _weaponItem.type);

        if (_weaponItem.strength == 0)
        {
            if (mainHandLoad)
                numWeaponMain = -1;
            else
                numWeaponExtra = -1;
        }
        
    }
    */

    public void WeaponFinalChange(int main)//Animator
    {
        if (main == 1)
            WeaponLoad(true, equipMy.GetCurrentWeapon(true));
        else
            WeaponLoad(false, equipMy.GetCurrentWeapon(false));
    }

    public void UIChangeWeapon(int main)//Animator
    {
        if (main == 1)
            EventInvoking("Switch weapon", new BecomeEvent(true, 0, -1));
        else
            EventInvoking("Switch weapon", new BecomeEvent(false, 0, -1));
    }

    public override void SetRune(Rune _rune)
    {

        if (applRune.RuneType == Rune.TypeRune.empty)
        {
            applRune = _rune;
            RuneEffect(applRune, true);
            StartCoroutine(runeTimer());

        }
        else
            Debug.Log("Rune not empty!");
    }

    IEnumerator runeTimer()
    {
        yield return new WaitForSeconds(3.5f);
        RuneEffect(applRune, false);
        ReSetRune();
    }

    private void RuneEffect(Rune rune, bool aplied)
    {
        switch (rune.RuneType)
        {
        case Rune.TypeRune.stun:
                if(aplied)
                    StartCoroutine(StunCoroutine());
                break; 
            case Rune.TypeRune.root:
                //rootRuneAnimator.gameObject.SetActive(true);
                //rootRuneAnimator.Play("root");
                GetComponent<Control_Ulf>().Rooted(aplied, applRune.PeriodTime);
                break;
        }
        EventInvoking("RuneSet", new BecomeEvent(aplied, myID, (int)rune.RuneType));

    }

    private IEnumerator StunCoroutine()
    {
        float currMaxSpeed = Control_Ulf.ulf_static.MaxSpeed;
        float currAceleration = Control_Ulf.ulf_static.Acceleration;
        float stunSpeed = .3f;

        for(int i = 0; i < 10; i++)
        {
            ChangeSpeedAnim(stunSpeed, currMaxSpeed * stunSpeed, currAceleration * stunSpeed);

            stunSpeed += .07f;
            yield return new WaitForSeconds(.35f);
        }
        ChangeSpeedAnim(1, currMaxSpeed , currAceleration);

    }
    private void ChangeSpeedAnim(float speed_anim, float maxSpeed, float acceleration)
    {
        Debug.Log("Speed: " + speed_anim);
        _animator.speed = speed_anim;
        Control_Ulf.ulf_static.MaxSpeed = maxSpeed;
        Control_Ulf.ulf_static.Acceleration = acceleration;
    }

    

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "lootable")
        {
            lootingEnemy.Add(collision.GetComponent<lootObject>());
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "lootable")
        {
            if(lootingEnemy.Contains(collision.GetComponent<lootObject>()))
                lootingEnemy.Remove(collision.GetComponent<lootObject>());

        }
    }

    public void BoneEath()
    {
        _animator.SetTrigger("gnawing");

    }

    public void HeartEath()
    {
        _animator.SetTrigger("heal");

        //if (Input.GetKeyDown(KeyCode.R))
        //{
          //  _animator.SetTrigger("gnawing");
        //}
    }
    //From Inventory
    public void SwitchingWeapon(bool leftHand_)
    {
        if (leftHand_)
        {
            _animator.SetTrigger("switchMain");
        }
        else
        {
            _animator.SetTrigger("switchExtra");
        }
    }

    

    public void DeactiveMainHand(int deactive)
    {
        if (deactive == 1)
        {
            rightHandEmpty.GetComponent<SkinnedMeshRenderer>().enabled = false;
        }
        else //if(!rightHandWeapon.activeSelf)
        {
            rightHandEmpty.GetComponent<SkinnedMeshRenderer>().enabled = true;
        }
    }

    public void DeactiveExtraHand(int deactive)
    {
        if (deactive == 1)
        {
            leftHandEmpty.GetComponent<SkinnedMeshRenderer>().enabled = false;
        }
        else //if(!rightHandWeapon.activeSelf)
        {
            leftHandEmpty.GetComponent<SkinnedMeshRenderer>().enabled = true;
        }
    }

    public void DeactiveExtraZP(int active)
    {
        Debug.Log("ZP active: " + active);
        if (active == 1)
        {
            leftHandWeapon.GetComponent<SpriteRenderer>().enabled = true;
        }
        else
        {
            leftHandWeapon.GetComponent<SpriteRenderer>().enabled = false;

        }
    }

    public void SetActiveLeftHand(int activating)
    {
        //Debug.Log("Set active: " + activating);
        if (activating == 1)
        {
            leftHandEmpty.GetComponent<SkinnedMeshRenderer>().enabled = true;
            leftHandWeapon.GetComponent<SpriteRenderer>().enabled = true;
        }
        else
        {
            leftHandEmpty.GetComponent<SkinnedMeshRenderer>().enabled = false;
            leftHandWeapon.GetComponent<SpriteRenderer>().enabled = false;
        }

    }
    public void DeactivateBattle(bool active)
    {
        isInactive = active;
    }
    //From Inventory
    public void Looting(bool enable_)
    {
        _animator.SetBool("loot", enable_);

    }

    private void EventInvoking(string name_, BecomeEvent event_)
    {
        if (GetComponent<Battle_Ulf_Enemy>())
            return;

        EventManager.Invoke(name_, event_);
    }

    public void openInventory(BecomeEvent BE)
    {
        inventoryOpened = BE.come;
    }

    // Update is called once per frame
    protected void Update()
    {
        if (isInactive || inventoryOpened)
        {
            return;
        }
        if (!IsDamaged && !Weapon_array[0].Recharging())
        {

            if(Control.control.altFire1.start)//Superattack
            {
                _animator.SetBool("altMain", true);
                EventInvoking("Ulf attack", new BecomeEvent(true, 2, Weapon_array[1].Priority));
                //_animator.SetInteger("weaponMain", numWeaponMain);

            }

            if (Control.control.fire1.press && weaponMain.item.strength > 0 && !_animator.GetBool("attackMain"))
            {
                Inventory.inventory.TryClose();
                Debug.Log("Str main: " + weaponMain.item.strength);
                _animator.SetInteger("custom", Random.Range(0, 4));
                _animator.SetBool("attackMain", true);

                EventInvoking("Ulf attack", new BecomeEvent(true, 1, Weapon_array[1].Priority));

                if (Converter.converter)
                {
                    Converter.converter.SendCorrectDegUlf(GetComponent<Control_Ulf>().deg.new_);
                    Converter.converter.AttackFromBattleUlf(true, true, _animator.GetInteger("custom"));
                }
            }
            else if(!Control.control.fire1.press || weaponMain.item.strength <= 0)
            {
                _animator.SetBool("attackMain", false);

                Inventory.inventory.TryClose();
                //_animator.SetTrigger("attackMain");

                if (Converter.converter)
                {
                    Converter.converter.AttackFromBattleUlf(true, false, _animator.GetInteger("custom"));
                }
            }
        }
        if (!IsDamaged && !Weapon_array[1].Recharging())
        {
            if (Control.control.altFire2.start)//Superattack
            {
                _animator.SetBool("altExtra",true);
                EventInvoking("Ulf attack", new BecomeEvent(false, 2, Weapon_array[1].Priority));
                //_animator.SetInteger("weaponExtra", numWeaponExtra);

            }
            else if(!Control.control.altFire2.press)
                _animator.SetBool("altExtra", false);


            if (Control.control.fire2.press && weaponBack.item.strength > 0 && !_animator.GetBool("attackExtra"))
            {
                Inventory.inventory.TryClose();
                Debug.Log("Str back: " + weaponBack.item.strength);

                _animator.SetInteger("custom", Random.Range(0, 4));
                _animator.SetBool("attackExtra", true);

                //EventInvoking("Ulf attack", new BecomeEvent(false, 1, Weapon_array[1].Priority));

                if (Converter.converter)
                {
                    Converter.converter.SendCorrectDegUlf(GetComponent<Control_Ulf>().deg.new_);

                    Converter.converter.AttackFromBattleUlf(false, true, _animator.GetInteger("custom"));
                }
            }
            else if (!Control.control.fire2.press || weaponBack.item.strength <= 0)
            {
                _animator.SetBool("attackExtra", false);

                Inventory.inventory.TryClose();
                //_animator.SetTrigger("attackExtra");

                if (Converter.converter)
                {
                    Converter.converter.AttackFromBattleUlf(false, false, _animator.GetInteger("custom"));
                }
            }
        }


        
        if(Control.control.dropCollect.start)
        {
            Control.control.dropCollect.ResetStart();//TEST
            bool allEmptyDel = false;
            while (lootingEnemy.Count > 0 && !allEmptyDel)
            {
                allEmptyDel = true;
                foreach (lootObject lo in lootingEnemy)
                {
                    if (lo.IsEmpty())
                    {
                        lootingEnemy.Remove(lo);
                        Destroy(lo.gameObject);
                        Debug.Log("Empty loot");
                        allEmptyDel = false;
                        break;
                    }
                }
            }
            if (lootingEnemy.Count > 0)
                Inventory.inventory.LootOpen(lootingEnemy);

        }

        if (Control.control.gnawing.start)
        {
            _animator.SetTrigger("gnawing");
        }
        if (Control.control.healing.start)
        {
            _animator.SetTrigger("heal");
        }
    }

}