﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class damageText : MonoBehaviour {
    public float flySpeed = 2f, rightFly;
    public int upFrames, downFrames;
    private float temFlySpeed;

    private void OnEnable()
    {
        transform.rotation = Camera.main.transform.rotation;
        //flySpeed = 3f;
        temFlySpeed = flySpeed;
        StartCoroutine(textFlyCoroutine());

	}
	IEnumerator textFlyCoroutine()
    {
        for(int i = 0; i < upFrames; i++)
        {
            yield return null;
            transform.position += transform.up * Time.deltaTime * temFlySpeed;
            temFlySpeed -= .05f;
        }
        //for (int i = 0; i < downFrames; i++)
        int dropCount = 0;
        while(dropCount < downFrames)
        {
            yield return null;
            transform.position -= transform.up * Time.deltaTime * temFlySpeed;
            transform.position += transform.right * Time.deltaTime * temFlySpeed * rightFly * 2;
            temFlySpeed += .05f;
            rightFly *= .9530f;
            dropCount++;
        }
        gameObject.SetActive(false);
    }
}
