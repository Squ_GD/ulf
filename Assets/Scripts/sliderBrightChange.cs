﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class sliderBrightChange : MonoBehaviour
{

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKey(KeyCode.Equals))
        {
            GetComponent<Slider>().value += .005f;
        }
        if (Input.GetKey(KeyCode.Minus))
        {
            GetComponent<Slider>().value -= .005f;
        }
    }
}
