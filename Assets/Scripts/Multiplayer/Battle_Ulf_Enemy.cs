﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Battle_Ulf_Enemy : Battle_Ulf
{

    public void UlfAttacking(bool left_, bool rise_, int customVar_)
    {
        _animator.SetInteger("custom", customVar_);
        //raiseMain = rise_;
        if (left_)
        {
            if(rise_)
            _animator.SetInteger("weaponMain", numWeaponMain);
            else
            _animator.SetTrigger("attackMain");
        }
        else
        {
            if (rise_)
                _animator.SetInteger("weaponExtra", numWeaponExtra);
            else
                _animator.SetTrigger("attackExtra");
        }
    }

    public override void Attacking(int damage, float _fromAngle)
    {
        Debug.Log("Attacking ulf_enemy");
        int tmpHeals = heals;
        heals -= damage;
        if (heals > HealMax)
        {
            Debug.Log("Damage: " + damage);
            damage += heals - HealMax;
            heals -= heals - HealMax;
        }
        else if (heals < 0)
        {
            damage += heals;
            heals = tmpHeals;
        }
        if (myID < 10 || damage == 0)
            instDamageText(damage, _fromAngle);
        else
        {
            for (int i = 0; i < damage; i++)
            {
                instDamageIcon();
            }
        }

        //EventManager.Invoke("TakeDamage", new BecomeEvent(true, myID, damage));
        if (!isAttacked && damage > 0)
            _animator.SetTrigger("damageLow");
        
    }

    public void ThrowAxe(float angle_, bool leftHand_)
    {
        if(leftHand_)
        {
            weaponMain.GetComponent<Weapon_Hybrid>().
            Throw(GetComponent<Control_Ulf>().getPlanetCenter(), GetComponent<Control_Ulf>().SpeedLinear, angle_, GetComponent<Collider2D>(), transform);

        }
        else
            weaponBack.GetComponent<Weapon_Hybrid>().
            Throw(GetComponent<Control_Ulf>().getPlanetCenter(), GetComponent<Control_Ulf>().SpeedLinear, angle_, GetComponent<Collider2D>(), transform);


    }

    new public void Death()
    {
        GetComponent<Movement_basis>().enabled = false;
        CapsuleCollider2D myCollider_ = GetComponent<CapsuleCollider2D>();
        myCollider_.enabled = false;
        SetLoot();
        StopAllCoroutines();
        _animator.SetBool("death", true);
        //_animator.SetBool("move", false);
        Planet currPlanet = GetComponent<Movement_basis>().GetPlanet();
        if (currPlanet)
            currPlanet.RemoveEnemy(myID);
        Debug.Log("Death... ID: " + myID);
        //EventManager.Invoke("Death", new BecomeEvent(true, myID, 0));
        if (controlMove)
            controlMove.SetSortGroup("Planet", myID);
        this.enabled = false;
    }

    public void SetMyID(int newID_)
    {
        myID = newID_;
    }




    new protected void Update()
    {

    }
}
