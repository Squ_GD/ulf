﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WorldCreator : MonoBehaviour {
    private List<CustomisePlanet> planetList;
    private const float minDispersion = 5f, DistBetweenPlanets = 5.3f, LimitOfPlanet = 50;
    private Transform lastBuild;
    private Bridge lastBridge;
    private CustomisePlanet currCustPlanet;
	// Use this for initialization
	void Start () {
        planetList = new List<CustomisePlanet>();
        Planet firstPlanet = findFirstPlanet();
        planetList.Add(new CustomisePlanet(firstPlanet));
        /*for (int i = 0; i < 3; i++)
        {
             int deg = i * 120;
             int side = 1;
             if (deg > 0 && deg < 180)
                  side *= -1;
            newBuild(firstPlanet, deg, side);
        }*/
        StartCoroutine(CreateUniverse(5));
    }

    private IEnumerator CreateUniverse(int cyclesPlanet)
    {
        for(int cy = 0; cy < cyclesPlanet || planetList.Count > LimitOfPlanet; cy++)
        {
            for(int i = 0; i < planetList.Count; i++)
            {
                currCustPlanet = planetList[i];
                newBridge();

                yield return null;
            }
            if (planetList.Count > LimitOfPlanet)
                break;
        }
        StartCoroutine(PopulateUniverse(1));
    }

    private IEnumerator PopulateUniverse(int cyclesBuild)
    {
        //for(int cb = 0; cb < cyclesBuild; cb++)
        {
            for (int i = 0; i < planetList.Count; i++)
            {
                currCustPlanet = planetList[i];
                newBuild();
                yield return null;
            }
        }
    }


    private Planet findFirstPlanet()
    {
        return GameObject.FindGameObjectWithTag("planet").GetComponent<Planet>();
    }

    private void newBuild()
    {
        int deg = Customise.GetInt(0, 360);
        int trying = 0;
        while (currCustPlanet.Load() < 1.3f && trying++ < 125) 
        {
            if(!checkForNewBuild(deg))
            {
                deg = Customise.GetInt(0, 360);
                continue;
            }
            int side = 1;
            if (deg > 0 && deg < 180)
                side *= -1;
            instNewBuild(currCustPlanet.CPlanet, deg, side);
            currCustPlanet.buildsInPlanet.Add(deg);
        }
        Debug.Log("Trying: " + trying + " load: " + currCustPlanet.Load() );
    }

    private void instNewBuild(Planet firstPlanet, int deg, int side)
    {
        string[] buildsList = new string[] {"foxes", "Egger_Ent", "Wispi's" };
        int rndBuild = Customise.GetInt(0, buildsList.Length);
        GameObject go = (GameObject)Instantiate(Resources.Load("Builds\\" + buildsList[rndBuild]));
        lastBuild = go.transform;
        lastBuild.GetComponent<Build>().SetPlanet(firstPlanet);
        float radius = firstPlanet.GetComponent<CircleCollider2D>().radius;
        //planetList[firstPlanet].buildsInPlanet.Add(lastBuild.GetComponent<Build>(), deg);

        Build_rotate(deg, side, firstPlanet);
    }


    private void newBridge()
    {
        if (currCustPlanet.Load() >= .3f)
            return;

        int deg = Customise.GetInt(0, 360);

        if (!checkForNewBridge(deg))
            return;

        int side = 1;
        if (deg > 0 && deg < 180)
            side *= -1;
        Planet Planet = currCustPlanet.CPlanet;
        Bridge newBridge = Planet.Add_bridge(false);
        lastBridge = newBridge;
        Planet.Bridge_rotate(deg, side, false);
        Planet nPlanet =  newBridge.Add_planet("green", Customise.GetInt(0, 5));
        if(checkPlanetContacts(nPlanet))
        {
            //currCustPlanet.bridgesDegs.Add(deg);
            planetList.Add(new CustomisePlanet(nPlanet));
        }
        else
        {
            Planet.RemoveBridge(newBridge);
            Destroy(nPlanet.gameObject);
            Destroy(newBridge.gameObject);
        }
        //planetList.Add(nPlanet, new CustomisePlanet(newBridge, deg));
    }

    private bool checkForNewBuild(int deg)
    {
        float radius = currCustPlanet.radius;
        float verifyDeg = minDispersion * 180f / (Mathf.PI * radius);
            //betweenDegreeMin * 4f / Mathf.Pow(radius, 1.7f);


        List<float> vs = currCustPlanet.CPlanet.GetBridgesPoints();
        //Debug.Log("Bridges: " + vs.Count);
        if (vs != null)
            foreach (int br in vs)
            {
                if (!checkDegrees(br, deg, (int)verifyDeg))
                    return false;
            }



        if (currCustPlanet.buildsInPlanet.Count > 0)
            foreach (int checkBuild in currCustPlanet.buildsInPlanet)
            {
                if (!checkDegrees(checkBuild, deg, (int)(verifyDeg)))
                    return false;
            }
        return true;
    }

    private bool checkDegrees(int one, int twoo, int verifydeg)
    {
        int varianceDeg = Mathf.Abs(one - twoo);
        if (varianceDeg <= verifydeg)
        {
            return false;
        }

        int revertVariance = 360 - varianceDeg;
        if (varianceDeg > 240 && revertVariance <= verifydeg)
        {
            return false;
        
        }

        return true;
    }

    private bool checkForNewBridge(int deg)
    {
        float radius = currCustPlanet.CPlanet.GetComponent<CircleCollider2D>().radius;
        //float verifyDeg = betweenDegreeMin / Mathf.Pow(radius, 1.25f);
        float verifyDeg = minDispersion * 180f / (Mathf.PI * radius);

        List<float> vs = currCustPlanet.CPlanet.GetBridgesPoints();

        if(vs != null && vs.Count < 3)
        foreach (int br in vs)
        {
                if (!checkDegrees(br, deg, (int)verifyDeg))
                    return false;
        }

        return true;
    }

    private bool checkPlanetContacts(Planet nplanet)
    {
        Planet oppositePlanet = lastBridge.GetOppositePlanet(nplanet);
        foreach(CustomisePlanet checkPlanet in planetList)
        {
            if (checkPlanet.CPlanet == oppositePlanet)
                continue;

            Transform checkedPlanet = checkPlanet.CPlanet.transform;

            float radiusChecked = checkedPlanet.GetComponent<CircleCollider2D>().radius;
            float radiusNew = nplanet.GetComponent<CircleCollider2D>().radius;
            float totalCheckDist = radiusChecked + radiusNew + DistBetweenPlanets;
            if ((checkedPlanet.position - nplanet.transform.position).magnitude < totalCheckDist)
                return false;
        }
        return true;
    }

    public void Build_rotate(float deg, int degAbs, Planet _planet)
    {
        float heightCollider = lastBuild.GetComponent<BoxCollider2D>().size.y * lastBuild.localScale.y;
        //Расчитываем точку на окружности
        float radiusRotation = _planet.GetRadius() + heightCollider / 2;
        float rad = deg / 180 * Mathf.PI;
        float x = radiusRotation * Mathf.Sin(rad) + _planet.transform.position.x;
        float y = radiusRotation * Mathf.Cos(rad) + _planet.transform.position.y;

        lastBuild.position = new Vector2(x, y);

        //Вычисления угла поворота на планету
        Vector3 vectorToPlanet = (_planet.transform.position - lastBuild.position);
        Vector3 bridgeRotateVector = -lastBuild.up;

        float angleToPlanet = Vector3.Angle(bridgeRotateVector, vectorToPlanet);

        lastBuild.Rotate(Vector3.forward, degAbs * angleToPlanet);
    }
}
struct CustomisePlanet
{
    public Planet CPlanet;
    public float radius;
   // public List<int> bridgesDegs;
    public List<int> buildsInPlanet;
    public CustomisePlanet(Planet pl)
    {
        CPlanet = pl;
        radius = pl.GetComponent<CircleCollider2D>().radius;
        // bridgesDegs = new List<int>();
        buildsInPlanet = new List<int>();
    }

    public float Load()
    {
        if (CPlanet.GetBridgesPoints() == null)
            return 0;
        float load = buildsInPlanet.Count * 2f;
        load += CPlanet.GetBridgesPoints().Count;
        load /= Mathf.Pow(radius, 1.05f);
        return load;
    }

    void Update()
    {

        if (Input.GetKeyDown(KeyCode.End))
            SceneManager.LoadScene(0);
            
        
    }
}