﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Quiver_Weapon : Weapon_Basis
{
    // Start is called before the first frame update
    //void Start()
    //{
        
    //}
    public override void Attack(bool usedArrow)
    {

        if (usedArrow)
            strengthChange(-1);
        else
            strengthChange(1);
    }
    public override void Attack()
    {
        strengthChange(-1);

    }

    // Update is called once per frame
    //void Update()
    //{

    //}
}
