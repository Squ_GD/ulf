﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class inventoryChild : MonoBehaviour
{
    public Transform cellsTransform, skTransform;
    private ItemCell[,] itemCells = new ItemCell[1, 3];
    private Transform[,] skinTransforms;
    private lootObject lootList;
    private void Awake()
    {
        

        EventManager.AddListener("Inventory", closeInventory);
        skinTransforms = new Transform[skTransform.childCount, 10];//Если 10 - максимум на планете
        for (int j = 0; j < skTransform.childCount; j++)
            for (int i = 0; i < skTransform.GetChild(j).childCount; i++)
            {
                skinTransforms[j, i] = skTransform.GetChild(j).GetChild(i);
            }
        
    }
    private void Start()
    {
        if (!Inventory.inventory)
            Debug.LogError("NOT");
        //Inventory.inventory.SetItemCells(itemCells, cellsTransform, ItemCell.typeCell.loot);

        foreach (ItemCell cell in itemCells)
        {
            cell.activateCell(false);
        }
        gameObject.SetActive(false);
        //Inventory.inventory.ConnectNewCell(itemCells[0, 0], true);
    }
    public Transform getTransforms()
    {
        return cellsTransform;
    }

    public ItemCell[,] getCells()
    {
        return itemCells;
    }
    private void closeInventory(BecomeEvent BE)
    {
        if (!BE.come && BE.id == 1)
            closeLoot();        
    }
    private void closeLoot()
    {
        foreach(ItemCell cell in itemCells)
        {
            cell.SetItemNull();
            cell.activateCell(false);
        }
        gameObject.SetActive(false);
    }
    /*public void CollectAllDrop()
    {
        Inventory inventory_ = Inventory.inventory;
        if (lootList.lootItem1 != null && inventory_.AddItem(lootList.lootItem1))
            lootList.lootItem1 = null;
        if (lootList.lootItem2 != null && inventory_.AddItem(lootList.lootItem2))
            lootList.lootItem2 = null;
        if (lootList.lootItem3 != null && inventory_.AddItem(lootList.lootItem3))
            lootList.lootItem3 = null;

        if (lootList.IsEmpty())
            Destroy(lootList.gameObject);

        gameObject.SetActive(false);
    }*/
    public void SetLoot(lootObject loot_)
    {
        lootList = loot_;
        if (loot_.lootItem1 != null)
        {
            Inventory.inventory.MoveItem(itemCells[0, 0], loot_.lootItem1);
            itemCells[0, 0].onCollect += DropCollected;
            itemCells[0, 0].activateCell(true);
            Debug.Log("loot_.lootItem1: " + loot_.lootItem1.name);
            Debug.Log("loot_.lootCount: " + ((CraftItem)loot_.lootItem1).stuck);
        }
        if (loot_.lootItem2 != null)
        {
            Inventory.inventory.MoveItem(itemCells[0, 1], loot_.lootItem2);
            itemCells[0, 1].onCollect += DropCollected;
            itemCells[0, 1].activateCell(true);

            Debug.Log("loot_.lootItem2: " + loot_.lootItem2.name);
            Debug.Log("loot_.lootCount: " + ((CraftItem)loot_.lootItem2).stuck);
        }
        if (loot_.lootItem3 != null)
        {
            Inventory.inventory.MoveItem(itemCells[0, 2], loot_.lootItem3);
            itemCells[0, 2].onCollect += DropCollected;
            itemCells[0, 2].activateCell(true);

            Debug.Log("loot_.lootItem3: " + loot_.lootItem3.name);
            Debug.Log("loot_.lootCount3: " + ((CraftItem)loot_.lootItem3).stuck);
        }

        for (int j = 0; j < skTransform.childCount; j++)
            for (int i = 0; i < skTransform.GetChild(j).childCount; i++)
            {
                skinTransforms[j, i].gameObject.SetActive(false);
            }

        skinTransforms[loot_.planetType, loot_.EnemyType].gameObject.SetActive(true);

    }
    private void DropCollected()
    {

        if (itemCells[0, 0].item == null)
        {
            lootList.lootItem1 = null;
        }
        if(itemCells[0, 1].item == null)
        {
            lootList.lootItem2 = null;
        }
        if(itemCells[0, 2].item == null)
        {
            lootList.lootItem3 = null;
        }


        if ( lootList.IsEmpty())// itemCells[0,0].item == null && itemCells[0, 1].item == null && itemCells[0, 2].item == null)
        {
            if(lootList)
            Destroy(lootList.gameObject);
            foreach(ItemCell cell in itemCells)
            {
                cell.disableCell();
                cell.onCollect -= DropCollected;
            }
            gameObject.SetActive(false);
        }
    }
    private void Update()
    {
        /*if(Input.GetKeyDown(KeyCode.Space))
        {
            CollectAllDrop();
        }
        */
    }
}


