﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Control : MonoBehaviour
{
    public static Control control;

    public ControlState fire1, fire2, altFire1, altFire2, craft, moveRight, moveLeft, moveUp, moveDown,
        dropCollect, healing, gnawing;
    private void OnEnable()
    {
        control = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        fire1 = new ControlState();
        fire2 = new ControlState();
        altFire1 = new ControlState();
        altFire2 = new ControlState();
        craft = new ControlState();
        moveRight = new ControlState();
        moveLeft = new ControlState();
        moveUp = new ControlState();
        moveDown = new ControlState();
        dropCollect = new ControlState();
        healing = new ControlState();
        gnawing = new ControlState();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetAxis("Fire1") > 0)
        {
            fire1.Pressed(true);
        }
        else
            fire1.Pressed(false);

        if (Input.GetAxis("Fire2") > 0)
        {
            fire2.Pressed(true);
        }
        else
            fire2.Pressed(false);

        if (Input.GetAxis("ALT Fire1") > 0)
        {
            altFire1.Pressed(true);
        }
        else
            altFire1.Pressed(false);

        if (Input.GetAxis("ALT Fire2") > 0)
        {
            altFire2.Pressed(true);
        }
        else
            altFire2.Pressed(false);

        if (Input.GetAxis("Horizontal") > 0)
        {
            moveLeft.Pressed(false);
            moveRight.Pressed(true);
        }
        else if (Input.GetAxis("Horizontal") < 0)
        {
            moveRight.Pressed(false);
            moveLeft.Pressed(true);
        }
        else
        {
            moveLeft.Pressed(false);
            moveRight.Pressed(false);
        }

        if (Input.GetAxis("Vertical") > 0)
        {
            moveUp.Pressed(true);
        }
        else
            moveUp.Pressed(false);
        if (Input.GetAxis("Vertical") < 0)
        {
            moveDown.Pressed(true);
        }
        else
            moveDown.Pressed(false);

        if (Input.GetAxis("Healing") > 0)
        {
            healing.Pressed(true);
        }
        else
            healing.Pressed(false);

        if (Input.GetAxis("Gnawing") > 0)
        {
            gnawing.Pressed(true);
        }
        else
            gnawing.Pressed(false);
        if(Input.GetAxis("CollectDrop") > 0)
        {
            dropCollect.Pressed(true);
        }
        else
            dropCollect.Pressed(false);
    }
}
public struct ControlState
{
    public bool start { get; private set; }//Single press
    public bool press { get; private set; }

    public void Pressed(bool isTrue_)
    {
        if (press)
        {
            start = false;
        }
        else if (isTrue_)
        {
            //Debug.Log("Press start");

            start = true;
        }
        press = isTrue_;
    }
    public void ResetStart()
    {
        start = false;
    }
}
