﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class Control_enemy : Movement_basis {
    public float PatrolDistance;

   // private float speed_temp;
    private int newDir;
    private Vector3 startPosition;
    private Animator animator;
    private Battle_enemy battleComponent;
    private bool inArea, waitForMove;
    public Build build;
    private SortingGroup sortingGroup;
    private int countStay;

    private void Awake()
    {
        sortingGroup = GetComponent<SortingGroup>();

        if(!sortingGroup)
            sortingGroup = gameObject.AddComponent<SortingGroup>();

        sortingGroup.enabled = false;
        battleComponent = GetComponent<Battle_enemy>();

    }

    void Start () {
        inArea = false;
        newDir = lastDir = -1;

        animator = GetComponent<Animator>();
        StartCoroutine(Deciding());
       // speed_temp = SpeedLinear;
        if (planet)
        {
            planet.Get_Object_degree(this);
        }
        if (startPosition == Vector3.zero)
        {
            startPosition = transform.position;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "planet")
        {

            if (!planet)
            {
                Planet newPlanet = collision.gameObject.GetComponent<Planet>();
                //set_Planet(planet);
                SetPlanet(newPlanet, false);
                planet.Get_Object_degree(this);
                startPosition = transform.position;
                //Добавляем на планету в список трансформов
                //battleComponent.AddToPlanet(planet);
                Move(newDir);
            }
        }
    }
    public Vector3 GetStartPos()
    {
        return startPosition;
    }
    public void SetStartPos(Vector3 startPos)
    {
        startPosition = startPos;
    }

    public override void SetSortGroup(string srtLayer, int srtOrder)
    {
        sortingGroup.sortingOrder = srtOrder;
        sortingGroup.sortingLayerName = srtLayer;
        sortingGroup.enabled = true;
    }

    public void EnterBuild(float scaleFactor, Build _build, Planet _planet)
    {
        SetPlanet(_planet, false);
        planet.Get_Object_degree(this);
        SetSortGroup("Builds", 5);
        build = _build;
        if(scaleFactor > 0)
            transform.localScale *= scaleFactor;
        GetComponent<CapsuleCollider2D>().isTrigger = true;
    }

    public void ExitBuild(float delayExit)
    {
        if (!build)
            return;
        float scaleFact = build.Exit(this);
        StartCoroutine(delayDoorExit(delayExit, scaleFact));
        
       // animator.speed = .1f;
    }
    private IEnumerator delayDoorExit(float delayExit, float scaleFact)
    {
        yield return new WaitForSeconds(delayExit);
        build = null;
        if(scaleFact > 0)
            transform.localScale /= scaleFact;
        sortingGroup.enabled = false;
        GetComponent<CapsuleCollider2D>().isTrigger = false;

    }


    public bool InBuild()
    {
        if (build)
            return true;
        else
            return false;
    }
    // Update is called once per frame
    void Update () {
        if (planet && !waitForMove && battleComponent.CanMove())
        {

            planet.Get_Object_degree(this);
            if (build)
            {
                if (build.Walk(transform, SpeedLinear * transform.localScale.x * .3f, newDir))
                {
                    animator.SetBool("move", true);
                }
                else
                {
                    animator.SetBool("move", false);
                }
            }
            else
            {
                Move(newDir);
                animator.SetBool("move", true);
            }

            lastDir = newDir;

            //if (!animator.GetBool("run")) 
            //{
                  //SpeedLinear = speed_temp;
                //  animator.SetBool("move", true);
            //}
        }
        else
        {

            animator.SetBool("move", false);
            animator.SetBool("run", false);
            if(!build && ++countStay > 5)
            {
                planet.Get_Object_degree(this);
                Move(0);
                countStay = 0;
            }
            //SpeedLinear = speed_temp;

        }
	}

    public void SetPlanet(Planet plan, bool startMove)
    {
        if (!planet)
            battleComponent.AddToPlanet(plan);

        planet = plan;
        set_Planet(plan);
        if (startMove)
        {
            plan.Get_Object_degree(this);
            Move(lastDir);
            newDir = lastDir;
        }
    }



    IEnumerator Deciding()
    {
        float time = .1f;
        while(true)
        {
            yield return new WaitForSeconds(time);

            //if (battleComponent.GetStatus() == Battle_enemy.Status_Attack.agressive)
              //  continue;

            float startDeg = planet.Get_Object_degree(startPosition);
            float totalDeg;
            float simDeg1 = Mathf.Abs(startDeg - currDegree);
            float simDeg2 = Mathf.Abs(startDeg - currDegree - 360f);
            float simDeg3 = Mathf.Abs(startDeg - currDegree + 360f);
            totalDeg = simDeg1 < simDeg2 ? simDeg1 : simDeg2;
            totalDeg = totalDeg < simDeg3 ? totalDeg : simDeg3;

            float currPatrol = planet.Circumference(totalDeg);
            if (InBuild())
                currPatrol *= 4.5f;

            if (currPatrol > PatrolDistance && battleComponent.GetUsedWeapon() == -1)
            {
                
                if (!planet.GetReturnDir(transform, startPosition))
                    newDir = lastDir * -1;
                //time *= 2f;
                inArea = false;
                battleComponent.ChangeStatus(Battle_enemy.Status_Attack.runAway);
            }
            else
            {
                inArea = true;

                if (battleComponent.GetStatus() == Battle_enemy.Status_Attack.passive)
                {
                    bool changeDir = Random.Range(0, 10) < 4 ? true : false;
                    if (changeDir)
                    {
                        newDir = Random.Range(-1, 2);

                        if (newDir == 0)
                        {
                            newDir = lastDir;
                            waitForMove = true;
                        }
                        else
                            waitForMove = false;
                    }
                   // SpeedLinear = speed_temp;
                }
                else if(battleComponent.GetStatus() == Battle_enemy.Status_Attack.runAway)
                {
                    bool changeDir = Random.Range(0, 10) < 6 ? true : false;
                    if (changeDir)
                        battleComponent.ChangeStatus(Battle_enemy.Status_Attack.passive);

                }
            }

            time = Random.Range(.5f, 2.2f);
        }
    }

    public void SetRunFromAnim(float multiplier)
    {
        SpeedLinear *= multiplier;
    }

    public void Death()
    {
        StopAllCoroutines();
    }
    

    public bool GetStatusLookUlf()
    {
        bool  DirToUlf = planet.Get_Ulf_Dir(transform);


        if (DirToUlf)
            return true;
        else
            return false;
    }

    public float GetDistToUlf()
    {
        float ulf_deg = planet.Get_Ulf_Deg(transform);
        float distanceToUlf;
        if (ulf_deg < 0)
        {
            distanceToUlf = -1f;
            //newDir = Random.Range(-1, 2);
        }
        //Если Ульф на планете
        else
        {
            float simDeg1 = Mathf.Abs(ulf_deg - currDegree);
            float simDeg2 = Mathf.Abs(ulf_deg - currDegree - 360f);
            float simDeg3 = Mathf.Abs(ulf_deg - currDegree + 360f);
            float totalDeg = simDeg1 < simDeg2 ? simDeg1 : simDeg2;

            totalDeg = totalDeg < simDeg3 ? totalDeg : simDeg3;
            distanceToUlf = planet.Circumference(totalDeg);
        }

        return distanceToUlf;
    }
    //Weapon Target
    public void rotateToUlf()
    {
        planet.Get_Object_degree(this);
        bool DirToUlf = planet.Get_Ulf_Dir(transform);
        if (!DirToUlf)
        {
            newDir  *= -1;
        }
        Move(newDir);
        lastDir = newDir;

    }
    public void MoveOver(float speed_start, float speed_end)
    {
        SpeedLinear = speed_start;

        Move(lastDir);
        planet.Get_Object_degree(this);
        SpeedLinear = speed_end;
    }

    public void MoveOver()
    {
        Move(lastDir);
    }

    public void GoToUlf(bool go)
    {
        if (go)
        {
            waitForMove = false;
            animator.SetBool("run", battleComponent.CanMove());
            bool DirToUlf = planet.Get_Ulf_Dir(transform);
           // Debug.Log("DirToUlf: " + DirToUlf);
            if (!DirToUlf)
            {

               newDir = lastDir * -1;
            }
        }
        else
            animator.SetBool("run", false);
    }
    public void WaitForMove()
    {
        waitForMove = true;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            //SpeedLinear = speed_temp / 20f;
        }
        else if(collision.gameObject.tag == "enemy")
        {
            Physics2D.IgnoreCollision(GetComponent<CapsuleCollider2D>(), collision.collider);
        }
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
           // SpeedLinear = speed_temp;
        }
    }
}
