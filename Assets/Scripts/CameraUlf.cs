﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraUlf : MonoBehaviour {
    protected Transform target;
    private Camera cam;
    private float colliderUlfHeight, speed, tempSpeed;
    private Vector3 rightPlusVect;
    public float rigthPlusCorrect;
    public static CameraUlf cameraUlf;
    public bool forTesting;

    private void Awake()
    {
        cameraUlf = this;
    }

    private void OnEnable()
    {
       // transform.parent = null;
    }
    void Start () {
        speed = tempSpeed = 25f;
        cam = GetComponent<Camera>();
        //camTextLabel.text = "cam size: " + cam.orthographicSize;

        Vector3 startPos = transform.position;
        Quaternion startRotate = transform.rotation;
        //ulfSpeed = target.GetComponent<Control_Ulf>().SpeedLinear;
        if (!Control_Ulf.ulf_static)
            return;
        //target = transform.parent;
        if (!target)
        {
            target = Control_Ulf.ulf_static.transform;

        }
        colliderUlfHeight = Control_Ulf.ulf_static.get_Height();
        transform.SetParent(null);
        transform.position = startPos;
        transform.rotation = startRotate;
    }


    public void SetTarget(Transform _target)
    {
        target = _target;
    }

    public Transform GetTarget()
    {
        if (!target)
        {

            target = Control_Ulf.ulf_static.transform;
        }

        return target;
    }

    // Update is called once per frame
    private void LateUpdate()
    {
        int moveTestCamHor = 0;
        if (Input.GetKey(KeyCode.LeftArrow)) moveTestCamHor = -1;
        else if (Input.GetKey(KeyCode.RightArrow)) moveTestCamHor = 1;

        int moveTestCamVer = 0;
        if (Input.GetKey(KeyCode.DownArrow)) moveTestCamVer = -1;
        else if (Input.GetKey(KeyCode.UpArrow)) moveTestCamVer = 1;

        if (forTesting && (moveTestCamHor != 0 || moveTestCamVer != 0))
        {
            transform.position += transform.right * moveTestCamHor * Time.deltaTime;
            transform.position += transform.up * moveTestCamVer * Time.deltaTime;
            return;
        }

        float mw = Input.GetAxis("Mouse ScrollWheel");
        if (mw > 0)
        {
            Camera.main.orthographicSize -= .3f;
            transform.position += transform.forward * 3f;
        }
        else if (mw < 0)
        {
            Camera.main.orthographicSize += .3f;
            transform.position -= transform.forward * 3f;
        }

        if (!target)
            return;

        if (Vector3.Angle(rightPlusVect, target.right) > 100f)
            speed *= .1f;

        Vector3 tempRight = rightPlusVect;
        rightPlusVect = target.right * rigthPlusCorrect;

        if (speed < tempSpeed)
        {
            speed += .03f;
            if (tempRight != rightPlusVect)
            {
                speed += .07f;

            }
        }

        float delta = Time.fixedDeltaTime;

        Vector3 downPlusVect = target.up * colliderUlfHeight;
        Vector3 ulfPos = new Vector3(target.position.x, target.position.y, transform.position.z) - rightPlusVect - downPlusVect;
        transform.position += (ulfPos - transform.position) * delta * speed;

        float angle = Vector3.Angle(transform.right, -target.right) * delta;
        transform.rotation.SetLookRotation(target.up, transform.right);
        transform.LookAt(transform.position + transform.forward, (target.up + transform.up) * delta);


    }
}