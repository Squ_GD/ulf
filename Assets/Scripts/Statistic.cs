﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Statistic {
    public static int UnitsCount = 0;
    private static Dictionary<int,Battle_enemy> unitList = new Dictionary<int, Battle_enemy>();

    public static Battle_enemy GetUnit(int id)
    {
        return unitList[id];
    }

    public static int GetID(Battle_enemy BE)
    {
        unitList.Add(++UnitsCount,BE);
        return UnitsCount;
    }
    public static int GetID(Battle_Ulf BU)
    {
       // Debug.Log("ULF ID = 0");
        return 0;
    }
}
