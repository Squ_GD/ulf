﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class Blacksmith_behaviour : behavior {
    private int myID;


    private void OnEnable()
    {
        EventManager.AddListener("TakeDamage", damageBS);
    }
    private void Start()
    {
        Control = GetComponent<Control_enemy>();
        Battle = GetComponent<Battle_enemy>();
        _animator = GetComponent<Animator>();
        myID = Battle.GetMyID();
        GetComponent<SortingGroup>().enabled = true;
    }
    private void damageBS(BecomeEvent BE)
    {
        if (BE.id == myID)
        {
            Battle.LookDistance = 3;
            GetComponent<CapsuleCollider2D>().isTrigger = false;
        }
    }
}
