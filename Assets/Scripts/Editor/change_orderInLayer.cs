﻿using UnityEditor;

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[CustomEditor(typeof())]
[CanEditMultipleObjects]

public class change_orderInLayer : Editor
{
    int layerChange = -10;
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        //Impact_Basis change_object = (Impact_Basis)target;

        

        if (GUILayout.Button("Change"))
        {
           // changeFunc(change_object.gameObject);
        }
    }

    private void changeFunc(GameObject GO)
    {
        SpriteRenderer SR = GO.GetComponent<SpriteRenderer>();
        if(SR)
        {
            Debug.Log("SR.sortingOrder: " + SR.sortingOrder);
            SR.sortingOrder += layerChange;
        }
        if(GO.transform.childCount > 0)
        for(int c = 0; c < GO.transform.childCount; c++)
        {
                //Рекурсия
                changeFunc(GO.transform.GetChild(c).gameObject);
        }
    }
}
