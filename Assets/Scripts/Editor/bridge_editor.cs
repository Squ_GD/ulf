﻿using UnityEditor;
using UnityEngine;
using System.Collections;

[CustomEditor(typeof(Bridge))]
[CanEditMultipleObjects]

public class bridge_editor : Editor {
    string[] planetArray;
    int planet_type;

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        planetArray = new string[] {
         "1 || 1",
         "1 || 2",
         "1 || 3",
         "1 || 4",
         "1 || 5",
         "2 || 1",
         "2 || 2",
         "2 || 3",
         "2 || 4",
         "2 || 5",
         "3 || 1",
         "3 || 2",
         "3 || 3",
         "3 || 4",
         "3 || 5",
        };

        Bridge bridge = (Bridge)target;
        GUILayout.Label("Тип планеты:");
        planet_type = GUILayout.Toolbar(planet_type, planetArray);


        if (GUILayout.Button("Добавить планету"))
        {
            int PlTp = planet_type / 5;
            string PT = "";
            Debug.Log("PlTp: " + PlTp);
            switch (PlTp)
            {
                case 0:
                    PT = "green";
                    break;
                case 1:
                    PT = "death";
                    break;
                case 2:
                    PT = "snow";
                    break;
            }
            int PN = planet_type - PlTp * 5;
            bridge.Add_planet(PT, PN);
            EditorUtility.SetDirty(bridge);

        }
    }
}
