﻿using UnityEditor;

using System.Collections;
using System.Collections.Generic;
using UnityEngine;



//[CustomEditor(typeof())]
//[CanEditMultipleObjects]
[CustomEditor(typeof(ShaderChange))]
public class change_shader : Editor
{
    
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        ShaderChange change_object = (ShaderChange)target;

        

        if (GUILayout.Button("Change"))
        {
            change_object.changeFunc(change_object.go_for_change);
        }
    }

    
}
