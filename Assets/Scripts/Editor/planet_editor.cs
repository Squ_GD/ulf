﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Planet))]
[CanEditMultipleObjects]

public class planet_editor : Editor {
    private float bridge_deg = 0, build_deg = 0;
    private const int maxbridge = 3;
    private const float stdRadius = 3.55f;
    private bool mirrorIs = false;
    private int selectBuild = 0;
    private Transform lastBuild;
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        
        Planet planet = (Planet)target;
        //GUILayout.BeginArea(new Rect(Vector2.zero, new Vector2(100, 100)));
        //Кнопка создать мост или планету(если это мост)
        if (GUILayout.Button("Добавить мост"))
        {
            bridge_deg = 0;
            mirrorIs = false;
            planet.Add_bridge(mirrorIs);
            EditorUtility.SetDirty(planet);
        }

        GUILayout.Label("Вращение моста(вокруг планеты): " + bridge_deg);
        float newBridgeDeg = GUILayout.HorizontalSlider(bridge_deg, 0, 360);

        if (newBridgeDeg != bridge_deg)
        {
            int sideRotate = 1;
            if (bridge_deg < newBridgeDeg)
                sideRotate *= -1;

            if (mirrorIs)
                sideRotate *= -1;

            bridge_deg = newBridgeDeg;

            planet.Bridge_rotate(newBridgeDeg, sideRotate, mirrorIs);

        }

        if (GUILayout.Toggle(mirrorIs, "Зеркально") != mirrorIs)
        {
            mirrorIs = !mirrorIs;

            planet.Bridge_mirror(mirrorIs);
        }
        GUILayout.Space(20);
        string[] buildsList = new string[] {"", "foxes", "Egger_Ent", "Wispi's", "Inj", "Awalro" };
        int tempSelect = GUILayout.SelectionGrid(selectBuild, buildsList, 1);
        if (tempSelect != selectBuild)
        {

            GameObject go = (GameObject) Instantiate(Resources.Load("Builds\\"+buildsList[tempSelect]));
            lastBuild = go.transform;
            EditorUtility.SetDirty(lastBuild.GetComponent<Build>());
            lastBuild.GetComponent<Build>().SetPlanet(planet);
            float radius = planet.GetComponent<CircleCollider2D>().radius;
            Transform upTrans = lastBuild.Find("up");
            if (radius > stdRadius && upTrans)
            {
                float radDiffer = radius - stdRadius;
                float upReg = lastBuild.GetComponent<Build>().percUpCompReg;
                upTrans.localPosition -= upTrans.up * radDiffer * upReg;
            }
            Build_rotate(0, 1, planet);

        }

        float newBuildDeg = GUILayout.HorizontalSlider(build_deg, 0, 360);
        if(newBuildDeg != build_deg)
        {
            int sideRotate = 1;
            if (build_deg < newBuildDeg)
                sideRotate *= -1;

            //if (mirrorIs)
            //  sideRotate *= -1;

            build_deg = newBuildDeg;

            Build_rotate(build_deg, sideRotate, planet);
        }
    }

    public void Build_rotate(float deg, int degAbs, Planet _planet)
    {
        float heightCollider = lastBuild.GetComponent<BoxCollider2D>().size.y * lastBuild.localScale.y;
        //Расчитываем точку на окружности
        float radiusRotation = _planet.GetRadius() + heightCollider/2;
        float rad = deg / 180 * Mathf.PI;
        float x = radiusRotation * Mathf.Sin(rad) + _planet.transform.position.x;
        float y = radiusRotation * Mathf.Cos(rad) + _planet.transform.position.y;

        lastBuild.position = new Vector2(x, y);

        //Вычисления угла поворота на планету
        Vector3 vectorToPlanet = (_planet.transform.position - lastBuild.position);
        Vector3 bridgeRotateVector = -lastBuild.up;
        /*if (mirrored)
        {
            bridgeRotateVector *= -1;

            degAbs *= -1;
        }*/

        float angleToPlanet = Vector3.Angle(bridgeRotateVector, vectorToPlanet);

        lastBuild.Rotate(Vector3.forward, degAbs * angleToPlanet);
    }
}
