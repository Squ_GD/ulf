﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bridge : MonoBehaviour {
    public Planet startPlanet, finishPlanet;

    private Vector3 centr;
    private float widthBridge;

    private float colliderWidth, angleToStart, angleToFinish;
    private const float angleBetweenPlanets = 150f;

    public GameObject Light1, Light2;

    public bool startLeft, finishLeft;
    private bool PlaySoundStartPlanet;
	// Use this for initialization
	void Start () {
        centr = Get_Center();
        getCentralAngles();
        widthBridge = GetComponent<BoxCollider2D>().size.x * transform.localScale.x;
    }
	//Расчёт угла, который должен пройти Ульф по мосту
    private void getCentralAngles()
    {
        colliderWidth = GetComponent<BoxCollider2D>().size.x * transform.localScale.x;
        float diametr = GetComponent<CircleCollider2D>().radius * 2;
        float alpha = Mathf.Asin(colliderWidth / diametr);

        Vector3 toStartPlanet = startPlanet.transform.position - centr;
        Vector3 toFinishPlanet = finishPlanet.transform.position - centr;
        Vector3 upVector = new Vector3(centr.x, centr.y + 1f);
        upVector -= centr;
        angleToStart = Vector3.Angle(upVector, toStartPlanet);
        if (startPlanet.transform.position.x < centr.x)
            angleToStart = 360f - angleToStart;

        angleToFinish = Vector3.Angle(upVector, toFinishPlanet);
        if(finishPlanet.transform.position.x < centr.x)
            angleToFinish = 360f - angleToFinish;

        if (transform.InverseTransformPoint(startPlanet.transform.position).x < 0)
            startLeft = true;
        if (transform.InverseTransformPoint(finishPlanet.transform.position).x < 0)
            finishLeft = true;
    }

    public void PlaySoundPlanet(Planet opposite_)
    {
        if(opposite_ == finishPlanet)
            startPlanet.SoundON();
        else
            finishPlanet.SoundON();
    }
    public void StopPlaySoundOppositePlanet(Planet planet_)
    {
        if (planet_ == startPlanet)
            finishPlanet.SoundOFF();
        else
            startPlanet.SoundOFF();
    }

    public bool PositionToDown(Control_Ulf ulf)
    {
        float distToStart = (startPlanet.transform.position - ulf.transform.position).magnitude ;
        float distToFinish = (finishPlanet.transform.position - ulf.transform.position).magnitude ;
        distToStart -= (startPlanet.GetRadius());
        distToFinish -= (finishPlanet.GetRadius());
        //Debug.Log("distToStart: " + distToStart + " distToFinish: " + distToFinish);
        /*if(distToFinish < distToStart)//For Sound
        {
            if(PlaySoundStartPlanet)
            {
                //finishPlanet.SoundON();
                //startPlanet.SoundOFF(); 
                PlaySoundStartPlanet = false;
            }
            finishPlanet.SetDistToUlfSound(distToFinish);
            
        }
        else
        {
            if (!PlaySoundStartPlanet)
            {
                //startPlanet.SoundON();
                //finishPlanet.SoundOFF();
                PlaySoundStartPlanet = true;
            }
            startPlanet.SetDistToUlfSound(distToStart);
        }*/
        //Debug.Log("Start");
        startPlanet.SetDistToUlfSound(distToStart, widthBridge);
        //Debug.Log("Finish");
        finishPlanet.SetDistToUlfSound(distToFinish, widthBridge);

        bool leaveStart = distToStart <=  1f;
        bool leaveFinish = distToFinish <= 1f;



        return leaveFinish | leaveStart;
    }

    //Запрос угла для Ulf'a, с записью в переменную
    public void Get_Object_degree(Control_Ulf ulf)
    {
        Vector3 vecToObject = (ulf.transform.position - centr).normalized;
        Vector3 upVector = new Vector3(centr.x, centr.y + 1f);
        upVector -= centr;
        ulf.deg.old_ = ulf.deg.new_;

        ulf.deg.new_ = Vector3.Angle(upVector, vecToObject);

        if (ulf.transform.position.x < centr.x)
            ulf.deg.new_ = 360f - ulf.deg.new_;

        bool leaveStart = false;
        bool leaveFinish = false;
        float ulf_step = 1f;
        if(ulf.deg.old_ != -1)
            Mathf.Abs(ulf.deg.new_ - ulf.deg.old_);
        float degBetstart = Mathf.Abs(angleToStart - ulf.deg.new_);

        if (degBetstart <= ulf_step || degBetstart >= 360f - ulf_step)
        {
            //Debug.Log("angleToStart: " + angleToStart + " ulf.deg.new_: " + ulf.deg.new_);
            leaveStart = true;
            leaveStart &= ulf.lastDir == -1 && startLeft || ulf.lastDir == 1 && !startLeft;

        }

        float degBetfinish = Mathf.Abs(angleToFinish - ulf.deg.new_);
        if (degBetfinish <= ulf_step || degBetfinish >= 360f - ulf_step)
        {


            leaveFinish = true;

            leaveFinish &= ulf.lastDir == -1 && finishLeft || ulf.lastDir == 1 && !finishLeft;
            
        }
        
        if (leaveStart)
        {
            Debug.Log("LeaveStart");
            ulf.Set_New_Planet(startPlanet);
            ulf.TempBridgeNull();
            //ulf.deg.old_ = ulf.deg.new_ = -1;
            StopPlaySoundOppositePlanet(startPlanet);
        }
        else if (leaveFinish)
        {
            Debug.Log("LeaveFinish");
            ulf.Set_New_Planet(finishPlanet);
            ulf.TempBridgeNull();
            //ulf.deg.old_ = ulf.deg.new_ = -1;
            StopPlaySoundOppositePlanet(finishPlanet);
        }
        else
        {
            ulf.SetDegree(ulf.deg.new_);
        }
    }


    private IEnumerator Light1Flashing()
    {
        while(true)
        {
            yield return new WaitForSeconds(Random.Range(.5f, 3f));
            Light1.SetActive(!Light1.activeSelf);

        }
    }
    private IEnumerator Light2Flashing()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(.5f, 3f));
            Light2.SetActive(!Light2.activeSelf);

        }
    }


    public Planet GetOppositePlanet(Planet requester)
    {
        if (startPlanet == requester)
        {
            return finishPlanet;
        }
        else
            return startPlanet;
    }

    public Vector3 Get_Center()
    {
        float offsetY = GetComponent<CircleCollider2D>().offset.y;

        centr = transform.position + transform.up * offsetY;
        return centr;
    }

    public void SetStartPlanet(Planet planet)
    {
        startPlanet = planet;
        //Сохраняет изменения, сделанные в режиме Editor'a
       // UnityEditor.EditorUtility.SetDirty(this);

    }
    //Editor
    public Planet Add_planet(string PT, int PN)
    {
        Debug.Log("Type: " + PT + " num: " + PN);
        GameObject planetNew = (GameObject)Instantiate(Resources.Load("Planets\\"+ PT+"\\planet_" + PN));
        colliderWidth = GetComponent<BoxCollider2D>().size.x * transform.localScale.x;
        if (PN >= 3)
            colliderWidth += .05f;
        finishPlanet = planetNew.GetComponent<Planet>();
        float startRadius = startPlanet.GetComponent<CircleCollider2D>().radius;
        float finishRadius = planetNew.GetComponent<CircleCollider2D>().radius;
        float bridgeWidth = GetComponent<BoxCollider2D>().size.x * transform.localScale.x;
        planetNew.transform.position = startPlanet.transform.position;
        Vector3 vectorToBridge = (transform.position - planetNew.transform.position).normalized;

        planetNew.transform.position += vectorToBridge * (colliderWidth + finishRadius  + startRadius);
        finishPlanet.Add_Bridge_from_planet(this);
        return finishPlanet;
    }
}
