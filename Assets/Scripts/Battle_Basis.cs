﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Anima2D;

public class Battle_Basis : MonoBehaviour {
    public enum TypeHeal { blood, wooden, demon, dead, ghost }
    public TypeHeal bloodType;
    public float bloodMultiplier = 1;
    public int HealMax;
    [HideInInspector]
    public bool IsDamaged;
    protected Weapon[] Weapon_array;
    public Transform Missile_stick_transform; //Missile_Hand_Transform

    protected int heals, myID;
    protected Animator _animator;
    protected Control_enemy controlMove;
    protected int usedWeapon;

    protected bool isAttacked;
    protected CapsuleCollider2D myCollider;
    protected float ScaleX;
    protected Rune applRune;
    protected delegate void runeEffect();

    //a pool of sound events - use for long sounds in order to call stop on them
    private Dictionary<string, FMOD.Studio.EventInstance> soundEventsDict = new Dictionary<string, FMOD.Studio.EventInstance>();
    private List<FMOD.Studio.EventInstance> soundEvents = new List<FMOD.Studio.EventInstance>();

    private void Awake()
    {
        myCollider = GetComponent<CapsuleCollider2D>();
        ScaleX = transform.localScale.x;
    }

    void Start()
    {
        EventManager.Invoke("HealMax", new BecomeEvent(true, myID, HealMax));
        myCollider = GetComponent<CapsuleCollider2D>();
        ScaleX = transform.localScale.x;
        applRune = new Rune();
        _animator = GetComponent<Animator>();
        heals = HealMax;
        usedWeapon = -1;
        _animator.SetInteger("attack", usedWeapon);
    }

    protected void instDamageIcon()
    {
        string damageType = "damage";
        switch(bloodType)
        {
            case TypeHeal.blood:
                damageType += "Blood";
                break;
            case TypeHeal.demon:
                damageType += "Demon";
                break;
            case TypeHeal.dead:
                damageType += "Dead";
                break;
            case TypeHeal.wooden:
                damageType += "Wood";
                break;
            case TypeHeal.ghost:
                damageType += "Ghost";
                break;
        }

        GameObject damageIcon = UITextManager.uitextManager.GetObject(damageType);
        Vector3 startIconPos = transform.position + transform.up * myCollider.size.y * ScaleX / 2;
        float rndWidth = myCollider.size.x * ScaleX / 2;
        startIconPos += Random.Range(-rndWidth, rndWidth) * transform.right;
        //GameObject gameText = (GameObject)Instantiate(Resources.Load( damage_heal ), startTextPos, transform.rotation);

        damageIcon.transform.position = startIconPos;
        damageIcon.transform.rotation = transform.rotation;
        //damageIcon.transform.parent = CanvasDamage.canvasDamage.transform;
        damageIcon.SetActive(true);
    }

    protected void instDamageText(int damage, float angleToFly)
    {
        string damage_heal = "";

        damage_heal = damage > 0 ? "damage" : "heal";
        damage_heal += "Text";
        
        GameObject gameText = UITextManager.uitextManager.GetObject(damage_heal);

        Vector3 startTextPos = transform.position + transform.up * myCollider.size.y * ScaleX / 2;
        float rndWidth = myCollider.size.x * ScaleX / 2;
        startTextPos += Random.Range(-rndWidth, rndWidth) * transform.right;
        //GameObject gameText = (GameObject)Instantiate(Resources.Load( damage_heal ), startTextPos, transform.rotation);
        gameText.transform.position = startTextPos;
        gameText.transform.rotation = transform.rotation;
        gameText.transform.parent = CanvasDamage.canvasDamage.transform;
        int setTextFlyRight = 0;
        if (angleToFly > 90)
            setTextFlyRight = 1;
        else if (angleToFly < 90)
            setTextFlyRight = -1;
        gameText.GetComponent<damageText>().rightFly = setTextFlyRight;
        if (damage != 0)
            gameText.GetComponent<Text>().text = Mathf.Abs(damage).ToString();
        else
            gameText.GetComponent<Text>().text = "miss";
        gameText.SetActive(true);
    }
    public void AttackFromRune()
    {
        Attacking((int)applRune.Damage, 0);
    }
    public virtual void Attacking(int damage, float fromAngle_)
    {
        heals -= damage;
        if (heals > HealMax)
        {
            //Debug.Log("Damage: " + damage);
            damage += heals - HealMax;
            heals -= heals - HealMax;
        }
        else if (heals < 0)
        {
            damage += heals;
            heals = 0;
        }

        if(myID == 0 || damage == 0)
            instDamageText(damage, fromAngle_);
        else
        {
            for (int i = 0; i < damage; i++)
            {
                instDamageIcon();
            }
        }

        EventManager.Invoke("TakeDamage", new BecomeEvent(true, myID, damage));
        if (!isAttacked && damage > 0)
            _animator.SetTrigger("damageLow");
        if (heals <= 0)
        {
            Death();
        }
    }

    public void Attacking(Weapon _weapon, Vector3 punchPoint)
    {

        if (controlMove && _weapon.IsMelee() &&  controlMove.InBuild())
            return;
        int damage = _weapon.Power;

        damage *= WeakDamage(damage, _weapon);
        //Test

        
        if (_weapon.type_weapon == Weapon.Type.melee_magic
            || _weapon.type_weapon == Weapon.Type.magic)
            return;

        GameObject particleGO = ParticlesManager.particleManager.GetObject("particle_blood");//(GameObject)Instantiate(Resources.Load("Tooling\\particle_blood"));
        particleGO.transform.position = transform.position;
        particleGO.transform.rotation = transform.rotation;
        Vector3 punchVect = punchPoint - transform.position;
        float _angle;

        Vector3 inversPunch = transform.InverseTransformPoint(punchPoint);
        _angle = Vector3.Angle(transform.right, punchVect);

        Attacking(damage, _angle);

        if (inversPunch.y < 0)
        {
            particleGO.transform.Rotate(-Vector3.forward, _angle);
        }
        else
        {
            particleGO.transform.Rotate(Vector3.forward, _angle);
        }
        particleGO.transform.position += punchVect;
        float scaleX = transform.localScale.x;

        ParticleSystem PS = particleGO.GetComponent<ParticleSystem>();

        SetParticle(PS, _weapon.type_weapon);
        particleGO.SetActive(true);
        PS.Play();
    }

    

    //Если есть множители слабости...
    protected virtual int WeakDamage(int dmg, Weapon weapon)
    {
        return 1;
    }

    public void AnimatorFreeze(float _speed)
    {
        _animator.speed = _speed;
        //Debug.Log("freeze: " + _speed);
    }
    protected virtual void SetLoot()
    {
        return;
    }

    public void Death()
    {
        GetComponent<Movement_basis>().enabled = false;
        CapsuleCollider2D myCollider_ = GetComponent<CapsuleCollider2D>();
        myCollider_.enabled = false;
        SetLoot();
        StopAllCoroutines();
        _animator.SetBool("death", true);
        Planet currPlanet = GetComponent<Movement_basis>().GetPlanet();
        if (currPlanet)
            currPlanet.RemoveEnemy(myID);

        EventManager.Invoke("Death", new BecomeEvent(true, myID, 0));
        if(controlMove)
            controlMove.SetSortGroup("Planet", myID);
        this.enabled = false;
    }



    private void SetParticle(ParticleSystem PS, Weapon.Type typeAttack)
    {
        var sh = PS.shape;
        var _main = PS.main;
        switch (typeAttack)
        {
            case Weapon.Type.slash:
                _main.maxParticles = 8;
                sh.arc = 90f;
                break;
            case Weapon.Type.hand:
                _main.maxParticles = 6;
                sh.arc = 120f;
                break;
            case Weapon.Type.bow:
                _main.maxParticles = 10;
                sh.arc = 60f;
                break;
        }
        _main.maxParticles = (int)(_main.maxParticles * bloodMultiplier);
        switch (bloodType)
        {
            case TypeHeal.blood:
                _main.startColor = Color.red;
                break;
            default:
                _main.startColor = new Color(1, 1, 1, 0);
                break;
        }
    }

    public Transform Get_StickFor_Missle()
    {
        if (Missile_stick_transform)
            return Missile_stick_transform;
        else
            return
                transform;
    }



    //Атакуют его
    public void IsAttacking(bool isAttacking)
    {
        IsDamaged = isAttacking;
    }
    //Атакует сам
    public void IsAttacked(bool _attacked)
    {
        isAttacked = _attacked;
        if (!isAttacked)
        {
            usedWeapon = -1;
            _animator.SetInteger("attack", -1);
        }
    }
    public bool IsAttacked()
    {
        return isAttacked;
    }
    //Перегрузка функции Ульф'ом из аниматора устанавливает параметр used_weapon
    public virtual void StartRechargeWeapon(int _used)
    {
        if (usedWeapon == -1)
            return;
        StartCoroutine(RechargeWeapon());
    }

    public virtual void SetRune(Rune _rune)
    {
        return;


    }
   
    public void ReSetRune()
    {
        applRune = new Rune();
    }


    //protected IEnumerator RuneLifeTime()
    //{

    //}

    protected IEnumerator RechargeWeapon()
    {
        //Debug.Log("usedWeapon: " + usedWeapon);
        Weapon weapon = Weapon_array[usedWeapon];

        weapon.Recharging(true);
        yield return new WaitForSeconds(weapon.Recharge);
        weapon.Recharging(false);
    }

    public virtual bool CanMove()
    {
        bool can = !IsAttacked() && !IsDamaged;
        Debug.Log("can move: " + can);
        if (can)
            return true;
        else
            return false;
    }

    public int GetMyID()
    {
        return myID;
    }

    #region SOUND PLAYBACK METHODS

    public void SoundPlay(string clipName)
    {
        FMODUnity.RuntimeManager.PlayOneShotAttached(clipName, gameObject);
    }

    public void LongSoundPlay(string clipName)
    {
        FMOD.Studio.EventInstance _soundEvent;
        //check if the pool contains the event 
        if (!soundEventsDict.TryGetValue(clipName, out _soundEvent))
        {
            //create new instance
            _soundEvent = FMODUnity.RuntimeManager.CreateInstance(clipName);
            FMODUnity.RuntimeManager.AttachInstanceToGameObject(_soundEvent, transform, GetComponent<Rigidbody>());
            _soundEvent.start();
            soundEventsDict[clipName] = _soundEvent;
        }
    }

    public void LongSoundRetrigger(string clipName)
    {
        FMOD.Studio.EventInstance _soundEvent;
        //check if the pool contains the event 
        if (soundEventsDict.TryGetValue(clipName, out _soundEvent))
        {
            //retrigger the event
            _soundEvent.start();
        }
    }

    public void LongSoundStop(string clipName)
    {
        FMOD.Studio.EventInstance _soundEvent;
        //check if the pool contains the event 
        if (soundEventsDict.TryGetValue(clipName, out _soundEvent))
        {
            //stop the event
            _soundEvent.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        }
    }

    private void GetSoundEventName(ref FMOD.Studio.EventInstance _soundEventInstance, out string name)
    {
        // expect the result in the form event:/folder/sub-folder/eventName
        FMOD.Studio.EventDescription _description;

        _soundEventInstance.getDescription(out _description);
        _description.getPath(out name);
    }
    #endregion
}