﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShaderChange : MonoBehaviour
{
    public GameObject go_for_change;
    public Shader newShader;
    public Material newMaterial;
    // Start is called before the first frame update
    public void changeFunc(GameObject GO)
    {
        SkinnedMeshRenderer SMR = GO.GetComponent<SkinnedMeshRenderer>();
        SpriteRenderer SR = GO.GetComponent<SpriteRenderer>();

        if (SMR)
            SMR.material.shader = newShader;
        if (SR)
            SR.material = newMaterial;

        if (GO.transform.childCount > 0)
            for (int c = 0; c < GO.transform.childCount; c++)
            {
                //Рекурсия
                changeFunc(GO.transform.GetChild(c).gameObject);
            }
    }
}
