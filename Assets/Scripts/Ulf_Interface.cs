﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ulf_Interface : MonoBehaviour {
    private Canvas _canvas;
    public Texture mouseTexture;
    private float _height, _width;
    private const float ratio16 = 1.777778f;
    //public Transform newCursor;

    void Awake()
    {
        Cursor.visible = false;
    }
    void OnGUI()
    {
        Vector2 v3 = Input.mousePosition;
        v3.y = Screen.height - v3.y;
        GUI.depth = 0;
        GUI.Label(new Rect(v3.x, v3.y, mouseTexture.width, mouseTexture.height), mouseTexture);
    }


    // Use this for initialization
    void Start () {
        _height = Screen.height;
        _width = Screen.width;
        _canvas = GetComponent<Canvas>();
        // Texture2D texture2D = (Texture2D)Cursor_peace;
        // Cursor.SetCursor(texture2D, Vector2.zero, CursorMode.ForceSoftware);
        //Cursor.visible = false;
        float aspectRatio = _width / _height;
        if(Mathf.Abs(aspectRatio / ratio16 - 1f) >= .05f)
        {
            correctRatio(aspectRatio / ratio16);
        }
    }
	
    private void correctRatio(float correctVar)
    {
        int _childs = transform.childCount;
        for(int c = 0; c < _childs; c++)
        {
            RectTransform _rect = transform.GetChild(c).GetComponent<RectTransform>();
            if(_rect)
            {
                _rect.localScale = new Vector3(_rect.localScale.x , _rect.localScale.y * correctVar, 1);
            }
        }
    }
	// Update is called once per frame
	void Update () {
        /*
        var v3 = Input.mousePosition;
        v3.z = 10f;
        v3 = Camera.main.ScreenToWorldPoint(v3);

        Vector3 currentPositon = v3;

        newCursor.position = currentPositon;
        */
        //if(Input.GetKeyUp(KeyCode.U))
        //{
        //  _canvas.enabled ^= true;
        //}
    }
}
