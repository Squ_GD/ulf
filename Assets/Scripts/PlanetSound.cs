﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMOD.Studio;

public class PlanetSound : MonoBehaviour
{
    // A bank with the sound is loaded separately via standard FMOD StudioBankLoader
    RequireComponent StudioBankLoader;

    [SerializeField, Tooltip("Event path used for instantiation")]
	private string soundEventName;
    private EventInstance soundEvent;
    [SerializeField, Tooltip("Plays the sound on enable if TRUE")]
    private bool PlayOnEnable = false;
    [SerializeField, Tooltip("Fade curve")]
    private AnimationCurve fadeCurve;

    private void OnEnable()
	{
        if (soundEventName != null)
        {
            soundEvent = FMODUnity.RuntimeManager.CreateInstance(soundEventName);
        }
        if (PlayOnEnable)
        {
            PlayPlanetSound();
        }
	}
    private void OnDisable()
    {
        soundEvent.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        soundEvent.release();
    }

    //hadle sounds

    /// <summary>
    /// Starts playing a sound event 
    /// </summary>
    public void PlayPlanetSound()
	{
        PLAYBACK_STATE state;
        soundEvent.getPlaybackState(out state);
        if (state != PLAYBACK_STATE.PLAYING)
            soundEvent.start();
	}
    /// <summary>
    /// Stops playing a sound event
    /// </summary>
    public void StopPlanetSound()
    {
        soundEvent.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
    }
    /// <summary>
    /// Sets the volume level. Units: Linear, Range: [0, 1], Default: 1.
    /// </summary>
    public void SetSoundVolume(float volume)
    {
        float _output = fadeCurve.Evaluate(volume);
        soundEvent.setVolume(_output);
    }
}