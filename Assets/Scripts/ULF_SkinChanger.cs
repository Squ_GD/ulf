﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Anima2D;

public class ULF_SkinChanger : MonoBehaviour
{
    public SpriteMeshAnimation[] skins;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.End))
        {
            for (int i = 0; i < skins.Length; i++)
            {
                if (skins[i].frame == skins[i].frames.Length - 1)
                    skins[i].frame = 0;
                else
                    skins[i].frame += 1;
            }
        }
    }
}
