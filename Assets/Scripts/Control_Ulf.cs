﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class Control_Ulf : Movement_basis {
    protected bool MultiPlayer = false;

    public float StartSpeed, MaxSpeed, SlowMoveSpeed, Acceleration;

    protected Animator animator;

    //private int enemyCollisions;
    public bool Flipped { get; private set; }
    public static Control_Ulf ulf_static;
    private Battle_Ulf battleComponent;
    private bool firstCollisionPlanet, standingOnBridge, inventoryOpened, canMoveForward, inRoot, canDamageRoot;
    private Bridge tempBridge = null;
    protected float oldDirect = 0;

    public UlfDeg deg;
    //private bool withShield;
    // Use this for initialization

    protected void Awake()
    {
        ulf_static = this;
        IsUlf = true;

        if (PlayerNetSocket.playerNetSocket)
        {
            MultiPlayer = true;
        }
    }
    protected void OnDestroy()
    {
        ulf_static = null;
    }
    public void PlayMoveSound(string name)
    {
        //Debug.Log("Play: " + name);
        //AudioClip playingClip = Resources.Load<AudioClip>("Sound\\ULF\\" + name);
        // playMoveSound(playingClip);
        //audioSource.PlayOneShot(playingClip);
    }

    /*private void playMoveSound(AudioClip AC)
    {
        if (AC == null)
        {

            audioSource.Stop();
        }
        else
        {
            audioSource.Stop();

            audioSource.PlayOneShot(AC);
        }

    }

    public void Set_Random_Deg_Multiplayer()
    {
        if (!deg.planet)
            deg.planet = GameObject.FindGameObjectWithTag("planet").GetComponent<Planet>();
        float startDeg_ = deg.planet.Get_Random_Degree(this);
        Converter.converter.SendStartPosToAll(startDeg_);
        Move(0);
    }
    */

    void Start () {
        
        inventoryOpened = Inventory.inventory.isOpened();
        EventManager.AddListener("Death", deathUlf);
        EventManager.AddListener("Inventory", openInventory);
        //audioSource = GetComponents<AudioSource>()[1];
        battleComponent = GetComponent<Battle_Ulf>();
        animator = GetComponent<Animator>();
        if (deg.planet)
        {
            set_Planet(deg.planet);
            deg.planet.Get_Object_degree(this);
           // Move(1);
        }
       // speed_temp = SpeedLinear;
        StartCoroutine(checkForLookEnemy());
        StartCoroutine(checkForMoveForward());
        if (MultiPlayer)
        {
            PlayerNetSocket.playerNetSocket.IReady(true);
            this.enabled = false;
        }

    }
    //Проверка, близко ли враг и видим ли его(для Enemy_HP)
    public void openInventory(BecomeEvent BE)
    {
        inventoryOpened = BE.come;
    }
    protected IEnumerator checkForLookEnemy()
    {
        while(true)
        {
            yield return new WaitForSeconds(1f);
            if(deg.planet)
            {
                deg.planet.CheckEnemyList(transform);
            }
        }
    }

    

    private IEnumerator checkForMoveForward()
    {
        while (true)
        {
            yield return new WaitForSeconds(.1f);
            foreach(RaycastHit2D hit in Physics2D.RaycastAll(transform.position - transform.up * .2f, transform.right, colliderWidth+.07f))
            {
                if (hit.collider.gameObject.tag == "enemy" && !hit.collider.isTrigger)
                {
                    canMoveForward = false;
                    break;
                }
                else
                    canMoveForward = true;
            }

            if(canMoveForward)//Если первый не сработал
            foreach (RaycastHit2D hit in Physics2D.RaycastAll(transform.position + transform.up * .3f, transform.right, colliderWidth + .07f))
            {
                if (hit.collider.gameObject.tag == "enemy" && !hit.collider.isTrigger)
                {
                    canMoveForward = false;
                    break;
                }
                else
                    canMoveForward = true;
            }
        }

    }

    public void ChangeMaxSpeed(float value)
    {
        Debug.Log("Speed change");
        MaxSpeed += value;
        if (SpeedLinear > MaxSpeed)
            SpeedLinear = MaxSpeed;
    }
    private void deathUlf(BecomeEvent BE)
    {
        if(BE.come && BE.id == 0)
        {
            deg.planet.Ulf_Leave(this);
        }
    }

    public void Stand_To_Bridge(Bridge bridge_new, int numBridgeForMP, bool fromStartBridge, bool fromBackBridge)
    {

        //if (Input.GetAxis("Vertical") <= 0)
        //  return;

        if (MultiPlayer)
        {
            Converter.converter.SendStandBridge(numBridgeForMP);
        }

        if (Input.GetAxis("Vertical") > 0 && fromStartBridge)
        {
            Stand_Bridge_Inner(bridge_new);
            //Debug.Log("Stand from start bridge");
        }
        else if (!tempBridge && (fromBackBridge || fromStartBridge))
        {
            tempBridge = bridge_new;
            Debug.Log("temp bridge: " + tempBridge);
        }
        else if (!fromBackBridge && !fromStartBridge)
        {
            tempBridge = null;
            //Debug.Log("null temp bridge");
        }
    }

    protected void Stand_Bridge_Inner(Bridge bridge_new)
    {
        bridge_new.PlaySoundPlanet(deg.planet);

        deg.planet.Ulf_Leave(this);
        //bridge_new.Ulf_Stand(deg.planet, this);
        //deg.planet = null;

        deg.bridge = bridge_new;
        deg._deltaBridge = 0;
        deg.startBridgeDir = lastDir;
        set_Bridge(deg.bridge);

        EventManager.Invoke("Planet stand", new BecomeEvent(false, 0, 0));
        gameObject.GetComponent<SortingGroup>().enabled = true;


        //tempBridge = null;
    }

    public virtual void Set_New_Planet(Planet planet_new)
    {
        /*if(deg.bridge)
        {
            deg.bridge.StopPlaySoundOppositePlanet(planet_new);
        }
        */

        deg.bridge = null;

        deg.planet = planet_new;
        set_Planet(deg.planet);

        if (!firstCollisionPlanet && !MultiPlayer)
        {

            deg.planet.Get_Object_degree(this);
            Move(1);
        }
        Move(0);

        planet_new.Stand_To_Planet(this);

        EventManager.Invoke("Planet stand", new BecomeEvent(true, 0, 0));
        //Debug.Log("planet_new: " + Planet_current.name);
        gameObject.GetComponent<SortingGroup>().enabled = false;

    }

    public float get_Height()
    {
        return colliderHeight;
    }

    public Planet Get_planet()
    {
        return deg.planet;
    }

    // Update is called once per frame
    protected virtual void Update() // FixedUpdate()
    {

        if (standingOnBridge)
            return;
        if (inventoryOpened)
        {
            SpeedLinear = 0;
            animator.SetFloat("speed", SpeedLinear);
            return;
        }

        if (Input.GetAxis("Vertical") > 0 && !deg.bridge && tempBridge)//Подъем на мост
        {
            
            standingOnBridge = true;
            StartCoroutine(GoUp_On_Bridge());
            return;
        }
        else if (deg.bridge && deg.bridge.PositionToDown(this) && Input.GetAxis("Vertical") < 0)//Cпуск с моста
        {
            SpeedLinear = SlowMoveSpeed;
            animator.SetFloat("speed", SpeedLinear);

            standingOnBridge = true;
            StartCoroutine(GoDown_Planet());
            return;
        }


        float directHorizontal = Input.GetAxis("Horizontal");
        

        if (MultiPlayer && oldDirect != directHorizontal)
        {
            float newDirect = (float)System.Math.Round(directHorizontal, 1);
            Converter.converter.SendNewDirect(newDirect);
            oldDirect = directHorizontal;
        }
        
        if (directHorizontal != 0)
        {
           // Inventory.inventory.TryClose();
        }
        bool shift = Input.GetAxis("Shift") > 0;
        if (directHorizontal != 0 
            && battleComponent.CanMove() 
            && !Flipped)
        {
            Inventory.inventory.TryClose();
            Control_Move(directHorizontal, shift);
        }
        else
        {
            SpeedLinear = 0;
            animator.SetFloat("speed", SpeedLinear);

           // playMoveSound(null);
            if (!deg.bridge && deg.planet)
            {
                deg.planet.Get_Object_degree(this);
                Move(0);

            }
        }

        
    }

    public void Flip_Ulf()
    {
        Flipping();
        Flipped = false;
        lastDir *= -1;
    }

    IEnumerator GoUp_On_Bridge()
    {

        Vector3 bridgeCenter = tempBridge.Get_Center();
        float bridgeRadius = tempBridge.GetComponent<CircleCollider2D>().radius * tempBridge.transform.localScale.x;
        bool rotateOnCenter = false, moveOnCenter = false;
        while (!rotateOnCenter || !moveOnCenter)
        {
            SpeedLinear = SlowMoveSpeed;
            animator.SetFloat("speed", SpeedLinear);

            if (!rotateOnCenter)
            {
                float angle = Vector2.Angle(transform.up, bridgeCenter - transform.position);
                int rotateDir = transform.InverseTransformPoint(bridgeCenter).x > 0 ? -1 : 1;
                transform.Rotate(Vector3.forward * rotateDir, Time.deltaTime * 60f);

                if(angle <= 1f)
                {
                    rotateOnCenter = true;
                }
            }

            if (!moveOnCenter)
            {
                Vector3 vectorTo = (bridgeCenter - transform.position).normalized;
                transform.position += vectorTo * Time.deltaTime;
                if ((bridgeCenter - transform.position).magnitude <= bridgeRadius - colliderHeight)
                {

                    moveOnCenter = true;
                }
            }
            yield return null;
        }
        Stand_Bridge_Inner(tempBridge);
        //tempBridge = null;
        
        standingOnBridge = false;
    }

    IEnumerator GoDown_Planet()
    {
        Debug.Log("Standing...");
        float distStart = (transform.position - deg.bridge.startPlanet.transform.position).magnitude - deg.bridge.startPlanet.GetRadius();
        float distToFinish = (transform.position - deg.bridge.finishPlanet.transform.position).magnitude - deg.bridge.finishPlanet.GetRadius();
        Planet planetToDown = distStart < distToFinish ? deg.bridge.startPlanet : deg.bridge.finishPlanet;

        Vector3 planetCenter = planetToDown.transform.position;
        float planetRadius = planetToDown.GetComponent<CircleCollider2D>().radius;
        bool rotateOnCenter = false, moveOnCenter = false;
        while (!rotateOnCenter || !moveOnCenter)
        {
            SpeedLinear = SlowMoveSpeed;
            animator.SetFloat("speed", SpeedLinear);

            if (!rotateOnCenter)
            {
                float angle = Vector2.Angle(-transform.up, planetCenter - transform.position);
                int rotateDir = transform.InverseTransformPoint(planetCenter).x > 0 ? -1 : 1;
                transform.Rotate(-Vector3.forward * rotateDir, Time.deltaTime * 60f);

                if (angle <= 1f)
                {
                    rotateOnCenter = true;
                }
            }

            if (!moveOnCenter)
            {
                Vector3 vectorTo = (planetCenter - transform.position).normalized;
                transform.position += vectorTo * Time.deltaTime;
                if ((planetCenter - transform.position).magnitude <= planetRadius + colliderHeight)
                {
                    Debug.Log("End Standing!");
                    moveOnCenter = true;
                }
            }
            yield return null;
        }
        standingOnBridge = false;
        planetToDown.Get_Object_degree(this);
        //deg.new_ = deg.old_ = -1;
        deg.bridge.StopPlaySoundOppositePlanet(planetToDown);

        Set_New_Planet(planetToDown);
        //tempBridge = null;



    }
    public void TempBridgeNull()//minus bug
    {
        tempBridge = null;
    }
    public void Rooted(bool root_, float timePeriod_)
    {
        inRoot = root_;
        if (root_)
            StartCoroutine(rootCorout(timePeriod_));
    }
    IEnumerator rootCorout(float time_)
    {
        while (inRoot)
        {
            canDamageRoot = true;
            yield return new WaitForSeconds(time_);
            
        }
    }
    protected void Control_Move(float direction, bool shifted)
    {
        
        bool  withShield = animator.GetInteger("weaponExtra") == 2;
        int newDir = (int)(Mathf.Abs(direction) / direction);
        if (newDir != lastDir)
        {
            if (!withShield)
            {
                SpeedLinear = 0;
                Flipped = true;
                //animator.SetTrigger("flip");
                Flip_Ulf();
                //lastDir = newDir;
                return;
            }
        }
        if (deg.bridge)
        {
            deg.bridge.Get_Object_degree(this);
            Move_Bridge(lastDir);
        }
        else
        {
            deg.planet.Get_Object_degree(this);
            Move(lastDir);
        }
        float speedUp = (lastDir * newDir) * Time.deltaTime * Acceleration;
        /*
        animator.SetBool("shift", shifted);
        if (shifted)
        {
            SpeedLinear = SlowMoveSpeed;
        }
        else*/

        

        if(SpeedLinear == 0)
            SpeedLinear = StartSpeed;
        else
            SpeedLinear += speedUp;

        if (SpeedLinear > MaxSpeed)
            SpeedLinear = MaxSpeed;
        else if(SpeedLinear < 0 && SpeedLinear < -MaxSpeed )
        {
            SpeedLinear = -MaxSpeed;

        }
        if (!canMoveForward || inRoot)//Enemy Block the way
            SpeedLinear = 0;
        if (inRoot && canDamageRoot)
        {
            battleComponent.AttackFromRune();
            canDamageRoot = false;
        }
        animator.SetFloat("speed", SpeedLinear);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "planet")
        {

            if (!deg.planet && !firstCollisionPlanet)
            {
                Set_New_Planet(collision.gameObject.GetComponent<Planet>());
                firstCollisionPlanet = true;

            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "enemy")
        {
          //  ++enemyCollisions;
          //  SpeedLinear = speed_temp / 15f;

        }
    }
    
    private void LateUpdate()
    {
        //if(Planet_current)
            Physics.gravity = -transform.up;
    }
}

public struct UlfDeg
{
    public Planet planet;
    public Bridge bridge;
    public float new_, old_, _deltaBridge;
    public int startBridgeDir;
}
