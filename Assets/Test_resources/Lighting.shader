﻿Shader "Sprites/Light"
{
    Properties
    {
        [PerRenndererData] _MainTex ("Sprite",2D) = "white" {}
        _Color ("Tint", Color) = (1,1,1,1)
        _LightPosition ("light", Color) = (1,1,1,1)
    }
    SubShader {
        Tags {
            "LightMode" = "ForwardBase"
            "Queue" = "Transparent"
            "RenderType" = "Transparent"
            "PreviewType" = "Plane"
            "CanUseSpriteAtlas" = "True"
        }
        LOD 100
        Cull Off
        Lighting On
        ZWrite Off
        Blend One OneMinusSrcAlpha
        
        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            
            #include "UnityCG.cginc"

            struct indata{
                float4 vertex : POSITION;
                float4 color : COLOR;
                float2 texcoord : TEXCOORD0;
            };

            struct vertdata{
                float4 vertex : SV_POSITION;
                fixed4 color : COLOR;
                float2 texcoord : TEXCOORD0;
                half4 worldPos : TEXCOORD1;
            };


            fixed4 _Color;


            vertdata vert(indata IN)
            {
                vertdata OUT;
                OUT.vertex = UnityObjectToClipPos(IN.vertex);
                OUT.texcoord = IN.texcoord;
                OUT.color = IN.color * _Color;
                OUT.worldPos = mul(unity_ObjectToWorld, IN.vertex);
              
                return OUT;
            }

            sampler2D _MainTex;
            

            fixed4 frag(vertdata IN) : SV_TARGET
            {
                fixed4 c = tex2D(_MainTex, IN.texcoord) * IN.color;
                float dist = distance(float2(unity_4LightPosX0.x, unity_4LightPosY0.x), IN.worldPos.xy);
                float range = (0.005 * sqrt(1000000 - unity_4LightAtten0.x)) / sqrt(unity_4LightAtten0.x);
                dist += 0.3;
                float power = 0.5 /  (dist * dist );
                c.rgb += unity_LightColor[0].rgb * power / range ;

                dist = distance(float2(unity_4LightPosX0.y, unity_4LightPosY0.y), IN.worldPos.xy);
                range = (0.005 * sqrt(1000000 - unity_4LightAtten0.y)) / sqrt(unity_4LightAtten0.y);
                dist += 0.3;
                power = 0.5 /  (dist * dist );
                c.rgb += unity_LightColor[1].rgb * power / range ;

                dist = distance(float2(unity_4LightPosX0.z, unity_4LightPosY0.z), IN.worldPos.xy);
                range = (0.005 * sqrt(1000000 - unity_4LightAtten0.z)) / sqrt(unity_4LightAtten0.z);
                dist += 0.3;
                power = 0.5 /  (dist * dist );
                c.rgb += unity_LightColor[2].rgb * power / range ;

                dist = distance(float2(unity_4LightPosX0.w, unity_4LightPosY0.w), IN.worldPos.xy);
                range = (0.005 * sqrt(1000000 - unity_4LightAtten0.w)) / sqrt(unity_4LightAtten0.w);
                dist += 0.3;
                power = 0.5 /  (dist * dist );
                c.rgb += unity_LightColor[3].rgb * power / range ;

                c.rgb *= c.a;
                return c;
            }

            ENDCG
        }
    }
}